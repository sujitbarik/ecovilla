<?php
namespace console\controllers;

use common\models\OxyPoints;
use common\models\TreePlantation;
use yii\db\Expression;
use yii\console\Controller;

class CronController extends Controller
{
    public function actionAddoxypoints(){
		$this->layout='appblanklayout';
		$query1 = TreePlantation::find()->where(['PlantingStatus'=>1])->andWhere(['<=','PlantingDate',new Expression('(NOW() - INTERVAL 1 HOUR)')])->all();
		if(count($query1)){
			foreach($query1 as $key1 => $value1){
				//$planttime = $value1->PlantingDate;
				$custid = $value1->CustomerID;
				$treeplatid = $value1->ID;
				$chkoxypoints = OxyPoints::find()->where(['TreePlantID'=>$treeplatid])->one();
				if(count($chkoxypoints)){
					$oldoxypoints = $chkoxypoints['OxyPoints'];
					$chkoxypoints->OxyPoints = $oldoxypoints + 0.00570;
					$chkoxypoints->save();
				}
				else{
					$oxypoints = new OxyPoints();
					$oxypoints->TreePlantID = $value1->ID;
					$oxypoints->CustomerID = $value1->CustomerID;
					$oxypoints->OxyPoints = 0.00570;
					$oxypoints->save();
					
				}
				
			}
		}
		
	}

}
