<!doctype html>
<html lang="en">
<head>
    <title>Carbon Footprint</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Include Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" crossorigin="anonymous" />

    <!-- Include SmartWizard CSS -->
    <link href="css/smart_wizard.css" rel="stylesheet" type="text/css" crossorigin="anonymous" />

    <!-- Optional SmartWizard theme -->
    <!--link href="css/smart_wizard_theme_circles.css" rel="stylesheet" type="text/css" crossorigin="anonymous" / -->
    <!--link href="css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" crossorigin="anonymous" / -->
    <link href="css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" crossorigin="anonymous" />
</head>
<body>
<div id="container" class="container-fluid" style="text-align:center; width:700px; height:500px; float:left;">
       
	   
	   
	   
	   
        <form id="myForm" role="form" data-toggle="validator"  accept-charset="utf-8">

        <!-- SmartWizard html -->
        <div id="smartwizard" style="background:red; width:650px;">
            <ul style="background:green; width:40%; float:left; display:none;">
				<li><a href="#step-1"><br /><small>Name</small></a></li>
                <li><a href="#step-2"><br /><small>You Live</small></a></li>
                <li><a href="#step-3"><br /><small>Travel To Office</small></a></li>
				<li><a href="#step-4"><br /><small>Weekend Fun</small></a></li>
                <li><a href="#step-5"><br /><small>Cooking & Electricity</small></a></li>
            </ul>

            <div id="step-container" class="step-container" style="background:yellow;  text-align:center; margin:auto; width:300px !important;">
				 <div id="step-1">
                    
                    <div id="form-step-0" role="form" data-toggle="validator" style="text-align:center;">
					<h2>Your Name</h2>
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Write your name" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div id="step-2">
                   
                    <div id="form-step-1" role="form" data-toggle="validator">
					 <h2>How many people you live together?</h2>
                        <div class="form-group">
							
                            <label for="live-alone">I live alone.</label>
							<input type="radio" class="form-control" name="live" value="1" id="live-alone" required>
                           
                           
                        </div>
						<div class="form-group">
							
                            <label for="live-two">We're two.</label>
							<input type="radio" class="form-control" name="live" value="2" id="live-two" required>
                           
                         
                        </div>
						<div class="form-group">
							
                            <label for="live-three">We're three.</label>
							<input type="radio" class="form-control" name="live" value="3" id="live-three" required>
                           
                         
                        </div>
						<div class="form-group">
							
                            <label for="live-four">We're four.</label>
							<input type="radio" class="form-control" name="live" value="4" id="live-four" required>
                           
                         
                        </div>
						<div class="form-group">
							
                            <label for="live-five">We're five.</label>
							<input type="radio" class="form-control" name="live" value="5" id="live-five" required>
                                             
                        </div>
						<div class="form-group">
							
                            <label for="live-six">We're six(or more).</label>
							<input type="radio" class="form-control" name="live" value="6" id="live-six" required>
                            <div class="help-block with-errors"></div>                  
                        </div>
						
						
                    </div>

                </div>
               
                <div id="step-3">
                   
                    <div id="form-step-2" role="form" data-toggle="validator">
					    
						 <h2>What is your favorite mode of transportation in a working day?</h2>
                        <div class="form-group">
							
                            <label for="office-bike">Personal Motorbike or Scooter</label>
							<input type="radio" class="form-control" name="office" value="bike" id="office-bike" required>
                                         
                        </div>
						<div class="form-group">
							
                            <label for="office-car">Personal Car</label>
							<input type="radio" class="form-control" name="office" value="car" id="office-car" required>
                           
                         
                        </div>
						<div class="form-group" id="office-car-type-cont" style="display:none">
							
                            <label for="office-car-type">Fuel Type</label>
							<select class="form-control" name="office-car-type" id="office-car-type">
							<option></option>
							<option value="petrol">Petrol</option>
							<option value="diesel">Diesel</option>
							</select>
							<div class="help-block with-errors" id="office-car-type-error"></div>
                        </div>
						<div class="form-group">
							
                            <label for="office-auto">Public Auto</label>
							<input type="radio" class="form-control" name="office" value="auto" id="office-auto" required>
                           
                         
                        </div>
						
						<div class="form-group">
							
                            <label for="office-bus">Public Bus</label>
							<input type="radio" class="form-control" name="office" value="bus" id="office-bus" required>
                           
                          <div class="help-block with-errors"></div>
                        </div>
						
						
						<h2>How much do you move in a working day (round trip)?</h2>
						<div class="form-group">
							
                            <label for="office-20"> 1-20 KM</label>
							<input type="radio" class="form-control" name="officedistance" value="20" id="office-20" required>
                                         
                        </div>
						<div class="form-group">
							
                            <label for="office-50"> 20-50 KM </label>
							<input type="radio" class="form-control" name="officedistance" value="50" id="office-50" required>
                           
                         
                        </div>
						<div class="form-group">
							
                            <label for="office-100">50-100 KM</label>
							<input type="radio" class="form-control" name="officedistance" value="100" id="office-100" required>
                           
							<div class="help-block with-errors"></div>
                        </div>
						
						
                    </div>
                </div>
				 <div id="step-4">
                    
                    <div id="form-step-3" role="form" data-toggle="validator">
						<h2>What is your favorite mode of transportation during the weekend?</h2>
                         <div class="form-group">
							
                            <label for="fun-bike">Personal Motorbike or Scooter</label>
							<input type="radio" class="form-control" name="weekfun" value="bike" id="fun-bike" required>
                                         
                        </div>
						<div class="form-group">
							
                            <label for="fun-car">Personal Car</label>
							<input type="radio" class="form-control" name="weekfun" value="car" id="fun-car" required>
                        </div>
						<div class="form-group" id="weekfun-car-type-cont" style="display:none">
							
                            <label for="weekfun-car-type">Fuel Type</label>
							<select class="form-control" name="weekfun-car-type" id="weekfun-car-type">
							<option></option>
							<option value="petrol">Petrol</option>
							<option value="diesel">Diesel</option>
							</select>
							<div class="help-block with-errors" id="weekfun-car-type-error"></div>
                        </div>
						<div class="form-group">
							
                            <label for="fun-auto">Public Auto</label>
							<input type="radio" class="form-control" name="weekfun" value="auto" id="fun-auto" required>
                           
                         
                        </div>
						<div class="form-group">
							
                            <label for="fun-bus">Public Bus</label>
							<input type="radio" class="form-control" name="weekfun" value="bus" id="fun-bus" required>
                        </div>
						<h2>How much do you travel on weekends (round trip)?</h2>
						<div class="form-group">
							
                            <label for="fun-50"> Less then 50 KM</label>
							<input type="radio" class="form-control" name="fundistance" value="50" id="fun-50" required>
                                         
                        </div>
						<div class="form-group">
							
                            <label for="fun-150"> 50-150 KM </label>
							<input type="radio" class="form-control" name="fundistance" value="150" id="fun-150" required>
                           
                         
                        </div>
						<div class="form-group">
							
                            <label for="fun-250">150-250 KM</label>
							<input type="radio" class="form-control" name="fundistance" value="250" id="fun-250" required>
                           
							
                        </div>
						<div class="form-group">
							
                            <label for="fun-500">250-500 KM</label>
							<input type="radio" class="form-control" name="fundistance" value="500" id="fun-500" required>
                           
							<div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div id="step-5" class="">
					<div id="form-step-4" role="form" data-toggle="validator">
                    <h2>No of LPG cylinder you use in a year?</h2>
						<div class="form-group">
							<label for="lpg">LPG cylinder</label>
							<select name="lpg" class="form-control" id="lpg" required>
							<option></option>
							<option value="3">3 Nos</option>
							<option value="4">4 Nos</option>
							<option value="5">5 Nos</option>
							<option value="6">6 Nos</option>
							<option value="7">7 Nos</option>
							<option value="8">8 Nos</option>
							<option value="9">9 Nos</option>
							<option value="10">10 Nos</option>
							<option value="11">11 Nos</option>
							<option value="12">12 Nos or More</option>
							</select>
							<div class="help-block with-errors"></div>
						</div>
						 <h2>Average electricity units consume  in a month?</h2>
						<div class="form-group">
							
                            <label for="units">Units</label>
							<input type="number" class="form-control" name="units" id="units" required>
                           
							<div class="help-block with-errors"></div>
                        </div>
					</div>
                </div>
				
				
            </div>
			
			
        </div>
		
        </form>
	  <div id="result" style="display:none">
		<div id="result_oval">
		<div id="co_index"></div>
		<div id="tree_index"></div>
		</div>
	    <div id="recalculate">
		
		<button onclick="recalculate();" class="btn btn-info">Re-Calculate</button>
		</div>
	   
	   </div>

    </div>

	
	<script type="text/javascript" src="js/jquery.js" crossorigin="anonymous"></script>
	
	<script type="text/javascript" src="js/validator.js" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/jquery.smartWizard.js" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/jquery.slimscroll.js" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/jquery-ui.js" crossorigin="anonymous"></script>
	
<script>

diesel_index=2.653;
petrol_index=2.296;
lpg_index=41.762;
electric_index=0.85;
tree_index=158;
vehicle={
	"diesel":{"auto":{"index":diesel_index,"_public":5,"milage":15},"car":{"index":diesel_index,"_public":4,"milage":15},"bus":{"index":diesel_index,"_public":30,"milage":4}},
	"petrol":{"car":{"index":petrol_index,"_public":4,"milage":15},"bike":{"index":petrol_index,"_public":2,"milage":50}}
}
	


var client_name=null;
var live_as=null;
var office=null;
var officedistance=null;
var weekfun=null;
var fundistance=null;
var officecartype=null;
var weekfuncartype=null;
var lpg=null;	
var units=null;
var slimScroll_item;
function reset_vars(){
client_name=null;
live_as=null;
office=null;
officedistance=null;
weekfun=null;
fundistance=null;
officecartype=null;
weekfuncartype=null;
lpg=null;
units=null;
$('#smartwizard').smartWizard("reset");
$('#myForm').trigger("reset");
}
function openmodal(data){
	reset_vars();
	$("#result #co_index").html("CO2 Index "+ parseFloat(Math.round(data.total_co2 * 100) / 100).toFixed(2)+" tonne per year");
	$("#result #tree_index").html("Need "+data.plant_tree_index+" trees to plant per year");
	$("#myForm").hide('drop', {}, 1500,function(){
		$("#result").show('drop', {}, 1500);
	});
	
	
}

function recalculate(){
	
	$("#result").hide('drop', {}, 1500,function(){
		$("#myForm").show('drop', {}, 1500);
		$("#result #co_index").html("");
		$("#result #tree_index").html("");
	});
	
}
function calculate(){
	
	var office_index=0;
	var weekfun_index=0;
	var indi_weekfun_index=0;
	var cooking_index=0;
	var ele_index=0;
	var plant_tree_index=0;
	
	console.log(office);
	console.log(officedistance);
	console.log(officecartype);
	
	if(office=="bike"){
		office_index=(vehicle.petrol.bike.index/vehicle.petrol.bike.milage)*officedistance;
	}
	else if(office=="car"){
		office_index=(vehicle[officecartype].car.index/vehicle[officecartype].car.milage)*officedistance;
	}
	else {
		office_index=((vehicle.diesel[office].index/vehicle.diesel[office].milage)/vehicle.diesel[office]._public )*officedistance;
	}
	office_index=office_index*300;
	
	
	console.log(office_index);
	
	if(weekfun=="bike"){
		weekfun_index=(vehicle.petrol.bike.index/vehicle.petrol.bike.milage)*fundistance;
		weekfun_index=weekfun_index*Math.ceil(live_as/vehicle.petrol.bike._public);
		
	}
	else if(weekfun=="car"){
		weekfun_index=(vehicle[weekfuncartype].car.index/vehicle[weekfuncartype].car.milage)*fundistance;
		console.log(weekfun_index);
		weekfun_index=weekfun_index*Math.ceil(live_as/vehicle[weekfuncartype].car._public);
		
	}
	else {
		weekfun_index==((vehicle.diesel[weekfun].index/vehicle.diesel[weekfun].milage)/vehicle.diesel[weekfun]._public )*fundistance;
		weekfun_index=weekfun_index*live_as;
		
	}
	weekfun_index=weekfun_index*40;
	var indi_weekfun_index=weekfun_index/live_as;
	
	cooking_index=lpg_index*lpg;
	
	var indi_cooking_index=cooking_index/live_as;
	
	ele_index=units*12*electric_index;
	var indi_ele_index=ele_index/live_as;
	
	
	
	var total=office_index+weekfun_index+cooking_index+ele_index;
	var indi_total=office_index+indi_weekfun_index+indi_cooking_index+indi_ele_index;
	
	plant_tree_index=Math.ceil(total/tree_index);
	
	var indi_plant_tree_index=Math.ceil(plant_tree_index/live_as);
	
	total=total/1000;
	indi_total=indi_total/1000;
	
	var data={"office_index":office_index,"weekfun_index":weekfun_index,"indi_weekfun_index":indi_weekfun_index,"cooking_index":cooking_index,"indi_cooking_index":indi_cooking_index,"ele_index":ele_index,"indi_ele_index":indi_ele_index,"client_name":client_name,"live_as":live_as,"plant_tree_index":plant_tree_index,"indi_plant_tree_index":indi_plant_tree_index,"total_co2":total,"indi_total_co2":indi_total}
	
	
	
	
	openmodal(data);
}
</script>	
	<script type="text/javascript">
        $(document).ready(function(){
			
			
			
			 var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info btn-finish d-none')
                                             .on('click', function(){
                                                    if( !$(this).hasClass('disabled')){
														var elmForm = $("#myForm");
														
                                                        if(elmForm){
                                                            elmForm.validator('validate');
															
                                                            var elmErr = elmForm.find('.has-error');
                                                            if(elmErr && elmErr.length > 0){
                                                               return false;
                                                            }else{
                                                              
                                                               calculate();
															   return false;
															   
                                                              
                                                            }
                                                        }
                                                    }
                                                });
			var btnCancel = $('<button></button>').text('Reset')
                                             .addClass('btn btn-success')
                                             .on('click', function(){
                                                    reset_vars();
                                                });
			
			
			 $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'dots',
                    transitionEffect:'fade',
					autoAdjustHeight:false,
                    toolbarSettings: {toolbarPosition: 'top',
									
                                      toolbarExtraButtons: [btnCancel,btnFinish]
                                    },
                    anchorSettings: {
                                markDoneStep: true, // add done css
                                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                            }
                 });
			 $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                var elmForm = $("#form-step-" + stepNumber);
                // stepDirection === 'forward' :- this condition allows to do the form validation
                // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
                if(stepDirection === 'forward' && elmForm){
                    elmForm.validator('validate');
                    var elmErr = elmForm.children('.has-error');
                    if(elmErr && elmErr.length > 0){
                        // Form validation failed
                        return false;
                    }
                }
                return true;
            });
			$("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
                // Enable finish button only on last step
				
				$("#step-container").slimScroll({scrollTo:"0px"});
				
				
                if(stepNumber == 4){
                    $('.btn-finish').removeClass('d-none');
					 $('.btn-finish').addClass('d-block');
					 $('.btn-success').removeClass("radious");
                }else{
                   $('.btn-finish').removeClass('d-block');
					 $('.btn-finish').addClass('d-none');
					 $('.btn-success').addClass("radious");
                }
            });
			
			$("input[name='name']").change(function(){
				client_name=this.value;
			});
			
			$("input[name='live']").change(function(){
				live_as=this.value;
			});
			
			$("input[name='office']").change(function(){
				
				office=this.value;
				
				if(office=="car"){
					$("#office-car-type").prop('required',true);
					$("#office-car-type-cont").show();
					$("#office-car-type").show();
				}
				else{
					$("#office-car-type").prop('required',false);
					if($("#office-car-type-cont").hasClass( "has-error" )){
						$("#office-car-type-cont").removeClass('has-error');
						$("#office-car-type-cont").removeClass('has-danger');
					}
					$("#office-car-type-cont").hide();
					$("#office-car-type").hide();
				}
				
			});
			$("input[name='officedistance']").change(function(){
				officedistance=this.value;
			});
			
			$("input[name='weekfun']").change(function(){
				
				weekfun=this.value;
				
				if(weekfun=="car"){
					$("#weekfun-car-type").prop('required',true);
					$("#weekfun-car-type-cont").show();
					$("#weekfun-car-type").show();
				}
				else{
					$("#weekfun-car-type").prop('required',false);
					if($("#weekfun-car-type-cont").hasClass( "has-error" )){
						$("#weekfun-car-type-cont").removeClass('has-error');
						$("#weekfun-car-type-cont").removeClass('has-danger');
					}
					$("#weekfun-car-type-cont").hide();
					$("#weekfun-car-type").hide();
				}
				
			});
			$("input[name='fundistance']").change(function(){
				fundistance=this.value;
			});
			$("#office-car-type").change(function () {
				officecartype=this.value;
			});
			$("#weekfun-car-type").change(function () {
				weekfuncartype=this.value;
			});
			$("#lpg").change(function () {
				lpg=this.value;
			});
			
			$("input[name='units']").change(function(){
				units=this.value;
			});
			
			
			slimScroll_item=$("#step-container").slimScroll({
				  color: '#4ca300',
				  size: '10px',
				  height: '160px',
				  alwaysVisible: true,
				  railVisible: true,
				  railColor: '#222',
				  scrollTo:"0px"
			  });
			  
			reset_vars();
			
			
			
		});
		
	</script>
</body>
</html>