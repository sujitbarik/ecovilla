<?php

namespace backend\controllers;

use Yii;
use common\models\Farm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Image;
use common\models\StockFarm;
use common\models\FarmImage;
use common\models\Size;
use common\models\FarmStockTransaction;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
/**
 * FarmController implements the CRUD actions for Farm model.
 */
class FarmController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Farm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Farm::find()->joinWith(['size'])->where(['Farm.IsDelete'=>0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Farm model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Farm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Farm();
        $FarmImage=new FarmImage();
        $size=ArrayHelper::map(Size::find()->where(['IsDelete'=>0])->all(),'SizeID','Size');

        if ($model->load(Yii::$app->request->post())) {
            //soil report
            $imagemodel=new Image();
            $sitelogo = UploadedFile::getInstance($model, 'SoilReport');
            if($sitelogo!='')
            {
                $image_id=$imagemodel->pdfUpload($sitelogo,'SoilReport');
				$model->SoilReport=$image_id;
            }
            else
            {
                $model->SoilReport=0;
            }
            
            $model->OnDate=date('Y-m-d H:i:s');
			
			
            $model->AvailableStock=$model->Stock;
            if($model->save())
            {
				/*farm stock management*/
				$stocktransaction = new FarmStockTransaction();
				$stocktransaction->FarmID = $model->FarmID;
				$stocktransaction->StockQTY = $model->Stock;
				$stocktransaction->Type = 0; /*0->Initial Stock Add  1-> Stock Update  2-> Farm Rent*/
				$stocktransaction->UpdateDate = date("Y-m-d H:i:s");
				$stocktransaction->save();
				
				
				for ($x = 0; $x < $model->Stock; $x++) {
					$stocksize = new StockFarm();
					$stocksize->FarmID =  $model->FarmID;
					$stocksize->SizeID =  $model->Size;
					$stocksize->save();
				}
               
                    if($FarmImage->load(Yii::$app->request->post()))
                        {
                            $FarmImage->ImageID = UploadedFile::getInstances($FarmImage,'ImageID');
                            foreach ($FarmImage->ImageID as $file)
                            {
                                $ppimodel=new Image();
                                $ppimageid=$ppimodel->imageUpload($file,'FarmImage');
                                
                                $Farmi=new FarmImage();
                                $Farmi->FarmID=$model->FarmID;
								
                                $Farmi->ImageID=$ppimageid;
                                $Farmi->OnDate=date('Y-m-d H:i:s');
                                if($Farmi->save())
                                {
                                    
                                }
                                else
                                {
                                     $i=1;
                                }
                                //var_dump($banner1->getErrors()).'<br/>';
                            }
                        }
                //farm image
                Yii::$app->session->setFlash('success', 'Farm added successfully');
            }
            else
            {
                var_dump($model->getErrors());
                Yii::$app->session->setFlash('error', 'There is some problem, please try again');
            }
            
        }

        return $this->render('create', [
            'model' => $model,'FarmImage'=>$FarmImage,'size'=>$size
        ]);
    }

    /**
     * Updates an existing Farm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldsoilreport=$model->SoilReport;
        $FarmImage=new FarmImage();
        $size=ArrayHelper::map(Size::find()->where(['IsDelete'=>0])->all(),'SizeID','Size');
		$intstock = $model->Stock;
		$farmavailablestock = $model->AvailableStock;
        if ($model->load(Yii::$app->request->post())) {
		
			$updatestock = Yii::$app->request->post()['Farm']['Stock'];
			if($updatestock  > 0){
			    $lateststock =  $updatestock + $farmavailablestock;
				$stock = $intstock + $updatestock;
			}else{
				$lateststock =  $farmavailablestock - $updatestock;
				$stock = $intstock - $updatestock;
			}
			
			if($updatestock>0){
				
				/*Update Latest Stock in the farmstock..*/
				for ($x = 1; $x <= $updatestock; $x++) {
					
					$stocksize = new StockFarm();
					$stocksize->FarmID =  $model->FarmID;
					$stocksize->SizeID =  $model->Size;
					$stocksize->save();
				}
				
				
				$imagemodel=new Image();
				$sitelogo = UploadedFile::getInstance($model, 'SoilReport');
				if($sitelogo!='')
				{
					$image_id=$imagemodel->pdfUpload($sitelogo,'SoilReport');
				}
				else
				{
					$image_id=$oldsoilreport;
				}

				$model->SoilReport=$image_id;
				$model->Stock=$stock;
				$model->AvailableStock=$lateststock;
				
				if($model->save())
				{
					/*farm stock management*/
					$stocktransaction = new FarmStockTransaction();
					$stocktransaction->FarmID = $id;
					$stocktransaction->StockQTY = $updatestock;
					$stocktransaction->Type = 1; /*0->Initial Stock Add  1-> Stock Update  2-> Farm Rent*/
					$stocktransaction->UpdateDate = date("Y-m-d H:i:s");
					$stocktransaction->save();
				
				
				
					if($FarmImage->load(Yii::$app->request->post()))
					{
						$FarmImage->ImageID = UploadedFile::getInstances($FarmImage,'ImageID');
						if($FarmImage->ImageID)
						{
							foreach ($FarmImage->ImageID as $file)
							{
								$ppimodel=new Image();
								$ppimageid=$ppimodel->imageUpload($file,'FarmImage');
								
								$Farmi=new FarmImage();
								$Farmi->FarmID=$model->FarmID;
								$Farmi->ImageID=$ppimageid;
								$Farmi->OnDate=date('Y-m-d H:i:s');
								if($Farmi->save())
								{
									
								}
								else
								{
									 $i=1;
								}
							}
						}
					}
					Yii::$app->session->setFlash('success', 'Farm updated successfully');
					
				}
				else
				{
					Yii::$app->session->setFlash('error', 'There is some problem, please try again');
				}
			}else{
				Yii::$app->session->setFlash('error', 'Stock cannot be negative or zero...Please enter the correct stock');
			}
			
        }

        return $this->render('update', [
            'model' => $model,'FarmImage'=>$FarmImage,'size'=>$size
        ]);
    }

    
    public function actionDelete1($id)
    {
        $model = $this->findModel($id);
		$allfarmimage = FarmImage::find()->where(['FarmID'=>$id])->all();
		if(count($allfarmimage)){
		FarmImage::deleteAll(['and','FarmID' => $id]);
		}
		$soilreportid = $model->SoilReport;
		if($soilreportid!=0){
			$imgdet = Image::find()->where(['ImageID'=>$soilreportid])->one();
			if(count($imgdet)){
				$imgdet->delete();
			}
		}
		$allfarmstockmng = FarmStockTransaction::find()->where(['FarmID'=>$id])->all();
		if(count($allfarmstockmng)){
			//FarmStockTransaction::updateAll(['IsDelete'=>1],"FarmID='$id'");
			FarmStockTransaction::deleteAll(['FarmID' => $id]);
		}
		$model->delete();
        return $this->redirect(['index']);
    }
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		$model->IsDelete = 1;
		if($model->save()){
			$allfarmstockmng = FarmStockTransaction::find()->where(['FarmID'=>$id])->all();
			if(count($allfarmstockmng)){
				foreach($allfarmstockmng as $stockfarmkey => $stockfarmvalue){
					$deletestockfarm = StockFarm::find()->where(['StockId'=>$stockfarmvalue->StockId])->one();
					$deletestockfarm->IsDelete = 1;
					$deletestockfarm->save();
				}
			}
			FarmStockTransaction::updateAll(['IsDelete' =>1], "FarmID =$id" );
			
			Yii::$app->session->setFlash('success', 'Farm deleted successfully');
		}else{
			Yii::$app->session->setFlash('error', 'There is some problem, please try again');
		}
        return $this->redirect(['index']);
    }
    public function actionFarmimagedelete()
    {
        $farmid=yii::$app->request->get()['farmid'];
        $farmimage=FarmImage::findOne($farmid)->delete();
        return 1;
    }

    /**
     * Finds the Farm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Farm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Farm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
