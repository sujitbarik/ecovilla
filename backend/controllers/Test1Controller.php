<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\CustomerRegistration;
use common\models\User;
use common\models\CustomerRent;
use common\models\CustomerMessage;
use common\models\Farm;
use common\models\Crops;
use common\models\CropEstimatedOutput;
use common\models\CropGuaranteeOutput;
use common\models\FarmImage;
use common\models\Image;
use common\models\Size;
use common\models\CropSizePrice;
use common\models\StockFarm;
use common\models\Transaction;
use common\models\CropsGrowthImage;
use common\models\TreeDetails;
use common\models\TreePackage;
use common\models\TreesImage;
use common\models\TreePlantation;
use common\models\UserFCMnotification;
use common\models\StockTransaction;
use common\models\Pages;
use common\models\Faq;
use common\models\TreePosition;

date_default_timezone_set("Asia/Calcutta");


class Test1Controller extends Controller{
	
   
    public function beforeAction($action){ 
		
		$this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
	
    public function behaviors(){
		
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'corsFilter' => [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                
                       'Origin' => ['*'],
                       'Access-Control-Request-Method' => ['POST', 'PUT','GET','REQUEST'],
                    ],

            ],
        ];
    }

    public function actionCustomerRegistration()
    {
        
		$this->layout='appblanklayout';
        $returnarray = array();
		$userdetails_array = array();
        $result=Yii::$app->request->post();
        
            $name = $result['name'];
            $email = $result['email'];
            $contact = $result['contactno'];
            $password = $result['password'];
			//$deviceid = $result['deviceid'];
			//if(isset($result['token'])){
			//	$token = $result['token'];
			//}else{
			//	$token = "";
			//}
            $hashpassword = Yii::$app->getSecurity()->generatePasswordHash($password);
            
            $chk_email = CustomerRegistration::find()->where(['EmailId'=>$email])->all();
            if(count($chk_email) == 0){
                
				$customerreg_obj = new CustomerRegistration();
                $customerreg_obj->Name = $name;
                $customerreg_obj->EmailId = $email;
                $customerreg_obj->ContactNo =  $contact;
                $customerreg_obj->Password =  $hashpassword;
                if($customerreg_obj->save())
                {
					
					//$new_userfcm = new UserFCMnotification();
					//$new_userfcm->CustomerID = $customerreg_obj->Id;
					//$new_userfcm->DeviceID = $deviceid;
					//$new_userfcm->token = $token;
                   // if($new_userfcm->save()){
						$id = $customerreg_obj->Id;
						$userlist = CustomerRegistration::find()->where(['Id'=> $id])->asArray()->one();
						$userdetails_array['userid'] = (int)$userlist['Id'];
						$userdetails_array['name'] = $userlist['Name'];
						$userdetails_array['contact'] = $userlist['ContactNo'];
						$userdetails_array['email'] = $userlist['EmailId'];
						
						$status = 1;
						$msg = 'Customer registration successfull';
						$data =$userdetails_array;
					//}else{
					//	$status = 0;
					//	$msg = 'There is some errors while insert in the UserFCMnotification table';
					//	$data = array();
					//}
                }
                else
                {
                    $status = 0;
                    $msg = 'There is some errors. Please try after Sometimes';
                    $data = array();
                }
            }
            else{
                
                $status = 0;
                $msg = 'Email already exits';
                $data = (object)array();
				
            }


        
       
        $returnarray['status'] = $status;
        $returnarray['msg'] = $msg;
        $returnarray['data'] = $data;
        echo json_encode($returnarray);


    }
	
	public function actionCustomerLogin()
    {
		$this->layout='appblanklayout';
		$result_array = array();
        $result=Yii::$app->request->post();
		$userdetails_array = array();
		$emailid = $result['email'];
		$password = $result['password'];
		//if(isset($result['deviceid'])){
		//$deviceid = $result['deviceid'];
		//}else{
		//	$deviceid = '';
		//}
		//if(isset($result['token'])){
		//	$token = $result['token'];
		//}else{
		//	$token = "";
		//}
		//
		$userlist=CustomerRegistration::find()->where(['EmailId'=>$emailid])->one();
		if(count($userlist) == 1)
		{
			$oldhash=$userlist['Password'];
			if(Yii::$app->getSecurity()->validatePassword($password,$oldhash))
			{
				$status = "Sucess";
				$msg="Login Sucessfull";
				$userdetails_array['userid'] = $userlist['Id'];
				$userdetails_array['points'] = $userlist['Points'];
				$userdetails_array['name'] = $userlist['Name'];
				$userdetails_array['contact'] = $userlist['ContactNo'];
				$userdetails_array['email'] = $userlist['EmailId'];
				$data =$userdetails_array;
			}
			else{
				$status = "Fail";
				$msg = "Wrong Password...";
				$data = '';
			}
		}
		else{
			$status = "Fail";
			$msg = "Wrong Email Id..";
			$data = '';
		}
		$result_array['status'] = $status;
        $result_array['msg'] = $msg;  
        $result_array['data'] = $data;  
		echo json_encode($result_array);
	}

	public function actionFarm(){
		 $this->layout='appblanklayout';
		 $post=Yii::$app->request->post();
		 $userid = $post['userid'];
		 $returnarray = array();
		 $farmdata = array();
		 
		$userfarm = CustomerRent::find()->where(['CustomerId'=>$userid,'IsDelete'=>0])->asArray()->all();
		if(count($userfarm) > 0){
			foreach($userfarm as $key1 => $value1){
				$farmimg = array();
				$cropdetailss = array();
				$farmid = $value1['FarmId'];
				$farmdetails = Farm::find()->where(['FarmId'=>$farmid])->one();
				$farmdata[$key1]['farmid'] = (string)$farmdetails['FarmID'];
				//$farmid = $value1['FarmID'];
				$getallimage = FarmImage::find()->where(['FarmID'=>$farmid])->all();
				if(count($getallimage) > 0){
					foreach($getallimage as $key11=>$value11){
						$imageid = $value11->ImageID;
						$imagedetails = Image::find()->where(['ImageID'=>$imageid])->one();
						$image = $imagedetails['Image'];
						$farmimg[$key11]['image'] = "http://188.166.247.24/farm/backend/".$image;
					}
					$farmdata[$key1]['farmimage'] = $farmimg;
				}else{
					$farmdata[$key1]['farmimage'] = array();
				}
				
				
				
				$farmdata[$key1]['farmname'] = $farmdetails['FarmName'];
				$farmdata[$key1]['farmpoint'] = $farmdetails['Price'];
				$getsize = Size::find()->where(['SizeID'=>$farmdetails['Size']])->one();
				//$farmdata[$key1]['sizeid'] = $farmdetails['Size'];
				$farmdata[$key1]['farmsize'] = $getsize['Size']." Sq. Ft.";
				$soilreportfile = Image::find()->where(['ImageID'=>$farmdetails['SoilReport']])->one();
				$farmdata[$key1]['report'] = "http://188.166.247.24/farm/backend/".$soilreportfile['Image'];
				$farmdata[$key1]['soilreportid'] = $farmdetails['SoilReport'];
				$farmdata[$key1]['location'] = $farmdetails['Location'];
				$farmdata[$key1]['duration'] = $farmdetails['Duration'];
				$farmdata[$key1]['availablestock'] = $farmdetails['AvailableStock'];
				$farmdata[$key1]['rentid'] = (int)$value1['RentId'];
				$farmdata[$key1]['farmstockid'] = $value1['FarmStockID'];
				if($value1['CropId']!=0){
					
					$cropsizedata = CropSizePrice::find()->where(['Id'=>$value1['CropId']])->one();
					$cropid = $cropsizedata['CropID'];
					$cropdetails = Crops::find()->where(['CropID'=>$cropid])->one();
					$cropdetailss['cropname'] = $cropdetails['CropName'];
					$cropdetailss['cropid'] = $cropdetails['CropID'];
					$cropdetailss['cropscientificname'] = $cropdetails['ScientificName'];
					$cropdetailss['cropmaturitytime'] = $cropdetails['MaturityTime'];
					$cropimageid = $cropdetails['CropImage'];
					$cropimagedetails = Image::find()->where(['ImageID'=>$cropimageid])->one();
					$cropdetailss['cropimage'] = "http://188.166.247.24/farm/backend/".$cropimagedetails['Image'];
					$cropdetailss['cropplantingdays'] = $cropdetails['PlantationDays'];
					$cropdetailss['cropplantingprice'] = $cropsizedata['PlantingPrice'];
					
				}
				else{
					$cropdetailss = (object)array();
				}
				$farmdata[$key1]['cropdetails'] = $cropdetailss;
			}
			
			$status = 1;
		    $msg = "Success";
		    $data = $farmdata;
			
		}
		else{
			//$farmdata = array();
			 $status = 0;
			 $msg = "Fail";
			 $data = array();
		}
		
		 $returnarray['status'] = $status; 
		 $returnarray['msg'] = $msg; 
		 $returnarray['data'] = $data; 
				
		 return json_encode($returnarray);
	}
	
	public function actionFarmlist(){
		$this->layout='appblanklayout';
        $returnarray = array();
        $result=Yii::$app->request->post();
		$farmdetails = array();
		
		$allfarmlist = Farm::find()->where(['IsDelete'=>0])->asArray()->all();
		if(count($allfarmlist)>0){
	
			foreach($allfarmlist as $key1=>$value1){
				$farmimage = array();
				//echo $value1['FarmID'];
				$farmdetails[$key1]['farmid'] = $value1['FarmID'];
				$farmdetails[$key1]['farmname'] = $value1['FarmName'];
				$farmdetails[$key1]['sizeid'] = $value1['Size'];
				$getsize = Size::find()->where(['SizeID'=>$value1['Size']])->one();
				$farmdetails[$key1]['farmsize'] = $getsize['Size'];
				$farmdetails[$key1]['farmpoint'] = $value1['Price'];
				$farmdetails[$key1]['soilreportid'] = $value1['SoilReport'];
				$soilreportfile = Image::find()->where(['ImageID'=>$value1['SoilReport']])->one();
				$farmdetails[$key1]['report'] = "http://188.166.247.24/farm/backend/".$soilreportfile['Image'];
				$farmdetails[$key1]['location'] = $value1['Location'];
				$farmdetails[$key1]['duration'] = $value1['Duration'];
				
				$farmid = $value1['FarmID'];
				$getallimage = FarmImage::find()->where(['FarmID'=>$farmid])->all();
				if(count($getallimage) > 0){
					foreach($getallimage as $key11=>$value11){
						$imageid = $value11->ImageID;
						$imagedetails = Image::find()->where(['ImageID'=>$imageid])->one();
						$image = $imagedetails['Image'];
						$farmimage[$key11]['image'] = "http://188.166.247.24/farm/backend/".$image;
					}
					$farmdetails[$key1]['farmimage'] = $farmimage;
				}else{
					$farmdetails[$key1]['farmimage'] = array();
				}
				
				
				if($value1['AvailableStock']>0){
					$stockmsg = "InStock";
				}else{
					$stockmsg = "OutofStock";
				}
				$farmdetails[$key1]['availablestock'] = $value1['AvailableStock'];
				$farmdetails[$key1]['stockmsg'] = $stockmsg ;
				
			}
			
			$status = 1;
			$msg = "Success";
			$data = $farmdetails;
		}
		else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		$returnarray['status']= $status;
		$returnarray['msg']= $msg;
		$returnarray['data']= $data;
		return json_encode($returnarray);
		
	}
	
	public function actionFarmrent(){
		
		$this->layout='appblanklayout';
        $post=Yii::$app->request->post();
		$result = array();
		$return = array();
		$userid = $post['userid'];
		$farmid = $post['farmid'];
		if($farmid!=''){
		$cus_ptsdetails = CustomerRegistration::find()->where(['Id'=>$userid])->one();
		$avail_pts = $cus_ptsdetails['Points'];
		$frm_ptdetails = Farm::find()->where(['FarmID'=>$farmid])->one();
		$farm_pts = $frm_ptdetails['Price'];
		if($farm_pts <= $avail_pts){
			$farmstock = StockFarm::find()->where(['FarmID'=>$farmid,'Status'=>0])->orderBy(['StockId'=>SORT_ASC])->one();
			if(count($farmstock) == 1){
				$allotstock = $farmstock['StockId'];
				/* rent the perticular farm */
				$availablefarmstock = StockFarm::find()->where(['StockId'=>$allotstock])->one();
				$availablefarmstock->Status = 1;
				if($availablefarmstock->save()){
					
					$rentmodel = new CustomerRent();
					$rentmodel->CustomerId = $userid;
					$rentmodel->FarmId = $farmid;
					$rentmodel->BookingDate = date("Y-m-d H:i:s");
					$farmpoint = Farm::find()->where(['FarmID'=>$farmid])->one();
					$rentmodel->RentPoints = (string)$farmpoint['Price'];
					$rentmodel->FarmStockID = $availablefarmstock->StockId;
					
					if($rentmodel->save()){
						$transaction_obj = new Transaction();
						$transaction_obj->CustomerID = $userid;
						$customerdetails = CustomerRegistration::find()->where(['Id'=>$userid])->one();
						$opeingbalance = $customerdetails['Points'];
						$transaction_obj->OpeningBalance = $opeingbalance;
						$closingbalance = $opeingbalance - $farmpoint['Price'];
						$transaction_obj->ClosingBalance = $closingbalance;
						
						$details = "Opening Balance : ".$opeingbalance." . Closing Balance : ".$closingbalance." . Farm Rent : ".$farmpoint['Price']." on Date : ".date("d-m-Y H:i:s");
						$transaction_obj->Details = $details;
						
						if($transaction_obj->save())
						{
							/* Customer table Update */
							$customerdetailss = CustomerRegistration::find()->where(['Id'=>$userid])->one();
							$customerdetails->Points = $closingbalance;
							if($customerdetails->save())
							{
								$farmdetails = Farm::find()->where(['FarmID'=>$farmid])->one();
								$available_stock = $farmdetails['AvailableStock'];
								$updatestock = $available_stock - 1;
								$farmdetails->AvailableStock = $updatestock;
								if($farmdetails->save()){
									
									$result['rentid'] = $rentmodel['RentId'];
									$result['userid'] = $userid;
									$result['farmid'] = $farmid;
									$result['farmname'] = $farmdetails['FarmName'];
									$status = 1;
									$msg = "You have Successfully rent the Farm...";
									$data = $result;
								}
								else{
									$status = 0;
									$msg = "Error in Farm Stock update....";
									$result = array();
									$data = $result;
								}
							}
							else{
								$status = 0;
								$msg = "There is some error while update the Customer table Points Column...";
								$result = array();
								$data = $result;
							}
						}
						else{
							$status = 0;
							$msg = "There is some errors while data insert in the Transaction Table...";
							$result = array();
							$data = $result;
						}
						
					}
					else{
						$status = 0;
						$msg = "There is some error in CustomerRent insert....";
						$result = array();
						$data = $result;
					}
				}else{
					$status = 0;
					$msg = "There is some error in StockFarm insert...";
					$result = array();
					$data = $result;
				}
				
			}
			else{
				$status = 0;
				$msg = "Selected farm is out of stock";
				$result = array();
				$data = $result;
			}
		}else{
			$status = 0;
			$msg = "You dont have Sufficient balance to rent the Farm ...";
			$result = array();
			$data = $result;
		}
		}else{
			$status = 0;
			$msg = "Please select a Farm..";
			$result = array();
			$data = $result;
		}
		
		$return['status'] = $status;
		$return['msg'] = $msg;
		$return['data'] = $data;
		
		return json_encode($return);
	}
	
	public function actionFarmImage(){
		$this->layout='appblanklayout';
        $post=Yii::$app->request->post();
		$result = array();
		$response = array();
		$farmid = $post['farmid'];
		$getallimage = FarmImage::find()->where(['FarmID'=>$farmid])->all();
		if(count($getallimage) > 0){
			foreach($getallimage as $key1=>$value1){
				$imageid = $value1->ImageID;
				$imagedetails = Image::find()->where(['ImageID'=>$imageid])->one();
				$image = $imagedetails['Image'];
				$result[$key1]['image'] = "http://188.166.247.24/farm/backend/".$image;
			}
			$status = 1;
			$msg = "Success";
			$data = $result;
		}
		else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		$response['status'] = $status;
		$response['msg'] = $msg;
		$response['data'] = $data;
		
		return json_encode($response);
		
	}
	
	public function actionFarmCrops(){
		
		$this->layout='appblanklayout';
        $post=Yii::$app->request->post();
		$croplistdetails = array();
		$returnarray = array();
		$farmid =$post['farmid'];
		//$farmid = 2;
		$farmdetails = Farm::find()->where(['FarmID'=>$farmid])->one();
		$sizeid = $farmdetails['Size'];
		$allcrops = CropSizePrice::find()->where(['SizeID'=>$sizeid])->asArray()->all();
		$sizedetails = Size::find()->where(['SizeID'=>$sizeid])->one();
		
		if(count($allcrops) > 0){
			foreach($allcrops as $key1=>$value1){
				$cropid = $value1['CropID'];
				$cropdetails = Crops::find()->where(['CropID'=>$cropid])->asArray()->one();
				$croplistdetails[$key1]['cropsizepriceid'] = $value1['Id'];
				$croplistdetails[$key1]['cropname'] = $cropdetails['CropName'];
				$croplistdetails[$key1]['scientificName'] = $cropdetails['ScientificName'];
				$croplistdetails[$key1]['maturitytime'] = $cropdetails['MaturityTime'];
				$croplistdetails[$key1]['plantationdays'] = $cropdetails['PlantationDays'];
				$croplistdetails[$key1]['plantingprice'] = $value1['PlantingPrice'];
				$croplistdetails[$key1]['size'] = $sizedetails['Size']." Sq. ft.";
				$imageid = $cropdetails['CropImage'];
				$estimatedetails = CropEstimatedOutput::find()->where(['SizeID'=>$sizeid,'CropID'=>$cropid])->one();
				$croplistdetails[$key1]['estimatedoutput'] = $estimatedetails['Harvest']." KG";
				$guarentdetails = CropGuaranteeOutput::find()->where(['SizeID'=>$sizeid,'CropID'=>$cropid])->one();
				$croplistdetails[$key1]['guarentedoutput'] = $guarentdetails['Harvest']." KG";
				$imagedetails = Image::find()->where(['ImageID'=>$imageid])->one();
				$image = $imagedetails['Image'];
				$croplistdetails[$key1]['image'] = "http://ecovilla.in/".$image;
				
			}
			$status = 1;
			$msg = "Success";
			$data = $croplistdetails;
		}
		else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		$returnarray['status'] = $status;
		$returnarray['msg'] = $msg;
		$returnarray['data'] = $data;
		
		return json_encode($returnarray);
		
	}
	
	public function actionCropsrent(){
		
		$this->layout='appblanklayout';
        $post=Yii::$app->request->post();
		$result = array();
		$returnarray = array();
		$rentid = $post['rentid'];
		$cropsizepriceid = $post['cropsizepriceid'];
		if($cropsizepriceid!='' && $rentid!=''){
		$cropsizedetails = CropSizePrice::find()->where(['Id'=>$cropsizepriceid])->one();
		$sizeid = $cropsizedetails['SizeID'];
		$sizedetails = Size::find()->where(['SizeID'=>$sizeid])->one();
		$sizename = $sizedetails['Size'];
		
		$rentdetails = CustomerRent::find()->where(['RentId'=>$rentid])->one();
		$customerid = $rentdetails['CustomerId'];
		$cus_ptsdetails = CustomerRegistration::find()->where(['Id'=>$customerid])->one();
		$avail_pts = $cus_ptsdetails['Points'];
		if($avail_pts >= $cropsizedetails['PlantingPrice']){
			if(count($rentdetails) == 1){
				if(($rentdetails['CropId']==0) && ($rentdetails['CropStatus']==0)){
					$rentdetails->CropId = $cropsizepriceid;
					$rentdetails->CropPoints = $cropsizedetails['PlantingPrice'];
					$cropdetails = Crops::find()->where(['CropID'=>$cropsizedetails['CropID']])->one();
					//$rentdetails->DaysToMature = $cropdetails['MaturityTime'];
					$rentdetails->CropStatus = 1;
					$rentdetails->CropBookingDate = date("Y-m-d H:i:s");
					$nextsunday = strtotime('next sunday');
                    $d1 = date("Y-m-d",$nextsunday); 
					$rentdetails->ExpectedPlantingDate = $d1;
					
					
					$rentdetails->Status = 1;
					
					if($rentdetails->save()){
						
						$transaction_obj = new Transaction();
						$transaction_obj->CustomerID = $customerid;
						$customerdetails = CustomerRegistration::find()->where(['Id'=>$customerid])->one();
						$opeingbalance = $customerdetails['Points'];
						$transaction_obj->OpeningBalance = $opeingbalance;
						$closingbalance = $opeingbalance - $cropsizedetails['PlantingPrice'];
						$transaction_obj->ClosingBalance = $closingbalance;
						
						$details = "Opening Balance : ".$opeingbalance." . Closing Balance : ".$closingbalance." . Crops Rent : ".$cropsizedetails['PlantingPrice']." on Date : ".date("d-m-Y H:i:s");
						
						$transaction_obj->Details = $details;
						if($transaction_obj->save())
						{
							$customerdetailss = CustomerRegistration::find()->where(['Id'=>$customerid])->one();
							$customerdetails->Points = $closingbalance;
							
							if($customerdetails->save())
							{
								$result['rentid'] = $rentdetails['RentId'];
								$result['cropsizepriceid'] = $rentdetails['CropId'];
								$cropname = Crops::getCropname($rentdetails['CropId']);
								$result['cropname'] = $cropname;
								$status = 1;
								$msg = "Your ".$cropname." has been planted on the desired area of ".$sizename." Square feet .You will be get update soon.";
								$data = $result;
							}else{
								$status = 0;
								$msg = "There is someerrors while updateing the Points on CustomerRegistration Table";
								$data = array();
							}
						}
						else{
							$status = 0;
							$msg = "There is some errors while data insert in the Transaction Table...";
							$data = array();
						}
						
					}else{
						$status = 0;
						$msg = "There is someerrors while updateing the CustomerRent Table";
						$data = array();
					}
				}else{
						$status = 0;
						$msg = "You have already rent...";
						$data = array();
					}
			}
			else{
				$status = 0;
				$msg = "There is some error please try aftersometime....";
				$data = array();
			}
		}else{
			$status = 0;
			$msg = "You dont have Sufficient balance to rent the Crops ...";
			$data = array();
		}
		}else{
			$status = 0;
			$msg = "Please send the required data for further step";
			$data = array();
		}
		$returnarray['status'] = $status;
		$returnarray['msg'] = $msg;
		$returnarray['data'] = $data;
		
		return json_encode($returnarray);
		
	}
	
	public function actionUserprofile(){
		$this->layout='appblanklayout';
        $post=Yii::$app->request->post();
		$result = array();
		$returnarray = array();
		$userid = $post['userid'];
		$customerdetails = CustomerRegistration::find()->where(['Id'=>$userid])->one();
		if(count($customerdetails)){
			$result['name'] = $customerdetails['Name'];
			$result['email'] = $customerdetails['EmailId'];
			$result['contactno'] = $customerdetails['ContactNo'];
			$result['points'] = $customerdetails['Points'];
			
			$status = 1;
			$msg = "Success";
			$data = $result;
		}
		else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		
		$returnarray['status'] = $status;
		$returnarray['msg'] = $msg;
		$returnarray['data'] = $data;
		return json_encode($returnarray);
	}
	
	public function actionAllcrops(){
		$this->layout='appblanklayout';
		$result_array = array();
		$jsonarray = array();
        $allcrops = Crops::find()->where(['IsDelete'=>0])->all();
		
		if(count($allcrops)){
			foreach($allcrops as $key=>$value){
				$sizedetails = array();
				$result_array[$key]['cropname'] = $value->CropName;
				$result_array[$key]['cropid'] = $value->CropID;
				$result_array[$key]['cropscientificname'] = $value->ScientificName;
				$result_array[$key]['cropmaturitytime'] = $value->MaturityTime;
				$imagepath = "http://188.166.247.24/farm/backend/".Image::getImagepath($value->CropImage);
				$result_array[$key]['cropimage'] = $imagepath ;
				$cropsizedetails = CropSizePrice::find()->where(['CropID'=>$value->CropID])->asArray()->all();
				foreach($cropsizedetails as $key1=>$value1){
					$sizedetails[$key1]['size'] = Size::getSize($value1['SizeID'])." Sq. Ft";
					$sizedetails[$key1]['plantingcost'] = $value1['PlantingPrice'];
					$estimateddetails = CropEstimatedOutput::find()->where(['CropID'=>$value->CropID,'SizeID'=>$value1['SizeID']])->one();
					$guarenteddetails = CropGuaranteeOutput::find()->where(['CropID'=>$value->CropID,'SizeID'=>$value1['SizeID']])->one();
					
					$sizedetails[$key1]['plantingcost'] = $value1['PlantingPrice'];
					$sizedetails[$key1]['estimatedoutput'] = $estimateddetails['Harvest']." KG";
					$sizedetails[$key1]['guarentedoutput'] = $guarenteddetails['Harvest']." KG";
				}
				$result_array[$key]['sizedetails'] = $sizedetails;
			}
			
			$status = 1;
			$msg = "Success";
			$data = $result_array;
		}
		else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		$jsonarray['status']=$status;
		$jsonarray['msg']=$msg;
		$jsonarray['data']=$data;
	    return json_encode($jsonarray);
	}
	
	public function actionAlltrees(){
		$this->layout='appblanklayout';
		$result_array = array();
		$json_array = array();
		
		$treeslist = TreeDetails::find()->where(['Isdelete'=>0])->asArray()->all();
		if(count($treeslist)){
			foreach($treeslist as $key=>$value){
				$imagearray = array();
				$response = array();
				$result_array[$key]['treename'] = $value['TreeName'];
				$result_array[$key]['treeid'] = $value['ID'];
				$result_array[$key]['treedescription'] = html_entity_decode(strip_tags($value['TreeDescription']),ENT_QUOTES);
				$result_array[$key]['plantingcostperunit'] = $value['PlantingCost'];
				$result_array[$key]['monthlyRecuringcostperunit'] = $value['MonthlyRecuringCost'];
				$result_array[$key]['monthlyRecuringcostperunit'] = $value['MonthlyRecuringCost'];
				
				$alltreeimage = TreesImage::find()->where(['TDID'=>$value['ID']])->all();
				if(count($alltreeimage)){
					foreach($alltreeimage as $keyimg => $valueimg){
						$imagearray[$keyimg]['image'] = "http://188.166.247.24/farm/frontend/web/".$valueimg['Image'];
					} 
				}
				else{
					$imagearray = array();
				}
				$result_array[$key]['treeimage'] = $imagearray;
				$packagedetails = TreePackage::find()->where(['TDID'=>$value['ID']])->asArray()->all();
				if(count($packagedetails)){
					foreach($packagedetails as $key1=>$value2){
						$response[$key1]['packagename']=$value2['PackageName'];
						$response[$key1]['pkgid']=$value2['PKGID'];
						$response[$key1]['pkgduration']=$value2['Duration'];
						$response[$key1]['pkgtreesqty']=$value2['Quantity'];
						$response[$key1]['pkgplantitingcost']=$value2['PlantingCost'];
						$response[$key1]['pkgmonthlyRecuringcost']=$value2['MonthlyRecuringCost'];
						$response[$key1]['pkgcost']=$value2['PackageCost'];
					}
				}else{
					$response = array();
				}
				$result_array[$key]['pkgdetails'] = $response;
				
			}
			$status = 1;
			$msg = "Success";
			$data = $result_array;
		}
		else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		
		$json_array['status'] = $status;
		$json_array['msg'] = $msg;
		$json_array['data'] = $data;
		return json_encode($json_array);
		
	}
	public function actionTreesrent()
    {
		$this->layout='appblanklayout';
		$postdata = Yii::$app->request->post();
		$result = array();
		$result_json = array();
		$customerid = $postdata['userid'];
		$pkgid = $postdata['pkgid'];
		$customerdetails = CustomerRegistration::find()->where(['Id'=>$customerid])->one();
		if(count($customerdetails)){
			$customerpoints = $customerdetails['Points'];
			$pkgdetails = TreePackage::find()->where(['PKGID'=>$pkgid])->one();
			$treesdetails = TreeDetails::find()->where(['ID'=>$pkgdetails['TDID']])->one();
			
            $customerpoint=$customerdetails->Points;
            
			if(count($pkgdetails))
            {
			   $pkgcost = $pkgdetails['PackageCost'];
               
               if($customerpoint>=$pkgcost)
               {
                    $treeplanting_obj = new TreePlantation();
                    $treeplanting_obj->TPID = $pkgid;
                    $treeplanting_obj->CustomerID = $customerid;
                    //$treeplanting_obj->PlantingStatus = 0;
                    $treeplanting_obj->AddedDate = date("Y-m-d H:i:s A");
                    $nextsunday = strtotime('next sunday');
                    $d1 = date("Y-m-d",$nextsunday); 
                    $treeplanting_obj->ExceptPlantingDate = $d1;
                    
                    if($treeplanting_obj->save())
                    {
                        
                        $transaction_obj = new Transaction();
                        $opeingbalance = $customerpoints;
                        $transaction_obj->OpeningBalance = $opeingbalance;
                        $transaction_obj->CustomerID = $customerid;
                        $closingbalance = $opeingbalance - $pkgcost;
                        $transaction_obj->ClosingBalance = $closingbalance;
                        $details = "Opening Balance : ".$opeingbalance." . Closing Balance : ".$closingbalance." . Trees Direct Planting Cost : ".$pkgcost." on Date : ".date("d-m-Y H:i:s")." . Package Name :".$pkgdetails['PackageName'];
                        $transaction_obj->Details = $details;
                        
                        if($transaction_obj->save())
                        {
                            $cust_details = CustomerRegistration::find()->where(['Id'=>$customerid])->one();
                            $cust_details->Points = $closingbalance;
                            if($cust_details->save())
                            {
                                 
                                 $treedetailmodel=TreeDetails::findOne($pkgdetails['TDID']);
                                 $treedetailmodel->Stock=$treesdetails->Stock-1;
                                 if($treedetailmodel->save())
                                 {
                                        $stocktrans=new StockTransaction();
                                        $stocktrans->TreeDetailsId=$pkgdetails['TDID'];
                                        $stocktrans->Stock=-1;
                                        $stocktrans->AddedDate=date("Y-m-d H:i:s");
                                        if($stocktrans->save())
                                        {
                                            $status = 1;
                                            $CustomerMessage = CustomerMessage::find()->where(['Type'=>1])->orderBy(['rand()' => SORT_DESC])->one();
                                            //$msg = "Thank You for plant the ".$treesdetails['TreeName'];
                                            $msg = $CustomerMessage['Details'];
                                            $result['treeplantid'] = $treeplanting_obj['ID'];
                                            $result['treename'] = $treesdetails['TreeName'];
                                            $data = $result;
                                        }
                                        else
                                        {
                                            $status = 0;
                                            $msg = "There is some error in stock entry in Stock transaction table";
                                            $data = array();
                                        }  
                                        
                                 }
                                 else
                                 {
                                        $status = 0;
                                        $msg = "There is some error in stock update in Treedetails table";
                                        $data = array();
                                 }
                                
                                 
                            }
                            else
                            {
                                 $status = 0;
                                 $msg = "There is some while update the Customer Registration table";
                                 $data = array();
                            }
                        }
                        else
                        {
                                 $status = 0;
                                 $msg = "There is some while update the Transaction table";
                                 $data = array();
                        }
                        
                    }
                    else
                    {
                             $status = 0;
                             $msg = "There is some while insert the TreePlantation table";
                             $data = array();
                    }
               }
               else
               {
                              $status = 0;
                              $msg = "Insufficient balance for rent.";
                              $data = array();
               }
			   
			}
			else
            {
					$status = 0;
					$msg = "Please choose the package";
					$data = array();
			}
		}else{
			$status = 0;
			$msg = "Please choose the Customer data";
			$data = array();
		}
		$result_json['status'] = $status;
		$result_json['msg'] = $msg;
		$result_json['data'] = $data;
		return json_encode($result_json);
	}
	
	public function actionCustomerrenttree(){
		
		$this->layout='appblanklayout';
		$postdata = Yii::$app->request->post();
		$customerid = $postdata['userid'];
		$details = array();
		$jsonarray = array();
		
		$rentdetails = TreePlantation::find()->where(['CustomerID'=>$customerid])->all();
		if(count($rentdetails)){
			foreach($rentdetails as $key=>$value){
				$imagearray = array();
				$packagedetails = TreePackage::find()->where(['PKGID'=>$value['TPID']])->one();
				$treesdetails = TreeDetails::find()->where(['ID'=>$packagedetails['TDID']])->one();
				$details[$key]['treename'] = $treesdetails['TreeName'] ;
				//$details[$key]['treedescription'] = $treesdetails['TreeDescription'] ;
				$details[$key]['treedescription'] = html_entity_decode(strip_tags($treesdetails['TreeDescription']),ENT_QUOTES);
				
				$alltreeimage = TreesImage::find()->where(['TDID'=>$treesdetails['ID']])->all();
				if(count($alltreeimage)){
					foreach($alltreeimage as $keyimg => $valueimg){
						$imagearray[$keyimg] = "http://188.166.247.24/farm/backend/".$valueimg['Image'];
					} 
				}
				else{
					$imagearray = array();
				}
				$details[$key]['packagename'] = $packagedetails['PackageName'] ;
				$details[$key]['packageid'] = $packagedetails['PKGID'] ;
				$details[$key]['packageduration'] = $packagedetails['Duration'] ;
				$details[$key]['plantingcost'] = $packagedetails['PlantingCost'] ;
				$details[$key]['monthlyrecursivecost'] = $packagedetails['MonthlyRecuringCost'] ;
				$details[$key]['packagecost'] = $packagedetails['PackageCost'] ;
				$details[$key]['dateofplantation'] = $value->PlantingDate ;
				$details[$key]['plantid'] = $value->ID ;
				
			}
			
			$status = 1;
			
			$msg = "Success";
			$data = $details;
		}
		else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		$jsonarray['status'] = $status;
		$jsonarray['msg'] = $msg;
		$jsonarray['data'] = $data;
		
		return json_encode($jsonarray);
	}
	public function actionPage(){
		$this->layout='appblanklayout';
		$result_array = array();
		$result = array();
		$pages = Pages::find()->where(['Isdelete'=>0])->one();
		if(count($pages)){
			$result_array['pagename'] = $pages['PageName'];
			$result_array['pagecontents'] =  html_entity_decode(strip_tags($pages['PageContents']),ENT_QUOTES);
			$status = 1;
			$msg = "Page Available";
			$data = $result_array;
		}else{
			$status =0;
			$msg = "Page Not Available..";
			$data = array();
		}
		$result['status'] = $status;
		$result['msg'] = $msg;
		$result['data'] = $data;
		return json_encode($result);
	}
	public function actionTreepackage(){
		$this->layout='appblanklayout';
		$postdata = Yii::$app->request->post();
		$treeid = $postdata['treeid'];
		
		$res = array();
		$response = array();
		$packagedetails = TreePackage::find()->where(['TDID'=>$treeid])->asArray()->all();
		if(count($packagedetails)){
			foreach($packagedetails as $key1=>$value1){
				$res[$key1]['packagename'] = $value1['PackageName'];
				$res[$key1]['packagecost'] = $value1['PackageCost'];
				//$res[$key1]['packageaddeddate'] = $value1['PackageName'];
			}
			$status = 1;
			$msg = "Package Available";
			$data = $res;
		}else{
			$status = 0;
			$msg = "Package Not Available";
			$data = array();
		}
		$response['status'] = $status;
		$response['msg'] = $msg;
		$response['data'] = $data;
		
		return json_encode($response);
		
	}
	
	public function actionCropgrowth(){
		$this->layout='appblanklayout';
        $post=Yii::$app->request->post();
		$result = array();
		$returnarray = array();
		$rentid = $post['rentid'];
		$croprentdetails = CustomerRent::find()->where(['RentID'=>$rentid])->one();
		if(count($croprentdetails)){
			$cropimage = array();
			$result['rentid'] = $rentid;
			$croppricesizeid = $croprentdetails['CropId'];
			$croppricesizedetails = CropSizePrice::find()->where(['Id'=>$croppricesizeid])->one();
			$sizeid = $croppricesizedetails['SizeID'];
			$cropid = $croppricesizedetails['CropID'];
			$estdetails = CropEstimatedOutput::find()->where(['CropID'=>$cropid,'SizeID'=>$sizeid])->one();
			$guaoutput = CropGuaranteeOutput::find()->where(['CropID'=>$cropid,'SizeID'=>$sizeid])->one();
			
			$result['customername'] = CustomerRegistration::getCustomername($croprentdetails['CustomerId']);
			$result['cropname'] = Crops::getCropname($croprentdetails['CropId']);
            $result['cropscname'] = Crops::getCropscname($croprentdetails['CropId']);
			$result['farmbookingdate'] = date("d-m-Y h:i:s",strtotime($croprentdetails['BookingDate']));
			$result['farmcost'] = $croprentdetails['RentPoints'];
			$result['cropcost'] = $croprentdetails['CropPoints'];
			$result['cropplantingcost'] = $croppricesizedetails['PlantingPrice'];
			$result['estimatedoutput'] = $estdetails['Harvest']." K.G.";
			$result['guarentedoutput'] = $guaoutput['Harvest']." K.G.";
			$result['cropbookingdate'] = date("d-m-Y h:i:s",strtotime($croprentdetails['CropBookingDate']));
			$result['daytomature'] = $croprentdetails['DaysToMature'];
			
			$growthimage = CropsGrowthImage::find()->where(['RentId'=>$rentid])->orderBy(['Ondate'=>SORT_DESC])->one();
			$result['growthimage'] = "http://188.166.247.24/farm/backend/".$growthimage->Image; ;
			
			$status = 1;
			$msg = "Success";
			$data = $result;
		}else{
			$status = 0;
			$msg = "Fail";
			$data = array();
		}
		$returnarray['status'] = $status;
		$returnarray['msg'] = $msg;
		$returnarray['data'] = $data;
		
		return json_encode($returnarray);
	}
	
	public function actionTreeGrowth(){
		$this->layout='appblanklayout';
        $post=Yii::$app->request->post();
		$result = array();
		$growth_details = array();
		$response_array = array();
		$rentid = $post['plantid'];
		//$rentid = 1;
		$rentdetails = TreePlantation::find()->where(['ID'=>$rentid])->asArray()->one();
		//echo"<pre>"; var_dump($rentdetails);
		if(count($rentdetails)){
			$treepackageid = $rentdetails['TPID'];
			$treepackagedetails = TreePackage::find()->where(['PKGID'=>$treepackageid])->one();
			
			$result['treename'] = TreePackage::getTreename($treepackageid);
			$result['packagename'] = TreePackage::getTreepackage($treepackageid);
			$result['packagecost'] = $treepackagedetails['PackageCost'];
			$result['packageduration'] = $treepackagedetails['Duration']." Months";
			$result['bookinddata'] = date("d-m-Y",strtotime($rentdetails['AddedDate']));
			$result['plantingdate'] = date("d-m-Y H:i:s A",strtotime($rentdetails['PlantingDate']));
			$growthdetails = CropsGrowthImage::find()->where(['TreePlantID'=>$rentid])->all();
			
			if(count($growthdetails)){
				foreach($growthdetails as $key => $value){
					if($value->StatusType == "Image"){
						$growth_details[$key]['description'] = "http://188.166.247.24/farm/backend/".$value->Image;
					}else{
						$growth_details[$key]['description'] = $value->Description;
					}
					
				}
			}else{
				$growthdetails = array();
			}
			$result['growthdetails'] = $growth_details;
			
			$status = 1;
			$msg = "Record available..";
			$data = $result;
		}else{
			$status = 0;
			$msg = "Record not available..";
			$data = array();
		}
		
		$response_array['status'] = $status;
		$response_array['msg'] = $msg;
		$response_array['data'] = $data;
		return json_encode($response_array);
	}
	
	
    
    public function actionCustomertransaction()
    {
        $this->layout='appblanklayout';
        $result = array();
        $response_array = array();
        $customerid=Yii::$app->request->post('customerid');
		$alltransactionlist = Transaction::find()->where(['CustomerID'=>$customerid])->orderBy(['Id'=>SORT_DESC])->all();
		if(count($alltransactionlist)>0)
        {
			foreach($alltransactionlist as $key=>$value)
            {
				$result[$key]['OpeningBalance'] = $value->OpeningBalance;
				$result[$key]['ClosingBalance'] = $value->ClosingBalance;
				$result[$key]['Details'] = $value->Details;
                $result[$key]['Ondate'] = $value->Ondate;
			}
			$status = 1;
			$msg = "Record Available";
			$data = $result;
		}
        else
        {
			$status = 0;
			$msg = "Record Not Available";
			$data = array();
		}
		$response_array['status'] = $status;
		$response_array['msg'] = $msg;
		$response_array['data'] = $data;
		
		return json_encode($response_array);
	}
    
    
    public function actionFcmtokeninsert()
    {
        
        $this->layout='appblanklayout';
        $result=Yii::$app->request->post();
        $deviceid = $result['deviceid'];
        $userid = $result['userid'];
        $fcmtoken = $result['fcmtoken'];
        
        $response_array = array();
        
        $chk_fcmtoken = UserFCMnotification::find()->where(['DeviceID'=>$deviceid,'CustomerID'=>$userid,'IsDelete'=>0])->one();
		if(count($chk_fcmtoken) == 0)
        {
				$userfcmtoken_obj = new UserFCMnotification();
				$userfcmtoken_obj ->token = $fcmtoken;
				$userfcmtoken_obj ->CustomerID = $userid;
				$userfcmtoken_obj ->DeviceID = $deviceid;
                $userfcmtoken_obj ->Ondate = date('Y-m-d H:i:s');
                if($userfcmtoken_obj->save())
                {
                    $status = 1;
			        $msg = "Sucess";
                }
                else
                {
                    $status = 0;
			        $msg = "Fail";
                }
				
		}
		else
        {
				$model= new UserFCMnotification();
				$res=$model->updateAll(['Token'=>$fcmtoken],"DeviceId='$deviceid'");
                if($res)
                {
                    $status = 1;
			        $msg = "Sucess";
                }
                else
                {
                    $status = 0;
			        $msg = "Fail";
                }
				
		}
         
        $response_array['status'] = $status;
		$response_array['msg'] = $msg;
		
        return json_encode($response_array);
        
    }
    
    public function actionFaq()
    {
        $this->layout='appblanklayout';
        $result = array();
        $response_array = array();
		$allfaq = Faq::find()->where(['IsDelete'=>0])->orderBy(['OnDate'=>SORT_DESC])->all();
		if(count($allfaq)>0)
        {
			foreach($allfaq as $key=>$value)
            {
				$result[$key]['Question'] = $value->Question;
				$result[$key]['Answer'] = $value->Answer;
			}
			$status = 1;
			$msg = "Record Available";
			$data = $result;
		}
        else
        {
			$status = 0;
			$msg = "Record Not Available";
			$data = array();
		}
		$response_array['status'] = $status;
		$response_array['msg'] = $msg;
		$response_array['data'] = $data;
		
		return json_encode($response_array);
	}
    
    public function actionTreeposition()
    {
        $this->layout='appblanklayout';
        $userid=Yii::$app->request->post('userid');
        $postition=Yii::$app->request->post('postition');
        $response_array = array();
        
        $position = TreePosition::find()->where(['UserId'=>$userid,'IsDelete'=>0])->one();
		if(count($position)==0)
        {
            
            $exp=explode('@',$postition);
            foreach($exp as $k=>$v)
            {
                if($v!='')
                {
                    $exx=explode('|',$v);
                    $posn=explode(',',$exx[1]);
                    $model=new TreePosition();
                    $model->UserId=$userid;
                    $model->TreeId=$exx[0];
                    $model->TopPosition=$posn[0];
                    $model->LeftPosition=$posn[1];
                    $model->Type=$posn[2];
                    $model->FarmId=$posn[3];
                    $model->OnDate=date('Y-m-d H:i:s');
                    if($model->save())
                    {
                      $status = 1;
                      $msg = "Success";
                    }
                    else
                    {
                        $status = 0;
                        $msg = "Fail";
                    }
                }
                
            }
            
        }
        else
        {
            
            TreePosition::deleteAll(['UserId'=>$userid,'IsDelete'=>0]);
            $exp=explode('@',$postition);
            foreach($exp as $k=>$v)
            {
                if($v!='')
                {
                    $exx=explode('|',$v);
                    $posn=explode(',',$exx[1]);
                    $model=new TreePosition();
                    $model->UserId=$userid;
                    $model->TreeId=$exx[0];
                    $model->TopPosition=$posn[0];
                    $model->LeftPosition=$posn[1];
                    $model->Type=$posn[2];
                    $model->FarmId=$posn[3];
                    $model->OnDate=date('Y-m-d H:i:s');
                    if($model->save())
                    {
                      $status = 1;
                      $msg = "Success";
                    }
                    else
                    {
                        $status = 0;
                        $msg = "Fail";
                    }
                    
                }
                
            }
        }
        
        $response_array['status'] = $status;
		$response_array['msg'] = $msg;
		
		echo json_encode($response_array);
        
    }
    
    
	public function actionViewtreeposition()
    {
        $this->layout='appblanklayout';
        $result = array();
        $response_array = array();
        $userid=Yii::$app->request->post('userid');
		$position = TreePosition::find()->where(['UserId'=>$userid,'IsDelete'=>0])->all();
		if(count($position)>0)
        {
			foreach($position as $key=>$value)
            {
                $result[$key]['TreeId'] = $value->TreeId;
                $result[$key]['TopPosition'] = $value->TopPosition;
			    $result[$key]['LeftPosition'] = $value->LeftPosition;
            }
		    
			$status = 1;
			$msg = "Record Available";
			$data = $result;
		}
        else
        {
			$status = 0;
			$msg = "Record Not Available";
			$data = array();
		}
		$response_array['status'] = $status;
		$response_array['msg'] = $msg;
		$response_array['data'] = $data;
		
		echo json_encode($response_array);
	}
    
   public function actionViewtree()
    {
        $this->layout='appblanklayout';
        $result = array();
        $response_array = array();
        $userid=Yii::$app->request->post('userid');
		$alltree = TreePlantation::find()->where(['CustomerID'=>$userid,'Isdelete'=>0])->all();
        $allcrop = CustomerRent::find()->where(['CustomerId'=>$userid,'IsDelete'=>0])->andWhere(['!=','CropId',0])->all();
       
			foreach($alltree as $key=>$value)
            {
                $result['tree'][$key]['PlantId']=$value->ID;
                $result['tree'][$key]['TreeId'] = $value->treepckg->treedetail->ID;
                $result['tree'][$key]['TreeImage'] = $value->treepckg->treedetail->TreeImage;
                
                $position = TreePosition::find()->where(['TreeId'=>$value->ID,'IsDelete'=>0,'Type'=>'tree'])->all();
                if(count($position)>0)
                {
                    foreach($position as $k=>$v)
                    {
                        $result['tree'][$key]['position'][$k]['TopPosition'] = $v->TopPosition;
                        $result['tree'][$key]['position'][$k]['LeftPosition'] = $v->LeftPosition;
                    }
                }
                
            }
           
            foreach($allcrop as $crkey=>$crvalue)
            {
                $result['crop'][$crkey]['PlantId']=$crvalue->RentId;
                if(isset($crvalue->cropsize))
                {
                    $result['crop'][$crkey]['CropId'] = $crvalue->cropsize->crops->CropID;
                    $result['crop'][$crkey]['CropImage'] =$crvalue->cropsize->crops->CropAppImage;
                }
                
               

                
                $position1 = TreePosition::find()->where(['TreeId'=>$crvalue->RentId,'IsDelete'=>0,'Type'=>'crop'])->all();
                if(count($position1)>0)
                {
                    foreach($position1 as $k1=>$v1)
                    {
                        $result['crop'][$crkey]['position'][$k1]['TopPosition'] = $v1->TopPosition;
                        $result['crop'][$crkey]['position'][$k1]['LeftPosition'] = $v1->LeftPosition;
                    }
                }
                
            }
            
        if(!empty($result))
        { 
			$status = 1;
			$msg = "Record Available";
			$data = $result;
		}

        else
        {
			$status = 0;
			$msg = "Record Not Available";
			$data = array();
		}
		$response_array['status'] = $status;
		$response_array['msg'] = $msg;
		$response_array['data'] = $data;
		
		echo json_encode($response_array);
    }
	
}