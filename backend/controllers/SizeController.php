<?php

namespace backend\controllers;

use Yii;
use common\models\Size;
use common\models\StockFarm;
use common\models\FarmStockTransaction;
use common\models\Farm;
use common\models\CropEstimatedOutput;
use common\models\CropGuaranteeOutput;
use common\models\CropSizePrice;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SizeController implements the CRUD actions for Size model.
 */
class SizeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Size models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Size::find()->where(['IsDelete'=>0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Size model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Size model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Size();

        if ($model->load(Yii::$app->request->post())) {
            $model->OnDate=date('Y-m-d H:i:s');
            if($model->save())
            {
                Yii::$app->session->setFlash('success', 'Size added successfully');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'There is some problem, please try again');
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Size model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save())
            {
				
                Yii::$app->session->setFlash('success', 'Size updated successfully');
            }
            else
            {
                Yii::$app->session->setFlash('error', 'There is some problem, please try again');
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Size model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->IsDelete=1;
        if($model->save())
		{
			$croppricesize = CropSizePrice::find()->where(['SizeID'=>$id])->all();
			if(count($croppricesize)){
				foreach($croppricesize as $key => $value){
					$deletecropsizeprice = CropSizePrice::find()->where(['Id'=>$value->Id])->one();
					$deletecropsizeprice->IsDelete = 1;
					$deletecropsizeprice->save();
				}
			}
			$sizeest = CropEstimatedOutput::find()->where(['SizeID'=>$id])->all();
			if(count($sizeest)){
				foreach($sizeest as $key1 => $value1){
					$deletesizeest = CropEstimatedOutput::find()->where(['CEID'=>$value1->CEID])->one();
					$deletesizeest->IsDelete = 1;
					$deletesizeest->save();
				}
			}
			$sizeguarented = CropGuaranteeOutput::find()->where(['SizeID'=>$id])->all();
			if(count($sizeguarented)){
				foreach($sizeguarented as $key2 => $value2){
					$deletesizegaur = CropGuaranteeOutput::find()->where(['CGID'=>$value2->CGID])->one();
					$deletesizegaur->IsDelete = 1;
					$deletesizegaur->save();
				}
			}
			$sizefarm = Farm::find()->where(['Size'=>$id])->all();
			if(count($sizefarm)){
				foreach($sizefarm as $sizefarmkey => $sizefarmvalue){
					$deletefarm = Farm::find()->where(['FarmID'=>$sizefarmvalue->FarmID])->one();
					$deletefarm->IsDelete = 1;
					if($deletefarm->save()){
						
						FarmStockTransaction::updateAll(['IsDelete' =>1], "FarmID =$sizefarmvalue->FarmID" );
						
						$stockfarm = StockFarm::find()->where(['FarmID'=>$sizefarmvalue->FarmID])->all();
						if(count($stockfarm)){
							foreach($stockfarm as $stockfarmkey => $stockfarmvalue){
								$deletestockfarm = StockFarm::find()->where(['StockId'=>$stockfarmvalue->StockId])->one();
								$deletestockfarm->IsDelete = 1;
								$deletestockfarm->save();
							}
						}
					}
				}
			}
			Yii::$app->session->setFlash('success', 'Size deleted successfully');
		}
		else
		{
			Yii::$app->session->setFlash('error', 'There is some problem, please try again');
		}

        return $this->redirect(['index']);
    }

    /**
     * Finds the Size model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Size the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Size::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
