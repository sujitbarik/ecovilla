<?php

namespace backend\controllers;

use Yii;
use common\models\CustomerRegistration;
use common\models\CustomerRegistrationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Transaction;

/**
 * CustomerRegistrationController implements the CRUD actions for CustomerRegistration model.
 */
class CustomerRegistrationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerRegistration models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerRegistrationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerRegistration model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerRegistration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerRegistration();

        if ($model->load(Yii::$app->request->post())) {
			$password = Yii::$app->request->post()['CustomerRegistration']['Password']; 
			$hashpassword = Yii::$app->getSecurity()->generatePasswordHash($password);
			$model->Password = $hashpassword;
			$model->save();
            return $this->redirect(['view', 'id' => $model->Id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CustomerRegistration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			$password = Yii::$app->request->post()['CustomerRegistration']['Password']; 
			$hashpassword = Yii::$app->getSecurity()->generatePasswordHash($password);
			$model->Password = $hashpassword;
			$model->save();
            return $this->redirect(['view', 'id' => $model->Id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CustomerRegistration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		$model->Isdelete = 1;
		if($model->save()){
			Yii::$app->session->setFlash('success', 'Record deleted successfully');
		}else{
			Yii::$app->session->setFlash('error', 'There is some problem, please try again');
		}
        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerRegistration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerRegistration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerRegistration::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
	public function actionAddpoints(){
		$customerdetails = CustomerRegistration::find()->where(['IsDelete'=>0])->asArray()->all();
		return $this->render('customerpointlist', [
            'customerdetails' => $customerdetails,
        ]);
	}
	public function actionMorepoints(){
		$cid = Yii::$app->request->get()['cid'];
		$customerdetails = CustomerRegistration::find()->where(['Id'=>$cid,'IsDelete'=>0])->asArray()->one();
		echo "<table class='table'>";
		echo "<tr>";
		echo "<td> Available Points </td>";
		echo "<td>".$customerdetails['Points']."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>Type</td>";
		echo "<td>";
		echo "<select name='points_type' id='points_type'><option value='Deposite'>Deposite</option><option value='Withhdraw'>Withdraw</option></select>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>Description</td>";
		echo "<td>";
		echo "<textarea col='10' row='8' id='points_add_details'></textarea>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td> Points Add</td>";
		echo "<td><input type='text' name='cust_points' id='cust_points' /><input type='hidden' id='cid' value='".$customerdetails['Id']."'/></td>";
		echo "</tr>";
		echo "</table>";
		
	}
	
	public function actionPointsadd(){
		$response_array = array();
		$cid = Yii::$app->request->get()['cid'];
		$points = Yii::$app->request->get()['points'];
		$desc = Yii::$app->request->get()['pointdsc'];
		$pointstype = Yii::$app->request->get()['pointstype'];
		$customerdetails = CustomerRegistration::find()->where(['Id'=>$cid,'IsDelete'=>0])->one();
		$availablepts = $customerdetails['Points'];
		
		if($pointstype == 'Deposite'){
		    $updatepoints = $availablepts + $points;
		}else{
			if($availablepts > $points){
			  $updatepoints = $availablepts - $points;
			}
			else{
				$points = 0;
				$updatepoints = $availablepts;
			}
		}
		if($points!=0){
			$customerdetails->Points = $updatepoints;
			if($customerdetails->save()){
				$transaction_obj = new Transaction();
				$transaction_obj->CustomerID = $cid;
				$opeingbalance = $availablepts;
				$transaction_obj->OpeningBalance = $opeingbalance;
				$transaction_obj->ClosingBalance = $updatepoints;
				if(trim($desc!='')){
					//$details = "Opening Balance : ".$opeingbalance." . Closing Balance : ".$updatepoints." . Points Add : ".$points." on Date : ".date("d-m-Y H:i:s")." .Description: ".$desc;
                    $details = $desc;
				}
				else{
					//$details = "Opening Balance : ".$opeingbalance." . Closing Balance : ".$updatepoints." . Points Add : ".$points." on Date : ".date("d-m-Y H:i:s");
                    $details='';
				}
				//$details = "Opening Balance : ".$opeingbalance." . Closing Balance : ".$updatepoints." . Points Add : ".$points." on Date : ".date("d-m-Y H:i:s");
				
				$transaction_obj->Details = $details;
				$transaction_obj->save();
				
				$status = 1;
				$msg ="Points Successfully updated..";
				
			}
			else{
				$status = 0;
				$msg ="There is some error while updating the Customer Points..";
			}
		}else{
			$status = 0;
			$msg ="Please enter the correct withdraw points...";
		}
		
		$response_array['status'] = $status;
		$response_array['msg'] = $msg;
		echo json_encode($response_array);
	}
	
	
}
