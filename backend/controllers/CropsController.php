<?php

namespace backend\controllers;

use Yii;
use common\models\Crops;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Size;
use common\models\CropEstimatedOutput;
use common\models\CropGuaranteeOutput;
use yii\web\UploadedFile;
use common\models\Image;
use common\models\CropSizePrice;
use common\models\CustomerRent;
use common\models\CropsGrowthImage;

/**
 * CropsController implements the CRUD actions for Crops model.
 */
class CropsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Crops models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Crops::find()->where(['IsDelete'=>0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Crops model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Crops model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Crops();
		$imagemodel=new Image();
        $allsize = Size::find()->where(['IsDelete'=>0])->all();
        if ($model->load(Yii::$app->request->post()))
        {
            
			$cropImage = UploadedFile::getInstance($model, 'MemberImageId');
            if($cropImage!='')
			{
				$image_id=$imagemodel->imageUpload($cropImage,'Crop Image');
				$model->CropImage=$image_id;
            }
			else
			{
                $model->CropImage=0;
            }
			
            $image = UploadedFile::getInstance($model,'CropAppImage');
            $imgmodel=new Image();
			$crppimage=$imgmodel->imageUploads($image);
            $model->CropAppImage=$crppimage;
            
            if(isset(Yii::$app->request->post()['SizeID']) && (Yii::$app->request->post()['SizeID']!='')){
                $allsizeid = Yii::$app->request->post()['SizeID'];
                $allestimatedoutput = Yii::$app->request->post()['EstimatedOutput'];
                $allguarentedoutput = Yii::$app->request->post()['GuaranteedOutput'];
                $allplantingcost = Yii::$app->request->post()['PlantingCost'];                
                $estcnt = count($allestimatedoutput);
                $guarentedtcnt = count($allguarentedoutput);
                if($estcnt > 0 &&  $guarentedtcnt > 0)
                {
                    $model->OnDate = date("Y-m-d H:i:s");
                    if($model->save()){
                        foreach($allsizeid as $keysize=>$sizevalue){
							/* Estimated Insert */
							if( $allestimatedoutput[$keysize]!=''){
								$estimated = $allestimatedoutput[$keysize];
							}else{
								$estimated = '0';
							}
							$estmate_obj = new CropEstimatedOutput();
							$estmate_obj-> CropID = $model->CropID;
							$estmate_obj->SizeID = $sizevalue;
							$estmate_obj->Harvest = $estimated;
							$estmate_obj->OnDate = date('Y-m-d H:i:s');
							$estmate_obj->save();
                            /* Guarented Insert */
							if( $allguarentedoutput[$keysize]!=''){
								$guarented = $allguarentedoutput[$keysize];
							}
							else{
								$guarented = '0';
							}
							$guarented_obj = new CropGuaranteeOutput();
							$guarented_obj-> CropID = $model->CropID;
							$guarented_obj->SizeID = $sizevalue;
							$guarented_obj->Harvest = $guarented;
							$guarented_obj->OnDate = date('Y-m-d H:i:s');
							$guarented_obj->save();
							
							if( $allplantingcost[$keysize]!=''){
								$plantingcost = $allplantingcost[$keysize];
							}
							else{
								$plantingcost = '0';
							}
							$cropsize_obj = new CropSizePrice();
							$cropsize_obj->CropID = $model->CropID;
							$cropsize_obj->SizeID = $sizevalue;
							$cropsize_obj->PlantingPrice = $plantingcost;
							$cropsize_obj->UpdateDate = date('Y-m-d H:i:s');
							$cropsize_obj->save();
							
                        }
                        $this->redirect(['view', 'id' => $model->CropID]);

                    }
                    else{
                        return $this->render('create', [
                            'model' => $model,'allsize'=>$allsize
                        ]);
                    }
                }else{
                    return $this->render('create', [
                        'model' => $model,'allsize'=>$allsize
                    ]);
                }
                
                
            }
        }
        return $this->render('create', [
            'model' => $model,'allsize'=>$allsize
        ]);
        
    }

    /**
     * Updates an existing Crops model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $allsize = Size::find()->where(['IsDelete'=>0])->all();
		$imagemodel=new Image();
        if ($model->load(Yii::$app->request->post())) {
			
			
			if(isset(Yii::$app->request->post()['SizeID']))
			{
				$allsizeid = Yii::$app->request->post()['SizeID'];
				$allestimateoutput = Yii::$app->request->post()['EstimatedOutput'];
				$allguarenteoutput = Yii::$app->request->post()['GuaranteedOutput'];
				$allplantingcost = Yii::$app->request->post()['PlantingCost'];
				$cropImage = UploadedFile::getInstance($model, 'CropImage');
				if($cropImage!='')
				{
					$image_id=$imagemodel->imageUpload($cropImage,'Crop Image');
					$cropimageid=$image_id;
				}
				else
				{
					$cropimageid = $model->CropImage;
				}
				$model->CropImage=$cropimageid;
                
                
                $image = UploadedFile::getInstance($model,'CropAppImage');
                if($image!='')
                {
                    $imgmodel=new Image();
                    $crppimage=$imgmodel->imageUploads($image);
                    $cimage=$crppimage;
                }
                else
                {
                    $cimage = $model->CropAppImage;
                }
                $model->CropAppImage=$cimage;
                
				if($model->save()){					
					if(isset(Yii::$app->request->post()['cropspricesize'])){
						foreach(Yii::$app->request->post()['cropspricesize'] as $cprzkey=>$cprzvalue){
							$crpszpc = CropSizePrice::find()->where(['Id'=>$cprzvalue])->one();
							if(isset(Yii::$app->request->post()['PlantingCost'][$cprzkey]) && Yii::$app->request->post()['PlantingCost'][$cprzkey]!=''){
								$crpszpc->PlantingPrice = Yii::$app->request->post()['PlantingCost'][$cprzkey];
							}else{
								$crpszpc->PlantingPrice = 0;
							}
							$crpszpc->save();
						}
					}
					if(isset(Yii::$app->request->post()['CEID'])){
						foreach(Yii::$app->request->post()['CEID'] as $CEIDkey=>$CEIDvalue){
							$crpest = CropEstimatedOutput::find()->where(['CEID'=>$CEIDvalue])->one();
							if(isset(Yii::$app->request->post()['EstimatedOutput'][$CEIDkey]) && Yii::$app->request->post()['EstimatedOutput'][$CEIDkey]!=''){
								$crpest->Harvest = Yii::$app->request->post()['EstimatedOutput'][$CEIDkey];
							}else{
								$crpest->Harvest = 0;
							}
							$crpest->save();
						}
					}
					if(isset(Yii::$app->request->post()['CGID'])){
						foreach(Yii::$app->request->post()['CGID'] as $CGIDkey=>$CGIDvalue){
							$crpged = CropGuaranteeOutput::find()->where(['CGID'=>$CGIDvalue])->one();
							if(isset(Yii::$app->request->post()['GuaranteedOutput'][$CGIDkey]) && Yii::$app->request->post()['GuaranteedOutput'][$CGIDkey]!=''){
								$crpged->Harvest = Yii::$app->request->post()['GuaranteedOutput'][$CGIDkey];
							}else{
								$crpged->Harvest = 0;
							}
							$crpged->save();
							//echo "<pre>"; var_dump($crpged->getErrors());
						}
					}
					Yii::$app->session->setFlash('success', 'Crops created successfully');
				}else{
					Yii::$app->session->setFlash('error', 'There is some problem, please try again');
				}
				
			}
			
            return $this->redirect(['view', 'id' => $model->CropID]);
        }
		$estimatedoutput = CropEstimatedOutput::find()->where(['CropID'=>$id])->asArray()->all();
		$guarentedoutput = CropGuaranteeOutput::find()->where(['CropID'=>$id])->asArray()->all();
		$cropsize = CropSizePrice::find()->where(['CropID'=>$id])->asArray()->all();
		
        return $this->render('update', [
            'model' => $model,'allsize'=>$allsize,'estimatedoutput'=>$estimatedoutput,'guarentedoutput'=>$guarentedoutput,'cropsize'=>$cropsize
        ]);
		
    }

    /**
     * Deletes an existing Crops model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		$crops = Crops::find()->where(['CropID'=>$id])->one();
		$crops->IsDelete = 1;
		if($crops->save()){
			$estmateopt = CropEstimatedOutput::find()->where(['CropID'=>$id])->all();
			foreach($estmateopt as $key1=>$value1){
				$estmateoptt = CropEstimatedOutput::find()->where(['CEID'=>$value1->CEID])->one();
				$estmateoptt->IsDelete = 1;
				$estmateoptt->save();
			}
			$guarented = CropGuaranteeOutput::find()->where(['CropID'=>$id])->all();
			foreach($guarented as $key2=>$value2){
				$guarentedd = CropGuaranteeOutput::find()->where(['CGID'=>$value2->CGID])->one();
				$guarentedd->IsDelete = 1;
				$guarentedd->save();
			}
			$allcropplantingcost = CropSizePrice::find()->where(['CropID'=>$id])->all();
			foreach ($allcropplantingcost as $keycrop => $cropvalue) {
			   $cropsizedelete = CropSizePrice::find()->where(['Id'=>$cropvalue->Id])->one();
			   $cropsizedelete->IsDelete = 1;
			   $cropsizedelete->save();
			}
		Yii::$app->session->setFlash('success', 'Crop deleted successfully');
		}else{
			Yii::$app->session->setFlash('error', 'There is some problem, please try again');
		}
	     return $this->redirect(['index']);
    }
	
	public function actionCroplist()
    {
		$croplist = CustomerRent::find()->where(['Status'=>2])->asArray()->all();
		return $this->render('allrentlist', [
            'croplist' => $croplist
        ]);
	}
	
	public function actionRentdetails(){
		$rentid = Yii::$app->request->get()['rentid'];
		$rentadetails = CustomerRent::find()->where(['RentId'=>$rentid])->one();
		$allimage = CropsGrowthImage::find()->where(['RentId'=>$rentid])->all();
		return $this->render('rentdeatails', [
            'rentadetails' => $rentadetails,'allimage'=>$allimage
        ]);
	}
	
	public function actionGrowthimage()
    {
		
		if(isset(Yii::$app->request->post()['CropsGrowthImage']))
		{
			$rentid = Yii::$app->request->post()['CropsGrowthImage']['RentId'];
			$status = Yii::$app->request->post()['CropsGrowthImage']['StatusType'];
			$FarmImage=new CropsGrowthImage();
			if($status == 'Description')
            {
				if(isset(Yii::$app->request->post()['CropsGrowthImage']['Description']) && trim(Yii::$app->request->post()['CropsGrowthImage']['Description'])!='')
                {
					
						$desc = Yii::$app->request->post()['CropsGrowthImage']['Description'];
						$FarmImage ->RentId = $rentid;
						$FarmImage ->StatusType = $status;
						$FarmImage ->Description = $desc;
						$FarmImage ->AddedDate = date("Y-m-d");
						$FarmImage ->save();
						
						Yii::$app->session->setFlash('success', 'Status Updated successfully');
				}
				else
                {
						Yii::$app->session->setFlash('error', 'Growth desciption status not updated..');
				}
			}
            else
            {
				$cpimage = UploadedFile::getInstances($FarmImage,'Image');
				if(count($cpimage))
                {
					foreach($cpimage as $file)
                    {
						$ppimodel=new CropsGrowthImage();
						$ppimageid=$ppimodel->imageUpload($file,$rentid);
					}
                    
                    /* 
                     
                
                    $customerid=Yii::$app->request->post('customerid');
                    $res=UserFCMnotification::find()->where(['CustomerID'=>$customerid,'IsDelete'=>0])->one();
                    if(count($res)>0)
                    {
                           $displaydata="You have new notification regarding growth.";
                           $descp="New Notification Received.";
                           $token=$res->token;
                           $fcmtoken = new UserFCMnotification();
                           $fcmtoken->sendnotification($displaydata,$descp,$token);
                    }
                    
                    */
                    
					 Yii::$app->session->setFlash('success', 'Status Updated successfully');
				}
				else
                {
					Yii::$app->session->setFlash('error', 'Growth image Status not updated..');
				}	
			}
			
		}
		$croplist = CustomerRent::find()->where(['Status'=>2])->asArray()->all();
		return $this->render('allrentlist', [
            'croplist' => $croplist
        ]);
	}
	
	public function actionStatusdetails(){
		$rentid = Yii::$app->request->get()['rentid'];
		$cropsgrowth = CropsGrowthImage::find()->where(['RentId'=>$rentid])->asArray()->all();
		echo "<table class='table'>";
			echo "<thead>";
			echo "<tr>";
				echo "<th>Update Date</th>";
				echo "<th>Update Type</th>";
				echo "<th>Update Description</th>";
			echo "</tr>";
			echo "</thead>";
		if(count($cropsgrowth)){
			echo "<tbody>";
			foreach($cropsgrowth as $key=>$value){
				echo "<tr>";
					echo "<td style='width:20%;'>".date("d-M-Y",strtotime($value['AddedDate']))."</td>";
					echo "<td style='width:30%;'>".$value['StatusType']."</td>";
					if($value['StatusType'] == "Description"){
						echo "<td style='width:50%;'>";
						echo $value['Description'];
						echo "</td>";
					}else{
						echo "<td style='width:50%;'>";
						echo "<img src='".Yii::$app->params['imageurl'].$value['Image']."' width='150' height='80'/>";
						echo "</td>";
					}
				echo "</tr>";
			}
			echo "</tbody>";
			
		}
		else{
			echo "<tbody>";
			echo "<tr>";
			echo "<td colspan='3'> No Update available......</td>";
			echo "</tr>";
			echo "</tbody>";
		}
		echo "</table>";
	}

    /**
     * Finds the Crops model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Crops the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Crops::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
