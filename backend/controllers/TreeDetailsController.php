<?php

namespace backend\controllers;

use Yii;
use common\models\TreeDetails;
use common\models\TreesImage;
use common\models\TreeDetailsSearch;
use common\models\TreePackage;
use common\models\TreePlantation;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\CropsGrowthImage;
use yii\web\UploadedFile;
use common\models\StockTransaction;
use common\models\UserFCMnotification;

/**
 * TreeDetailsController implements the CRUD actions for TreeDetails model.
 */
class TreeDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TreeDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TreeDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TreeDetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TreeDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TreeDetails();

        if ($model->load(Yii::$app->request->post()))
        {
            
            $image = UploadedFile::getInstance($model,'TreeImage');
            $imgmodel=new TreesImage();
			$treimage=$imgmodel->imageUploads($image);
            $model->TreeImage=$treimage;
			$model->AddedDate = date("Y-m-d H:i:s");
			$model->UpdateDate = date("Y-m-d H:i:s");
			if($model->save())
            {
				$rentid = $model->ID;
			    $FarmImage=new TreesImage();
				$cpimage = UploadedFile::getInstances($FarmImage,'Image');
				foreach($cpimage as $file)
                {
					$ppimodel=new TreesImage();
					$ppimageid=$ppimodel->imageUpload($file,$rentid);
				}
                
                $stocktrans=new StockTransaction();
                $stocktrans->TreeDetailsId=$rentid;
                $stocktrans->Stock=$model->Stock;
                $stocktrans->AddedDate=date("Y-m-d H:i:s");
                $stocktrans->save();
			}
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TreeDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dbstock=$model->Stock;
		$allimages = TreesImage::find()->where(['TDID'=>$id])->all();
        if ($model->load(Yii::$app->request->post()))
        {
            
            $image = UploadedFile::getInstance($model,'TreeImage');
            if($image!='')
			{
                $imgmodel=new TreesImage();
				$treimage=$imgmodel->imageUploads($image);
				$timage=$treimage;
			}
			else
			{
				$timage = $model->TreeImage;
			}
            $model->TreeImage=$timage;
            $newstock=$model->Stock;
			$model->UpdateDate = date("Y-m-d H:i:s");
            $model->Stock=$model->Stock+$dbstock;
			if($model->save())
            {
				$rentid = $id;
			    $FarmImage=new TreesImage();
				$cpimage = UploadedFile::getInstances($FarmImage,'Image');
				
				if(count($cpimage))
                {
					$allimage = TreesImage::find()->where(['TDID'=>$id])->all();
					if(count($allimage))
                    {
						TreesImage::deleteAll('TDID = :TDID',[':TDID'=>$id]);
						$FarmImage=new TreesImage();
						$cpimage = UploadedFile::getInstances($FarmImage,'Image');
						foreach($cpimage as $file)
                        {
							$ppimodel=new TreesImage();
							$ppimageid=$ppimodel->imageUpload($file,$rentid);
						}
					}
				}
                
                $stocktrans=new StockTransaction();
                $stocktrans->TreeDetailsId=$rentid;
                $stocktrans->Stock=$newstock;
                $stocktrans->AddedDate=date("Y-m-d H:i:s");
                $stocktrans->save();
			}
            return $this->redirect(['view', 'id' => $model->ID]);
        }
		
        return $this->render('update', [
            'model' => $model,'allimage'=>$allimages
        ]);
    }

    /**
     * Deletes an existing TreeDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->Isdelete=1;
        if($model->save()){
           StockTransaction::updateAll(['IsDelete' =>1], "TreeDetailsId =$id" );
		   $treepkgdata = TreePackage::find()->where(['TDID'=>$id])->all();
		    if(count($treepkgdata)){
			   TreePackage::updateAll(['Isdelete' =>1], "TDID =$id" );
		    }
			Yii::$app->session->setFlash('success', 'Tree Details deleted successfully');
		}else{
			Yii::$app->session->setFlash('error', 'There is some problem, please try again');
		}
        return $this->redirect(['index']);
    }

    /**
     * Finds the TreeDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TreeDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TreeDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	public function actionTreeplantlist(){
		$treeplantlist = TreePlantation::find()->where(['PlantingStatus'=>1])->asArray()->all();
		return $this->render('treerentlist', [
            'treeplantlist' => $treeplantlist
        ]);
	}
	public function actionRentdetails(){
		$rentid = Yii::$app->request->get()['rentid'];
		$rentadetails = TreePlantation::find()->where(['ID'=>$rentid])->one();
		$allimage = CropsGrowthImage::find()->where(['TreePlantID'=>$rentid])->all();
		return $this->render('treerentdetails', [
            'rentadetails' => $rentadetails,'allimage'=>$allimage
        ]);
	}
	public function actionGrowthimage()
    {
		
		if(isset(Yii::$app->request->post()['CropsGrowthImage']))
		{
			$rentid = Yii::$app->request->post()['CropsGrowthImage']['RentId'];
			$rentdetails = TreePlantation::find()->where(['ID'=>$rentid])->one();
			$customerid = $rentdetails['CustomerID'];
			$treepackageid = $rentdetails['TPID'];
			$packdet = TreePackage::find()->where(['PKGID'=>$treepackageid])->one();
			if(count($packdet)){
				$treeid = $packdet['TDID'];
				$treedetails = TreeDetails::find()->where(['ID'=>$treeid])->one();
				$treename = $treedetails['TreeName'];
			}
			else{
				$treename = "";
			}
			
			$status = Yii::$app->request->post()['CropsGrowthImage']['StatusType'];
			$FarmImage=new CropsGrowthImage();
			if($status == 'Description')
            {
				if(isset(Yii::$app->request->post()['CropsGrowthImage']['Description']) && trim(Yii::$app->request->post()['CropsGrowthImage']['Description'])!=''){
					
						$desc = Yii::$app->request->post()['CropsGrowthImage']['Description'];
						$FarmImage ->TreePlantID = $rentid;
						$FarmImage ->StatusType = $status;
						$FarmImage ->Description = $desc;
						$FarmImage ->AddedDate = date("Y-m-d");
						if($FarmImage ->save()){
							$fcmtokendetails = UserFCMnotification::find()->where(['CustomerID'=>$customerid])->one();
							if(count($fcmtokendetails)){
								$token = $fcmtokendetails['token'];
								$displaydata = $treename." growth notification";
								$descp = "Tree";
								$sendnotificatio = UserFCMnotification::sendnotification($displaydata,$descp,$token,'Tree');
							}
							
						    Yii::$app->session->setFlash('success', 'Status Updated successfully');
						
						}else{
							Yii::$app->session->setFlash('error', 'There is some error please try after sometime..');
						}
						
				}
				else{
						Yii::$app->session->setFlash('error', 'Growth desciption status not updated..');
				}
			}
            else
            {
				$cpimage = UploadedFile::getInstances($FarmImage,'Image');
				if(count($cpimage)){
					foreach($cpimage as $file)
                    {
						$ppimodel=new CropsGrowthImage();
						$ppimageid=$ppimodel->imageUpload1($file,$rentid);
					}
                    
                    
                    /*
                    $customerid=Yii::$app->request->post('customerid');
                    $res=UserFCMnotification::find()->where(['CustomerID'=>$customerid,'IsDelete'=>0])->one();
                    if(count($res)>0)
                    {
                           $displaydata="You have new notification regarding growth.";
                           $descp="New Notification Received.";
                           $token=$res->token;
                           $fcmtoken = new UserFCMnotification();
                           $fcmtoken->sendnotification($displaydata,$descp,$token);
                    }
                    */
                    
					 Yii::$app->session->setFlash('success', 'Status Updated successfully');
				}
				else{
					Yii::$app->session->setFlash('error', 'Growth image Status not updated..');
				}
					
					
			}
			
		}
		/*
		$croplist = CustomerRent::find()->where(['Status'=>2])->asArray()->all();
		return $this->render('allrentlist', [
            'croplist' => $croplist
        ]);
		*/
		return $this->redirect(['treeplantlist']);
	}
	public function actionStatusdetails(){
		$rentid = Yii::$app->request->get()['rentid'];
		$cropsgrowth = CropsGrowthImage::find()->where(['TreePlantID'=>$rentid])->asArray()->all();
		echo "<table class='table'>";
			echo "<thead>";
			echo "<tr>";
				echo "<th>Update Date</th>";
				echo "<th>Update Type</th>";
				echo "<th>Update Description</th>";
			echo "</tr>";
			echo "</thead>";
		if(count($cropsgrowth)){
			echo "<tbody>";
			foreach($cropsgrowth as $key=>$value){
				echo "<tr>";
					echo "<td style='width:20%;'>".date("d-M-Y",strtotime($value['AddedDate']))."</td>";
					echo "<td style='width:30%;'>".$value['StatusType']."</td>";
					if($value['StatusType'] == "Description"){
						echo "<td style='width:50%;'>";
						echo $value['Description'];
						echo "</td>";
					}else{
						echo "<td style='width:50%;'>";
						echo "<img src='".Yii::$app->params['imageurl'].$value['Image']."' width='150' height='80'/>";
						echo "</td>";
					}
				echo "</tr>";
			}
			echo "</tbody>";
			
		}
		else{
			echo "<tbody>";
			echo "<tr>";
			echo "<td colspan='3'> No Update available......</td>";
			echo "</tr>";
			echo "</tbody>";
		}
		echo "</table>";
	}
	
}
