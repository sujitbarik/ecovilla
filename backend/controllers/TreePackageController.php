<?php

namespace backend\controllers;

use Yii;
use common\models\TreePackage;
use common\models\TreeDetails;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * TreePackageController implements the CRUD actions for TreePackage model.
 */
class TreePackageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TreePackage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TreePackage::find()->where(['Isdelete'=>0]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TreePackage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TreePackage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TreePackage();
		$alltrees=ArrayHelper::map(TreeDetails::find()->where(['IsDelete'=>0])->all(),'ID','TreeName');
		
		
        if ($model->load(Yii::$app->request->post())) {
			
			$model->CreateDate = date("Y-m-d H:i:s A");
			$model->UpdateDate = date("Y-m-d H:i:s A");
			$model->save();
            return $this->redirect(['view', 'id' => $model->PKGID]);
        }

        return $this->render('create', [
            'model' => $model,'alltrees'=>$alltrees
        ]);
    }

    /**
     * Updates an existing TreePackage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $alltrees=ArrayHelper::map(TreeDetails::find()->where(['IsDelete'=>0])->all(),'ID','TreeName'); 
        if ($model->load(Yii::$app->request->post())) {
			
			$model->UpdateDate = date("Y-m-d H:i:s A");
			$model->save();
            return $this->redirect(['view', 'id' => $model->PKGID]);
        }

        return $this->render('update', [
            'model' => $model,'alltrees'=>$alltrees,'treeid'=>$model->TDID
        ]);
    }

    /**
     * Deletes an existing TreePackage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
		$model->Isdelete = 1;
		if($model->save()){
			Yii::$app->session->setFlash('success', 'Tree Package deleted successfully');
		}else{
			Yii::$app->session->setFlash('error', 'There is some problem, please try again');
		}

        return $this->redirect(['index']);
    }

    /**
     * Finds the TreePackage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TreePackage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TreePackage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
	public function actionTreedetails(){
		$treeid =Yii::$app->request->get()['treesid'];
		$details = TreeDetails::find()->where(['ID'=>$treeid])->one();
		echo "<table class='table' style='width:50%; background:#FFF;'>";
		echo "<tr><th>Planting Cost(Per Unit) : </th>";
		echo "<td> Rs. ".number_format($details['PlantingCost'],2)."<input type='hidden' id='pccost' value='".$details['PlantingCost']."'/></td>";
		echo "</tr><tr><th>Monthly Recuring Cost(Per Unit) : </th>";
		echo "<td> Rs. ".number_format($details['MonthlyRecuringCost'],2)."<input type='hidden' id='mccost' value='".$details['MonthlyRecuringCost']."'/></td>";
		echo "</tr>";
		echo "</table>";
	}
}
