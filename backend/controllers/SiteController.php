<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\CustomerRegistration;
use common\models\User;
use common\models\CustomerRent;
use common\models\Farm;
use common\models\Crops;
use common\models\CropEstimatedOutput;
use common\models\CropGuaranteeOutput;
use common\models\FarmImage;
use common\models\Image;
use common\models\Size;
use common\models\CropSizePrice;
use common\models\StockFarm;
use common\models\Transaction;
use common\models\CropsGrowthImage;
use common\models\TreeDetails;
use common\models\TreePackage;
use common\models\TreesImage;
use common\models\TreePlantation;
use common\models\OxyPoints;
use yii\db\Expression;

use common\models\UserFCMnotification;

date_default_timezone_set("Asia/Calcutta");

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','index', 'error','alltransaction','upcomingplantation','plantationstatuschange','planted','upcomingtreeplantation','treeplantationstatuschange','addoxypoints','qrcodegenerate','qrcodedetail'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','alltransaction'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
		
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			//echo "ifff";
            return $this->goBack();
        } else {

            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
		
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	
	public function actionAlltransaction(){
		$alltransactionlist = Transaction::find()->orderBy(['Id'=>SORT_DESC])->asArray()->all();
		return $this->render('alltransaction', [
            'alltransactionlist' => $alltransactionlist
        ]);
	}
	
	public function actionUpcomingplantation(){
		$nextsunday = strtotime('next sunday');
		$d1 = date("Y-m-d",$nextsunday);
		$allupcomingrent = CustomerRent::find()->where(['Status'=>1])->asArray()->all();
		//$allupcomingtreerent = TreePlantation::find()->where(['PlantingStatus'=>0])->asArray()->all();
		return $this->render('upcomingplantation', [
            'allupcomingrent' => $allupcomingrent,
        ]);
	}
	public function actionUpcomingtreeplantation(){
		$nextsunday = strtotime('next sunday');
		$d1 = date("Y-m-d",$nextsunday);
		//$allupcomingrent = CustomerRent::find()->where(['Status'=>1])->asArray()->all();
		$allupcomingtreerent = TreePlantation::find()->where(['PlantingStatus'=>0])->asArray()->all();
		return $this->render('upcomingtreerent', [
            'treeplant'=>$allupcomingtreerent
        ]);
	}
	
	public function actionPlantationstatuschange()
    {
		
		if(isset(Yii::$app->request->post()['Status']))
        {
			$data = Yii::$app->request->post()['Status'];
			foreach($data as $key => $value)
            {
				$customerrent = CustomerRent::find()->where(['RentId'=>$key])->one();
				$customerrent->Status = 2;
				$customerrent->CropPlantationDate = date("Y-m-d");
				$customerrent->save();
                
				/*Notification*/
                
                $customerid=$customerrent->CustomerId;
                $res=UserFCMnotification::find()->where(['CustomerID'=>$customerid,'IsDelete'=>0])->one();
                if(count($res)>0)
                {
                       $cropname=$customerrent->cropsize->crops->CropName;
                       $size=$customerrent->cropsize->size->Size;
                       $displaydata="Dear Farmer
                       Your $cropname has been planted on the desired area of  $size Square feet .You will be get update soon.";
                       $descp="New Notification Received.";
                       $token=$res->token;
                       $fcmtoken = new UserFCMnotification();
                       $fcmtoken->sendnotification($displaydata,$descp,$token);
                }
                
                /*Notification*/
                
			}
		}
        
		return $this->redirect(['upcomingplantation']);
		
	}
	
	public function actionTreeplantationstatuschange()
    {
		
		if(isset(Yii::$app->request->post()['Status']))
        {
			$data = Yii::$app->request->post()['Status'];
			foreach($data as $key => $value)
            {
				$customerrent = TreePlantation::find()->where(['ID'=>$key])->one();
				$customerrent->PlantingStatus = 1;
				$customerrent->PlantingDate = date("Y-m-d H:i:s");
				$customerrent->save();
				
                /*Notification*/
                
                $customerid=$customerrent->CustomerID;
                $res=UserFCMnotification::find()->where(['CustomerID'=>$customerid,'IsDelete'=>0])->one();
                if(count($res)>0)
                {
                       $treename=$customerrent->treepckg->treedetail->TreeName;
                       $displaydata="Dear Farmer
                       Your $treename has been planted on the desired area.You will be get update soon.";
                       $descp="New Notification Received.";
                       $token=$res->token;
                       $fcmtoken = new UserFCMnotification();
                       $fcmtoken->sendnotification($displaydata,$descp,$token);
                }
                
                /*Notification*/

			}
		}
		 return $this->redirect(['upcomingtreeplantation']);
		
	}
	
	public function actionPlanted(){
		$allplanted = CustomerRent::find()->where(['Status'=>2])->asArray()->all();
		
		return $this->render('planted', [
            'allplanted' => $allplanted
        ]);
		
	}
	
	public function actionAddoxypoints(){
		$this->layout='appblanklayout';
		$query1 = TreePlantation::find()->where(['PlantingStatus'=>1])->andWhere(['<=','PlantingDate',new Expression('(NOW() - INTERVAL 1 HOUR)')])->all();
		if(count($query1)){
			foreach($query1 as $key1 => $value1){
				//$planttime = $value1->PlantingDate;
				$custid = $value1->CustomerID;
				$treeplatid = $value1->ID;
				$chkoxypoints = OxyPoints::find()->where(['TreePlantID'=>$treeplatid])->one();
				if(count($chkoxypoints)){
					$oldoxypoints = $chkoxypoints['OxyPoints'];
					$chkoxypoints->OxyPoints = $oldoxypoints + 0.00570;
					$chkoxypoints->save();
				}
				else{
					$oxypoints = new OxyPoints();
					$oxypoints->TreePlantID = $value1->ID;
					$oxypoints->CustomerID = $value1->CustomerID;
					$oxypoints->OxyPoints = 0.00570;
					$oxypoints->save();
					
				}
			}
		}
		
	}
	public function actionQrcodegenerate()
    {
		$allplantedtree = TreePlantation::find()->where(['PlantingStatus'=>1,'Isdelete'=>0])->asArray()->All();
		//echo "<pre>"; var_dump($allplantedtree);
		$result = array();
		if(count($allplantedtree)){
			foreach($allplantedtree as $key=>$value){
				$result[$key]['PlantID'] = $value['ID'];
				$result[$key]['CustomerName'] = CustomerRegistration::getCustomername($value['CustomerID']);
				$result[$key]['TreeName'] = TreePackage::getTreename($value['TPID']);
				$result[$key]['BookingDate'] = date("d-M-y",strtotime($value['AddedDate']));
				$result[$key]['PlantationDate'] = date("d-M-y",strtotime($value['PlantingDate']));
				$result[$key]['PackageCost'] = number_format(TreePackage::getPackagecost($value['TPID']),2);
				$details = CustomerRegistration::getCustomername($value['CustomerID'])." \n ".TreePackage::getTreename($value['TPID'])." \n "."Booking Date : ".date("d-M-y",strtotime($value['AddedDate']))." \n "." Plantation Date : ".date("d-M-y",strtotime($value['PlantingDate']));
				$result[$key]['Qrcodedata'] = $details;
				$result[$key]['QRCode'] = CustomerRegistration::getQrcode($details);
				
			}
		}
		else{
			$result = array();
		}
		return $this->render('customerqrcode', [
            'qrcodedata' => $result
        ]);
	}
    
}
