<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\helpers\BaseUrl;
//use yii\helpers\Url;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\TreeDetails */
/* @var $form yii\widgets\ActiveForm */
$imageurl="http://ecovilla.in";
?>

<div class="tree-details-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'TreeName')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'TreeDescription')->textarea(['rows' => 6]) ?>
	<?= $form->field($model, 'TreeDescription')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>

    <?= $form->field($model, 'PlantingCost')->textInput() ?>

    <?= $form->field($model, 'MonthlyRecuringCost')->textInput() ?>
	
    <?= $form->field($model, 'OxyPoint')->textInput() ?>
    
    <?= $form->field($model, 'Stock')->textInput() ?>
    
    <?= $form->field($model, 'MaturityTime')->textInput() ?>
    
	<?php
			if(isset($allimage))
			{
				foreach($allimage as $fk=>$fval)
				{
					$path1 = $fval->Image;
					$id = $fval->ID;
					$url = $_SERVER['SERVER_NAME'];
		            $path = $url."/".$path1;
					$p = $imageurl."/".$path1;
			?>
			
			<div style="width: auto;float: left;position: relative; margin-right:5px;" id="farmimage<?=$id;?>">
				<img src="<?php echo $p;?>" style="height: 150px;"/>
				<span style="position: absolute;font-weight:bold;top: 0px;right: 0px;cursor: pointer; display:none;" onclick="farmimagedelete(<?=$fval->ID;?>);">X</span>
			</div> 
			
			<?php
				}
				?>
			<br/><br/><br/><br/><br/><br/><br/><br/>
			<?php
			}
			
             if(!$model->isNewRecord && $model->TreeImage!='')
             {
            
			?>
	
    
        <div style="width: auto;float: left;position: relative; margin-right:5px;">
			<img src="<?php echo $imageurl.'/'.$model->TreeImage;?>" style="height: 150px;"/>
	    </div>
        <?php
             }
        ?>
   
		<div class="form-group field-crops-estimated required">
			<label class="control-label">App Image</label>
			<input type="file" class="form-control" name="TreeDetails[TreeImage]">
		</div>

    
	<div id="crops_image">
		<div class="form-group field-crops-estimated required">
			<label class="control-label" for="crops-estimated"> Image</label>
			<input type="file" id="treesimage" class="form-control" value="" name="TreesImage[Image][]">
		</div>
	</div>
			
	<div id="alltreesimage" class="form-group field-crops-plantingscost required"></div>
	<span class="addmore" onclick="addmore();" style="display:none;">+ Add More</span>

    <?php //$form->field($model, 'AddedDate')->textInput() ?>

    <?php //$form->field($model, 'UpdateDate')->textInput() ?>

    <?php //$form->field($model, 'Isdelete')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
function addmore()
    {
        $('#alltreesimage').append($('#crops_image').html());
    }
</script>