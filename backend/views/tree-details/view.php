<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TreeDetails */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Tree Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-details-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'TreeName',
           // 'TreeDescription:ntext',
            [
                'label' => 'Tree Description',
                'value' => html_entity_decode(strip_tags($model->TreeDescription),ENT_QUOTES),
            ],
            'PlantingCost',
            'MonthlyRecuringCost',
            'OxyPoint',
            'Stock',
            'MaturityTime',
            'AddedDate',
            //'UpdateDate',
            //'Isdelete',
        ],
    ]) ?>

</div>
