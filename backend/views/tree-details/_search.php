<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TreeDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tree-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'TreeName') ?>

    <?= $form->field($model, 'TreeDescription') ?>

    <?= $form->field($model, 'PlantingCost') ?>

    <?= $form->field($model, 'MonthlyRecuringCost') ?>

    <?php // echo $form->field($model, 'AddedDate') ?>

    <?php // echo $form->field($model, 'UpdateDate') ?>

    <?php // echo $form->field($model, 'Isdelete') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
