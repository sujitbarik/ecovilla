<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CustomerRegistration;
use common\models\CropsGrowthImage;
use common\models\Crops;
use common\models\Farm;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use common\models\TreeDetails;
use common\models\TreePackage;
use common\models\TreesImage;
use common\models\TreePlantation;


$this->title = 'All Tree Planted List';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-index">
    <h2><?= strtoupper(Html::encode($this->title)) ?></h2>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
	    <thead>
            <tr>
                <th>#</th>
                <th>PlantationID/QRCode</th>
				<th>Customer Name</th>
				<th>Tree Name</th>
				<th>Package Name</th>
				<th>Booking Date</th>
				<th>Tree Planted Date</th>
				<th>No. of Updates</th>
				<th>Update Status</th>
            </tr>
		</thead>
		<tbody>
		    <?php 
			if(count($treeplantlist)){
				foreach($treeplantlist as $key=>$value){
					$rentid = $value['ID'];
			?>
		    <tr>
				<td><?= ($key+1); ?></td>
        <td><?= $value['ID']; ?></td>
				<td><?=CustomerRegistration::getCustomername($value['CustomerID']);?></td>
				<td><?=TreePackage::getTreename($value['TPID']);?></td>
				<td><?=TreePackage::getTreepackage($value['TPID']);?></td>
				<td><?=date("d-m-Y",strtotime($value['AddedDate']));?></td>
				<td><?=date("d-m-Y",strtotime($value['PlantingDate']));?></td>
				<td data-toggle="modal" data-target="#myModal" onclick="growthUpdatedetails(<?=$rentid?>)" style='cursor:pointer;'><?=CropsGrowthImage::noofUpdatess($rentid);?></td>
				<td>
					<a href="<?=Url::to(["tree-details/rentdetails"]);?>?rentid=<?=$rentid?>">
						<button class="btn btn-info btn-sm">
						Update
						</button>
					</a>
				</td>
				
			</tr>
			<?php 
				}
			}
			?>
		</tbody>
    </table>
</div>
<!-- Modal form-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">UPDATE DETAILS</h4>
      </div>
      <div class="modal-body" id="growthstatus"></div>
      
    </div>
  </div>
</div>
<script>
function growthUpdatedetails(rentid){
	if(rentid!='' || rentid!=0){
		
		$.ajax({url:"<?=Url::to(["tree-details/statusdetails"]);?>?rentid="+rentid,
		   success:function(results)
		   {
			   //alert(results);
			   console.log(results);
		   		$('#growthstatus').html(results);
		   }
		});
	}
}
</script>