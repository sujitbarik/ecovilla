<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TreeDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tree Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-details-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tree Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ID',
            'TreeName',
            //'TreeDescription:ntext',
            'PlantingCost',
            'MonthlyRecuringCost',
            'AddedDate',
            //'UpdateDate',
            //'Isdelete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
