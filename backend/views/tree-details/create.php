<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TreeDetails */

$this->title = 'Create Tree Details';
$this->params['breadcrumbs'][] = ['label' => 'Tree Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
