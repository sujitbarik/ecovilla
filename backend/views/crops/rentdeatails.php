<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Size;
use common\models\Crops;
use common\models\CustomerRegistration;
use yii\helpers\BaseUrl;
use yii\helpers\Url;

$this->title = 'Details';
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Crops */
/* @var $form yii\widgets\ActiveForm */
//echo "<pre>"; var_dump($rentadetails);
if(isset($rentadetails)){
	$cropname = Crops::getCropname($rentadetails['CropId']);
	$customername = CustomerRegistration::getCustomername($rentadetails['CustomerId']);
	$rentid = $rentadetails['RentId'];
}
else{
	$cropname = "";
	$customername = "";
	$rentid = "";
}
?>

<div class="crops-form">
		<?php $form = ActiveForm::begin(
					[
					    'action' => ['crops/growthimage'],
						'options' => ['enctype' => 'multipart/form-data'],
						
					]); 
		?>
		   <input type="hidden" name="customerid" value="<?=$rentadetails['CustomerId']?>"/>
			<div class="form-group field-crops-plantingsize required">
				<label class="control-label" for="crops-plantingsize">Customer Name</label>
				<input type="text" class="form-control"  aria-required="true" value="<?=$customername;?>" name="CropsRent[CustomerName]" readonly="readonly">
			</div>
			
			<div class="form-group field-crops-plantingscost required">
				<label class="control-label" for="crops-plantingscost">Crops Name</label>
				<input type="text" class="form-control"  aria-required="true" name="CropsRent[CropName]" value="<?=$cropname;?>" readonly="readonly">
				<input type="hidden" class="form-control"  aria-required="true" name="CropsGrowthImage[RentId]" value="<?=$rentid;?>" readonly="readonly">
			</div>
			<div class="form-group field-crops-plantingscost required">
				<label class="control-label" for="crops-plantingscost">Update Type</label>
				<select class="form-control" name="CropsGrowthImage[StatusType]" onchange="updateGrowthstatus(this.value)">
					<option value="Image">Growth Image</option>
					<option value="Description">Description</option>
				</select>
			</div>
			<div class="form-group field-crops-plantingscost required" id="growth_description" style="display:none;">
				<label class="control-label" for="crops-plantingscost">Description</label>
				<textarea class="form-control" name="CropsGrowthImage[Description]"></textarea>
			</div>
			
			<!----
			<?php
			if(isset($allimage)){
				if(count($allimage))
				{
					foreach($allimage as $fk=>$fval)
					{
				?>
				
				<div style="width: 200px;float: left;position: relative;" id="farmimage<?=$fval->ID;?>">
					<img src="<?=Yii::$app->params['imageurl'].$fval->Image;?>" style="height: 150px;"/>
					<span style="position: absolute;font-weight:bold;top: 0px;right: 0px;cursor: pointer; display:none;" onclick="farmimagedelete(<?=$fval->ID;?>);">X</span>
				</div>
				
				<?php
					}
				}
			}
			
			
			?>
			---->
			
			<div id="crops_image">
				<div class="form-group field-crops-estimated required">
					<label class="control-label" for="crops-estimated"> Image</label>
					<input type="file" id="cropsgrowthimage" class="form-control" value="" name="CropsGrowthImage[Image][]">
				</div>
			</div>
			
			<div id="allfarmimage" class="form-group field-crops-plantingscost required"></div>
			<span class="addmore" onclick="addmore();" id="addmore">+ Add More</span>
			
			
			
			<div class="form-group">
				<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
			</div>
        <?php ActiveForm::end(); ?>
 
</div>
<script>
function addmore()
    {
        $('#allfarmimage').append($('#crops_image').html());
    }
    
    function farmimagedelete(farmid)
    {
        $.ajax({url:"<?=Url::toRoute(['farm/farmimagedelete']);?>?farmid="+farmid,
               success:function(results)
                {
                    var res=JSON.parse(results);
                    if(res==1)
                    {
                        alert('Farm Image deleted successfully');
                        $('#farmimage'+farmid).remove();
                    }
                }
        });
    }
	function updateGrowthstatus(status){
		if(status!=''){
			if(status == 'Description'){
				$('#growth_description').show();
				$('#crops_image').hide();
				$('#addmore').hide();
			}
			else{
				$('#growth_description').hide();
				$('#crops_image').show();
				$('#addmore').show();
			}
		}
	}
	
</script>