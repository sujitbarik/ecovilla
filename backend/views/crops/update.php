<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Crops */

$this->title = 'Update Crops: ' . $model->CropID;
$this->params['breadcrumbs'][] = ['label' => 'Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CropID, 'url' => ['view', 'id' => $model->CropID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="crops-update">

    <?= $this->render('_form', [
        'model' => $model,'allsize'=>$allsize,'estimatedoutput'=>$estimatedoutput,'guarentedoutput'=>$guarentedoutput,'cropsize'=>$cropsize
    ]) ?>

</div>
