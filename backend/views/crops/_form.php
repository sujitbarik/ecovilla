<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Size;
use common\models\Image;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Crops */
/* @var $form yii\widgets\ActiveForm */
$imageurl="http://ecovilla.in";
?>

<div class="crops-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'CropName')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'CropImage')->fileInput() ?>
    
	<?php 
	if(isset($model->CropImage) && ($model->CropImage!=0)){
		//echo Yii::$app->params['imageurl'].$model->CropImage;
		$path1 = $imageurl."/".Image::getImagepath($model->CropImage);
		$path = str_replace("/frontend/web/","/backend",Yii::$app->params['imageurl'])."/".$path1;
	?>
	<div style="width: 200px;float: left;position: relative;" id="farmimage<?= $model->CropImage;?>">
        <img src="<?=$path1;?>" style="height: 150px;"/>
    </div></br/><br/></br/><br/></br/><br/></br/></br>
	<?php 
	}
	?>
    <?= $form->field($model, 'ScientificName')->textInput(['maxlength' => true]) ?>
 
    <?= $form->field($model, 'MaturityTime')->dropDownList(
				[
					'1' => '1 Month',
					'2' => '2 Month',
					'3' => '3 Month',
					'4' => '4 Month',
					'5' => '5 Month',
					'6' => '6 Month',
					'7' => '7 Month',
					'8' => '8 Month',
					'9' => '9 Month',
					'10' => '10 Month',
					'11' => '11 Month',
					'12' => '12 Month',
				]
			);
	?>

    <?php
        if(!$model->isNewRecord && $model->CropAppImage!='')
        {
            
    ?>
        <div style="width: auto;float: left;position: relative; margin-right:5px;">
			<img src="<?php echo $imageurl.'/'.$model->CropAppImage;?>" style="height: 150px;"/>
	    </div>
    <?php
        }
    ?>
    
    <div class="form-group required">
			<label class="control-label">App Image</label>
			<input type="file" class="form-control" name="Crops[CropAppImage]">
	</div>

    <?php //$form->field($model, 'PlantationDays')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'PlantingCost')->textInput() ?>

    <?php //$form->field($model, 'MaintenanceCost')->textInput() ?>
	
	

     <?php
	 
	if(isset($guarentedoutput) && isset($estimatedoutput) && isset($cropsize)){
		
		foreach($allsize as $keyxsize=>$keysvalue){
			
			if(isset($estimatedoutput[$keyxsize]['Harvest']) && $estimatedoutput[$keyxsize]['Harvest'] !=''){
				$estimedoutput = $estimatedoutput[$keyxsize]['Harvest'];
				$CEID = $estimatedoutput[$keyxsize]['CEID'];
			}
			else{
				$estimedoutput = '';
				$CEID = '';
			}
			if(isset($guarentedoutput[$keyxsize]['Harvest']) && $guarentedoutput[$keyxsize]['Harvest'] !=''){
				$guarenteoutput = $guarentedoutput[$keyxsize]['Harvest'];
				$CGID = $guarentedoutput[$keyxsize]['CGID'];

			}
			else{
				$guarenteoutput = '';
				$CGID = '';
			}
			if(isset($cropsize[$keyxsize]['PlantingPrice']) && $cropsize[$keyxsize]['PlantingPrice']!=''){
				$plantingscost = $cropsize[$keyxsize]['PlantingPrice'];
			}
			else{
				$plantingscost = '';
			}
			
			if(isset($cropsize[$keyxsize]['Id']) && $cropsize[$keyxsize]['Id']!=''){
				$croppricesizeid = $cropsize[$keyxsize]['Id'];
			}
			else{
				$croppricesizeid = '';
			}
			
		?>

		<div class="form-group field-crops-plantingsize required">
			<label class="control-label" for="crops-plantingsize">Size(Sq. Ft.)</label>
			<input type="text" class="form-control"  aria-required="true" value="<?=$keysvalue->Size?>" name="Size[]" readonly="readonly">
			<input type="hidden" id="crops-sizeid" class="form-control" value="<?=$keysvalue->SizeID?>" name="SizeID[]" aria-required="true" aria-invalid="true">
		</div>
		
		<div class="form-group field-crops-plantingscost required">
			<label class="control-label" for="crops-plantingscost"><?=$keysvalue->Size?> Sq. Ft. Planting Cost</label>
			<input type="text" class="form-control"  aria-required="true" name="PlantingCost[]" value="<?=$plantingscost;?>">
			<input type="hidden" id="cropspricesize" class="form-control" value="<?= $croppricesizeid; ?>" name="cropspricesize[]" aria-required="true" aria-invalid="true">
		</div>
		
		<div class="form-group field-crops-estimated required">
			<label class="control-label" for="crops-estimated"><?=$keysvalue->Size?> Sq. Ft. Estimated Output(In kg)</label>
			<input type="text" id="crops-EstimatedOutput" class="form-control" value="<?=$estimedoutput;?>" name="EstimatedOutput[]" aria-required="true" aria-invalid="true">
			<input type="hidden" id="cropsestimatedoutputid" class="form-control" value="<?=$CEID;?>" name="CEID[]" aria-required="true" aria-invalid="true">
		</div>
		
		<div class="form-group field-crops-guarented required">
			<label class="control-label" for="crops-plantingcost"><?=$keysvalue->Size?> Sq. Ft. Guaranteed Output (In kg)</label>
			<input type="text" id="crops-GuaranteedOutput" class="form-control" value="<?=$guarenteoutput;?>" name="GuaranteedOutput[]" aria-required="true" aria-invalid="true">
			<input type="hidden" id="cropsguaranteedoutput" class="form-control" value="<?=$CGID;?>" name="CGID[]" aria-required="true" aria-invalid="true">
		</div>
		<?php
		}
		
	}
	else{
		foreach($allsize as $key=>$value){
		?>

		<div class="form-group field-crops-plantingcost required">
			<label class="control-label" for="crops-plantingcost">Size(Sq. Ft.)</label>
			<input type="text" class="form-control"  aria-required="true" value="<?=$value->Size?>" name="Size[]" readonly="readonly">
			<input type="hidden" id="crops-sizeid" class="form-control" value="<?=$value->SizeID?>" name="SizeID[]" aria-required="true" aria-invalid="true">
		</div>

		<div class="form-group field-crops-plantingscost required">
			<label class="control-label" for="crops-plantingscost"><?=$value->Size?> Sq. Ft. Planting Cost</label>
			<input type="text" class="form-control"  aria-required="true" name="PlantingCost[]">
		</div>
		
		<div class="form-group field-crops-plantingcost required">
			<label class="control-label" for="crops-plantingcost"><?=$value->Size?> Sq. Ft. Estimated Output (In kg)</label>
			<input type="text" id="crops-EstimatedOutput" class="form-control" name="EstimatedOutput[]" aria-required="true" aria-invalid="true">
		</div>
		
		<div class="form-group field-crops-plantingcost required">
			<label class="control-label" for="crops-plantingcost"><?=$value->Size?> Sq. Ft. Guaranteed Output (In kg)</label>
			<input type="text" id="crops-GuaranteedOutput" class="form-control" name="GuaranteedOutput[]" aria-required="true" aria-invalid="true">
		</div>
		<?php 
		}
	}
    ?>
	
	
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
