<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TreePackage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tree-package-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'PackageName')->textInput() ?>
    
	<?= $form->field($model, 'TDID')->dropDownList($alltrees,['prompt'=>'Select...','onchange'=>'treesDetails(this.value);'])?>
	
	<div class="form-group" id="trees-details" style="display:none; text-align:center;">
		
	</div>

    <?= $form->field($model, 'Duration')->textInput() ?>

    <?php //$form->field($model, 'Quantity')->textInput(['onblur'=>'packageCostcalculation(this.value)']) ?>

    <?php //$form->field($model, 'PlantingCost')->textInput(['readonly'=>'readonly']) ?>

    <?php //$form->field($model, 'MonthlyRecuringCost')->textInput(['readonly'=>'readonly']) ?>
    
	<?= $form->field($model, 'PackageCost')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) { 
  <?php 
	if(isset($treeid)){
		?>
		treesDetails(<?=$treeid?>);
		<?php
	}
  ?>
});

function treesDetails(treesid)
    {
		if(treesid!=''){
			$('#trees-details').html('');
			$.ajax({url:"<?=Url::toRoute(['tree-package/treedetails']);?>?treesid="+treesid,
				   success:function(results)
					{
						$('#trees-details').show();
						$('#trees-details').html(results);
					}
			});
		}
		else{
			$('#trees-details').html('');
			$('#trees-details').hide();
		}
	
    }
	function packageCostcalculation(qty){
		if(qty!=''){
			var month = $('#treepackage-duration').val();
			var pccost = $('#pccost').val();
			var rcmcost = $('#mccost').val();
			var totalrcmcost = qty * rcmcost;
			var totalpccost = qty * pccost;
			var totalrecursivecost = month * totalrcmcost;
			
			var total = totalrecursivecost + totalpccost;
			if(!isNaN(totalpccost) && !isNaN(totalrecursivecost)){
				$('#treepackage-plantingcost').val(totalpccost);
				$('#treepackage-monthlyrecuringcost').val(totalrecursivecost);
				$('#treepackage-packagecost').val(total);
			}else{
				$('#treepackage-plantingcost').val();
			    $('#treepackage-monthlyrecuringcost').val();
				$('#treepackage-packagecost').val();
			}
		}
	}
</script>