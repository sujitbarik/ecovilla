<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TreePackage */

$this->title = $model->PKGID;
$this->params['breadcrumbs'][] = ['label' => 'Tree Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-package-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->PKGID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->PKGID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PackageName',
            [
                'attribute' => 'TDID',
                'value' => $model->treedetail->TreeName,
            ],
            'Duration',
            'CreateDate',
            'UpdateDate',
        ],
    ]) ?>

</div>
