<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TreePackage */

$this->title = 'Update Tree Package: ' . $model->PKGID;
$this->params['breadcrumbs'][] = ['label' => 'Tree Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PKGID, 'url' => ['view', 'id' => $model->PKGID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tree-package-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'alltrees'=>$alltrees,'treeid'=>$treeid
    ]) ?>

</div>
