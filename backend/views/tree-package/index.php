<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tree Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-package-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tree Package', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'PKGID',
            //'TDID',
            'PackageName',
            'PackageCost',
            //'CreateDate',
            //'MonthlyRecuringCost',
            //'CreateDate',
            //'UpdateDate',
            //'Isdelete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
