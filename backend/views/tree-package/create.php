<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TreePackage */

$this->title = 'Create Tree Package';
$this->params['breadcrumbs'][] = ['label' => 'Tree Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'alltrees'=>$alltrees,
    ]) ?>

</div>
