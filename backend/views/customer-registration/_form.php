<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustomerRegistration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-registration-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'EmailId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ContactNo')->textInput() ?>

    <?= $form->field($model, 'Password')->passwordInput() ?>

    <?php //$form->field($model, 'Isdelete')->textInput() ?>

    <?php //$form->field($model, 'Ondate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
