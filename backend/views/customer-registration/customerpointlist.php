<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\BaseUrl;
use yii\helpers\Url;
use common\models\CustomerRegistration;


$this->title = 'Customer Points List';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-index">
    <h2><?= Html::encode($this->title) ?></h2>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
	    <thead>
            <tr>
                <th>#</th>
				<th>Customer Name</th>
				<th>Available Points</th>
				<th>Action</th>
            </tr>
		</thead>
		<tbody>
			<?php 
			if(count($customerdetails)){
				foreach($customerdetails as $key => $value ){
			?>
		   <tr>
				<td><?=($key + 1);?></td>
				<td><?= $value['Name'];?></td>
				<td><?= $value['Points'];?></td>
				<td><button name="addpoints" id="addpoints" class="btn btn-sm btn-info" onclick="addcustomerpoints(<?=$value['Id'];?>);" data-toggle="modal" data-target="#myModal">Add Points</button></td>
		   </tr>
		   <?php
				}
			}
		   ?>
		</tbody>
    </table>
</div>
<!-- Modal form-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">POINTS</h4>
      </div>
      <div class="modal-body" id="customerdata"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="add();">Save</button>
      </div>
    </div>
  </div>
</div>
                <!-- end of modal ------------------------------>
<script>
	function addcustomerpoints(cid){
		
		$.ajax({url:"<?=Url::to(["customer-registration/morepoints"]);?>?cid="+cid,
		   success:function(results)
		   {
			   //alert(results);
		   		$('#customerdata').html(results);
		   }
		});
	}
	function add(){
		var cid = $('#cid').val();
		var pts = $('#cust_points').val();
		var pointstype = $('#points_type').val();
		var ptsdesc = $('#points_add_details').val();
		if(pts!=''){
			if(pts > 0){
				
				$.ajax({url:"<?=Url::to(["customer-registration/pointsadd"]);?>?cid="+cid+"&points="+pts+"&pointdsc="+ptsdesc+"&pointstype="+pointstype,
				   success:function(results)
				   {
					   var res = JSON.parse(results);
					   if(res.status == 1){
						   alert(res.msg);
						   location.reload();
					   }else{
						   alert(res.msg);
						   location.reload();
					   }
				   }
				});
			}
			else{
				alert('Points should be greator than zero..');
			}
		}
		else{
			alert('Please enter the Points..');
		}
	}
</script>