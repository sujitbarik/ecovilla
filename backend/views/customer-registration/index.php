<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CustomerRegistrationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Registrations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-registration-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Customer Registration', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'Id',
            'Name',
            'EmailId:email',
            'ContactNo',
            //'Password:ntext',
            //'Isdelete',
            //'Ondate',

            [
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Action',
				'template'=> '{view}{delete}'
			],
        ],
    ]); ?>
</div>
