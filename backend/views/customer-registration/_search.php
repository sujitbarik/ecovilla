<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustomerRegistrationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-registration-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'Name') ?>

    <?= $form->field($model, 'EmailId') ?>

    <?= $form->field($model, 'ContactNo') ?>

    <?= $form->field($model, 'Password') ?>

    <?php // echo $form->field($model, 'Isdelete') ?>

    <?php // echo $form->field($model, 'Ondate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
