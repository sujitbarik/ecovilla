<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CustomerMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Customer Message', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ID',
            //'Type',
            'Details:ntext',
            'AddedDate',
            //'Ondate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
