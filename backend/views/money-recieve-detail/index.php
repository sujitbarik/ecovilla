<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Money Recieve Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-recieve-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<p>
        <?= Html::a('Create Money Recieve Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'RecieveId',
            [
            'attribute'=>   'BankDetail',
            'label'=>'BankDetail',
            'format' => 'raw',
            'value' => function($data){
                return htmlspecialchars_decode($data->BankDetail);
            }

            ],
            'TezDetail',
            'OnDate',
            //'UpdatedDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
