<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MoneyRecieveDetail */

$this->title = 'Update Money Recieve Detail: ' . $model->RecieveId;
$this->params['breadcrumbs'][] = ['label' => 'Money Recieve Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->RecieveId, 'url' => ['view', 'id' => $model->RecieveId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="money-recieve-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
