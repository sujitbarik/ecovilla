<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MoneyRecieveDetail */

$this->title = 'Create Money Recieve Detail';
$this->params['breadcrumbs'][] = ['label' => 'Money Recieve Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-recieve-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
