<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MoneyRecieveDetail */

$this->title = $model->RecieveId;
$this->params['breadcrumbs'][] = ['label' => 'Money Recieve Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-recieve-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->RecieveId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->RecieveId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'RecieveId',
            'BankDetail:ntext',
            'TezDetail',
            'OnDate',
            'UpdatedDate',
        ],
    ]) ?>

</div>
