<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CustomerRegistration;
use common\models\Crops;
use common\models\Farm;
use yii\widgets\ActiveForm;

$nextsunday = strtotime('next sunday');
$d1 = date("Y-m-d",$nextsunday);
$this->title = 'Upcoming Tree Plantation';
$this->params['breadcrumbs'][] = $this->title;


//echo "<pre>"; var_dump($allupcomingrent);
?>
<div class="product-index">
    <h2 style="color:#9dbdf2; font-family: Georgia, serif;"><?= strtoupper(Html::encode($this->title)." on ".date("d-M-y",strtotime($d1))); ?></h2>
	<?php $form = ActiveForm::begin(['action' => ['site/plantationstatuschange']]); ?>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
	    <thead>
            <tr>
                <th>#</th>
				<th>Customer Name</th>
				<th>Farm Name</th>
				<th>Crop Name</th>
				<th>Plantation Action</th>
            </tr>
		</thead>
		<tbody>
			<?php
			if(count($allupcomingrent)){
				foreach($allupcomingrent as $key=>$value){
			?>
		    <tr>
				<td><?=($key+1)?></td>
				<td><?=CustomerRegistration::getCustomername($value['CustomerId']);?></td>
				<td><?=Farm::getFarmname($value['FarmId']);?></td>
				<td><?=Crops::getCropname($value['CropId']);?></td>
				<td>
					<input type="hidden" name="RentID[]" id="" value="<?=$value['RentId']?>"/>
					<input type="checkbox" name="Status[<?=$value['RentId']?>][]" id="" />
				</td>
			</tr>
			<?php
				}
			?>			
		
			<tr>
				<td colspan="5" style="text-align:center">
					<input type="submit" name="Save" value="Plant" class="btn" />
				</td>
			</tr>
		</tbody>
		<?php 
			}else{
		?>
		<tbody>
		    <tr>
				<td colspan="5">No record found .............</td>
			</tr>
		</tbody>
		<?php
			}
		?>
    </table>
	<?php ActiveForm::end(); ?>
</div>