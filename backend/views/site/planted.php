<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CustomerRegistration;
use common\models\Crops;
use common\models\Farm;
use yii\widgets\ActiveForm;

$this->title = 'All Planted';
$this->params['breadcrumbs'][] = $this->title;


//echo "<pre>"; var_dump($allupcomingrent);
?>
<div class="product-index">
    <h2 style="color:#9dbdf2; font-family: Georgia, serif;">
	   <?= strtoupper(Html::encode($this->title)); ?></h2>
	<?php $form = ActiveForm::begin(['action' => ['site/plantationstatuschange']]); ?>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
	    <thead>
            <tr>
                <th>#</th>
				<th>Customer Name</th>
				<th>Farm Name</th>
				<th>Crop Name</th>
				<th>Crop Book Date</th>
				<th>Planted Date</th>
            </tr>
		</thead>
		<tbody>
			<?php
			if(count($allplanted)){
				foreach($allplanted as $key=>$value){
			?>
		    <tr>
				<td><?=($key+1)?></td>
				<td><?=CustomerRegistration::getCustomername($value['CustomerId']);?></td>
				<td><?=Farm::getFarmname($value['FarmId']);?></td>
				<td><?=Crops::getCropname($value['CropId']);?></td>
				<td><?=date("d-M-Y",strtotime($value['CropBookingDate']));?></td>
				<td><?=date("d-M-Y",strtotime($value['CropPlantationDate']));?></td>
			</tr>
			<?php
				}
			}
			else{
			?>
			<tr>
				<td colspan="5">No record found .............</td>
			</tr>
			<?php 
			}
			?>
		</tbody>
		
    </table>
	<?php ActiveForm::end(); ?>
</div>