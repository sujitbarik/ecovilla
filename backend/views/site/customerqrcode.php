<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CustomerRegistration;
use common\models\Crops;
use common\models\Farm;
use yii\widgets\ActiveForm;
use common\models\TreeDetails;
use common\models\TreePackage;
use common\models\TreesImage;
use common\models\TreePlantation;

$this->title = 'Customer QRCode';
$this->params['breadcrumbs'][] = $this->title;


//echo "<pre>"; var_dump($allupcomingrent);
?>
<div class="product-index">
    <h2 style="color:#9dbdf2; font-family: Georgia, serif;"><?= strtoupper(Html::encode($this->title)); ?></h2>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
	    <thead>
            <tr>
                <th>#</th>
				<th>Customer Name</th>
				<th>Tree Name</th>
				<th>Booking Date</th>
				<th>Plantation Date</th>
				<th>QRCode</th>
            </tr>
		</thead>
		<tbody>
			<?php
			if(count($qrcodedata)){
				//echo "<pre>"; var_dump($qrcodedata);
				foreach($qrcodedata as $key=>$value){
			?>
		    <tr>
				<td><?=($key+1)?></td>
				<td><?=$value['CustomerName']?></td>
				<td><?=$value['TreeName']?></td>
				<td><?=$value['BookingDate']?></td>
				<td><?=$value['PlantationDate']?></td>
				<td><img src="<?= $value['QRCode']?>"  /></td>
			</tr>
			<?php
				}
			?>			
		
			<tr>
				<td colspan="6" style="text-align:center">
					<input type="submit" name="Save" value="Plant" class="btn" />
				</td>
			</tr>
		</tbody>
		<?php 
			}else{
		?>
		<tbody>
		    <tr>
				<td colspan="6">No record found .............</td>
			</tr>
		</tbody>
		<?php
			}
		?>
    </table>
</div>