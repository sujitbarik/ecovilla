<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CustomerRegistration;
use common\models\Crops;
use common\models\Farm;
use yii\widgets\ActiveForm;
use common\models\TreeDetails;
use common\models\TreePackage;
use common\models\TreesImage;
use common\models\TreePlantation;

$nextsunday = strtotime('next sunday');
$d1 = date("Y-m-d",$nextsunday);
$this->title = 'Upcoming Plantation';
$this->params['breadcrumbs'][] = $this->title;


//echo "<pre>"; var_dump($allupcomingrent);
?>
<div class="product-index">
    <h2 style="color:#9dbdf2; font-family: Georgia, serif;"><?= strtoupper(Html::encode($this->title)." on ".date("d-M-y",strtotime($d1))); ?></h2>
	<?php $form = ActiveForm::begin(['action' => ['site/treeplantationstatuschange']]); ?>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
	    <thead>
            <tr>
                <th>#</th>
				<th>Customer Name</th>
				<th>Tree Name</th>
				<th>Package Name</th>
				<th>Booking Date</th>
				<th>Plantation Action</th>
            </tr>
		</thead>
		<tbody>
			<?php
			if(count($treeplant))
            {
				foreach($treeplant as $key=>$value)
                {
			?>
		    <tr>
				<td><?=($key+1)?></td>
				<td><?=CustomerRegistration::getCustomername($value['CustomerID']);?></td>
				<td><?=TreePackage::getTreename($value['TPID']);?></td>
				<td><?=TreePackage::getTreepackage($value['TPID']);?></td>
				<td><?=date("d-m-Y",strtotime($value['AddedDate']));?></td>
				<td>
					<input type="hidden" name="RentID[]" id="" value="<?=$value['ID']?>"/>
					<input type="checkbox" name="Status[<?=$value['ID']?>][]" id="" />
				</td>
			</tr>
			<?php
				}
			?>			
		
			<tr>
				<td colspan="6" style="text-align:center">
					<input type="submit" name="Save" value="Plant" class="btn" />
				</td>
			</tr>
		</tbody>
		<?php 
			}else{
		?>
		<tbody>
		    <tr>
				<td colspan="6">No record found .............</td>
			</tr>
		</tbody>
		<?php
			}
		?>
    </table>
	<?php ActiveForm::end(); ?>
</div>