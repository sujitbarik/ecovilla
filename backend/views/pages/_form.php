<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PageName')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'PageContents')->textarea(['rows' => 6]) ?>
	<?= $form->field($model, 'PageContents')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>

    <?php //$form->field($model, 'AddedDate')->textInput() ?>

    <?php //$form->field($model, 'Ondate')->textInput() ?>

    <?php //$form->field($model, 'IsDelete')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
