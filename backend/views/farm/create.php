<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Farm */

$this->title = 'Create Farm';
$this->params['breadcrumbs'][] = ['label' => 'Farms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="farm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'FarmImage'=>$FarmImage,'size'=>$size
    ]) ?>

</div>
