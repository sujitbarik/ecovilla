<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Farms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="farm-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Farm', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			'FarmName',
            //'FarmID',
             ['attribute'=>'Size',
            'value'=>'size.Size'
            ],
            'Price',
            //'SoilReport',
            //'Location:ntext',
            'Duration',
            //'IsDelete',
            'OnDate',
            //'UpdatedDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
