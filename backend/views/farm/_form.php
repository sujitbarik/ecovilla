<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Farm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="farm-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'FarmName')->textInput() ?>
	
    <?= $form->field($model, 'Size')->dropDownList($size,['prompt'=>'Select...'])?>

    <?= $form->field($model, 'Price')->textInput() ?>

    <?= $form->field($model, 'SoilReport')->fileInput() ?>

    <?= $form->field($model, 'Location')->textarea(['rows' => 6]) ?>

    
	<?= $form->field($model, 'Duration')->dropDownList(
				[
					'1' => '1 Year',
					'2' => '2 Year',
					'3' => '3 Year',
					
				]
			);
	?>
    
    <?= $form->field($model, 'Stock')->textInput() ?>
    <div style="width: 100%;float: left;">
    <?php
    if($model->farmImages)
    {
        foreach($model->farmImages as $fk=>$fval)
        {
    ?>
    <div style="width: 200px;float: left;position: relative;" id="farmimage<?=$fval->FarmImageID;?>">
        <img src="<?=Yii::$app->params['imageurl'].$fval->image->Image;?>" style="height: 150px;"/>
        <span style="position: absolute;font-weight:bold;top: 0px;right: 0px;cursor: pointer;" onclick="farmimagedelete(<?=$fval->FarmImageID;?>);">X</span>
    </div>
    <?php
        }
    }
    ?>
    </div>
    <div id="farmimage">
    <?= $form->field($FarmImage, 'ImageID[]')->fileInput() ?>
    </div>
    <div id="allfarmimage"></div>
    <span class="addmore" onclick="addmore();" style="display:none;">+ Add More</span>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
    function addmore()
    {
        $('#allfarmimage').append($('#farmimage').html());
    }
    
    function farmimagedelete(farmid)
    {
        $.ajax({url:"<?=Url::toRoute(['farm/farmimagedelete']);?>?farmid="+farmid,
               success:function(results)
                {
                    var res=JSON.parse(results);
                    if(res==1)
                    {
                        alert('Farm Image deleted successfully');
                        $('#farmimage'+farmid).remove();
                    }
                }
        });
    }
</script>