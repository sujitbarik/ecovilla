<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Farm */

$this->title = 'Update Farm: ' . $model->FarmID;
$this->params['breadcrumbs'][] = ['label' => 'Farms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->FarmID, 'url' => ['view', 'id' => $model->FarmID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="farm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'FarmImage'=>$FarmImage,'size'=>$size
    ]) ?>

</div>
