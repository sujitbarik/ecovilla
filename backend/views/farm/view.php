<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Farm */

$this->title = $model->FarmID;
$this->params['breadcrumbs'][] = ['label' => 'Farms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="farm-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->FarmID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->FarmID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'FarmID',
            'Size',
            'Price',
            'SoilReport',
            'Location:ntext',
            'Duration',
            'IsDelete',
            'OnDate',
            'UpdatedDate',
        ],
    ]) ?>

</div>
