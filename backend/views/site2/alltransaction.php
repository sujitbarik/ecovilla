<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CustomerRegistration;


$this->title = 'Transaction Report';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-index">
    <h2><?= Html::encode($this->title) ?></h2>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
	    <thead>
            <tr>
                <th>#</th>
				<th>Customer Name</th>
				<th>Opening Balance</th>
				<th>Closing Balance</th>
				<th>Transaction Details</th>
				<th>Transaction Date</th>
            </tr>
		</thead>
		<tbody>
		    <?php 
			if(count($alltransactionlist)){
				foreach($alltransactionlist as $key => $value){
			?>
			<tr>
				<td><?= ($key+1);?></td>
				<td><?=CustomerRegistration::getCustomername($value['CustomerID']);?></td>
				<td><?=$value['OpeningBalance'];?></td>
				<td><?=$value['ClosingBalance'];?></td>
				<td><?=$value['Details'];?></td>
				<td><?=date("d-M-Y h:i:s A",strtotime($value['Ondate']));?></td>
			</tr>
			<?php
				}
			}
			?>
		</tbody>
    </table>
</div>