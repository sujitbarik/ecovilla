<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\CustomerRegistration;

class AppController extends Controller{
    public function beforeAction($action){ 
		
		$this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function behaviors(){
		
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'corsFilter' => [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                
                       'Origin' => ['*'],
                       'Access-Control-Request-Method' => ['POST', 'PUT','GET','REQUEST'],
                    ],

            ],
        ];
    }
    
    public function actionForgotpassword(){
        $this->layout="blanklayout";
        $model= new CustomerRegistration();
        $result=Yii::$app->request->post();
        $email=$result['email'];
            $chk=$model->find()->where(['EmailId'=>$result['email']])->one();
            $count=count($chk);
            if($count>0)
            {
                   $ucode=rand();
                   $model=Yii::$app->db->createCommand("UPDATE `CustomerRegistration` SET `uniquecode`='$ucode',`status`='0' where `EmailId`='$email'");
                   $model=$model->query();
                   $to=$email;
                   $from='info@echovilla.in';
                   $subject="Create a new Password";
                   
                  $html= "<html>
                  <head>
                  <title>Create a new password</title>
                  </head>
                  <body>
                  <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:rgb(183, 220, 54);text-align:center'>
                   <tbody><tr>
                                          <td><img src='http://ecovilla.in/images/logo.png' title='Echovilla' alt='Echovilla' style='margin-top:10px;width:150px;'></td>
                                      </tr>
                                      <tr>
                                          <td style='height:30px'></td>
                                      </tr>
                              <tr>
                                          <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://ecovilla.in' target='_blank'> Echovilla </a>
                  <br><br>
                  <span style='font-size:16px;line-height:1.5'>
                  Please click in the link to create your new password
                  <br/>
                  <span style='font-size:10px;line-height:1.5'>        
                  http://ecovilla.in/app/createpassword?ucode=$ucode
                  </span> 
                  </span>
                  </h2>
                  </td>
                  </tr>
                  </tbody>
                  </table>
                  </body>
                  </html>";
                   $mail= new ContactForm();
                  $mail->sendEmail($to,$from,$html,$subject);
                  $msg="Please check your mail to get a link for create a new password...";
                  $status = 1;
                  
            }
            else
            {
                $msg="Please give your registered Email Id for create a new password...";
                $status = 0;
            }
            
        $returnarray=array();
        $returnarray['status'] = $status;
        $returnarray['msg'] = $msg;
        echo json_encode($returnarray);
    }
    
    public function actionCreatepassword($ucode)
    {
        $this->layout='newmain';
        $model = new CustomerRegistration();
        $chk=$model->find()->where(['uniquecode'=>$ucode])->one();
        
        if ($model->load(Yii::$app->request->post())) {
            $hash = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post()['CustomerRegistration']['Password']);
            $chk->Password=$hash;
            $chk->save();
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->redirect(['site/index']);
            }
            else
            {
                if($chk && $chk->status==0)
                {
                    $chk->status=1;
                    $chk->save();
                    return $this->render('//site/resetPassword', ['ucode'=>$ucode,'model' => $model,]);
                }
                else
                {
                    Yii::$app->session->setFlash('error', 'Link expired, Please give your registered EmailId for create a new password.');
                     return $this->render('//site/requestPasswordResetToken', ['model'=>$model]); 
                }
            }
    }
    
    public function actionRequestpassword()
    {
        $model = new CustomerRegistration();
        if ($model->load(Yii::$app->request->post())) {
            $email=Yii::$app->request->post('CustomerRegistration')['EmailId'];
         $chk=$model->find()->where(['EmailId'=>$email])->one();
            $count=count($chk);
            if($count>0)
            {
                   $ucode=rand();
                   $model=Yii::$app->db->createCommand("UPDATE `CustomerRegistration` SET `uniquecode`='$ucode',`status`='0' where `EmailId`='$email'");
                   $model=$model->query();
                   $to=$email;
                   $from='info@echovilla.in';
                   $subject="Create a new Password";
                   
                  $html= "<html>
                  <head>
                  <title>Create a new password</title>
                  </head>
                  <body>
                  <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:rgb(183, 220, 54);text-align:center'>
                   <tbody><tr>
                                          <td><img src='http://ecovilla.in/images/logo.png' title='Echovilla' alt='Echovilla' style='margin-top:10px;width:150px;'></td>
                                      </tr>
                                      <tr>
                                          <td style='height:30px'></td>
                                      </tr>
                              <tr>
                                          <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://ecovilla.in' target='_blank'> Echovilla </a>
                  <br><br>
                  <span style='font-size:16px;line-height:1.5'>
                  Please click in the link to create your new password
                  <br/>
                  <span style='font-size:10px;line-height:1.5'>        
                  http://ecovilla.in/app/createpassword?ucode=$ucode
                  </span> 
                  </span>
                  </h2>
                  </td>
                  </tr>
                  </tbody>
                  </table>
                  </body>
                  </html>";
                   $mail= new ContactForm();
                  $mail->sendEmail($to,$from,$html,$subject);
               Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
         }
         else
         {
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
         }
         
         return $this->redirect(['site/index']);
        }
        
    }
}

?>
