<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<link rel="shortcut icon" href="/favicon.ico?v=1" type="image/x-icon" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Creative Digital Agency Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <?= Html::csrfMetaTags() ?>
	
    <title>Eco Villa - Self Farming for good Food.</title>
    <?php $this->head() ?>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<!-- start-smooth-scrolling -->
		
<style> 
.carousel-control {
  padding-top:10%;
  width:5%;
}
.carousel-control.left{ background:none !important;}
</style>
</head>
<body>
<?php $this->beginBody() ?>
<!-- banner -->
<div class="banner">
	<div class="container">
		<div class="header-grids">
			<div class="container">
				<div class="fixed-header">
					<div class="header-left">
						<a href="<?= Url::to(['site/index'])?>"><?= Html::img('@web/images/logo.png',['style'=>'height:90px;'])?></a>
					</div>
					<div class="header-right">
						<span class="menu"><?= Html::img('@web/images/menu.png')?></span>
								<ul class="nav1">
									<li><a class="active" href="<?= Url::to(['site/index'])?>">Home</a></li>
									<li><a href="<?= Url::to(['site/planttree'])?>">Plant A Tree</a></li>
									<li><a href="<?= Url::to(['site/selffirm'])?>"> Self Farming </a></li>
									<li><a href="<?= Url::to(['site/whoarewe'])?>"> Who Are We</a></li>
									<li><a href="<?= Url::to(['site/contactus'])?>">Contact</a></li>
								</ul>
								
								

					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		


       
        <?= $content ?>
       <?= $this->render('common/_footer.php') ?>

    </div>
<!-- //footer -->
<!-- here stars scrolling icon -->
	
	
	
	
<!-- //here ends scrolling icon -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
