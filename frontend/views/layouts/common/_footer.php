<footer class="footer_bar">
  <div class="container">
    <div class="col-md-3 ">
      <div class="footerbox">
        <div class="footerboxtop">ABOUT ECO Villa </div>
        <p class="paragraph top10">Ecovilla is a new generation in the field of cooperative farming. We hope to establish new standards in collaborative environment friendly farming.</p>
        <span class="rmorf"> <a href="#">read more </a> </span> </div>
    </div>
    <div class="col-md-3">
      <div class="footerbox">
        <div class="footerboxtop">Trees </div>
        <ul class="footer_menu_prd">
          <li><a href="#">Mango Trees</a></li>
          <li><a href="#">Banyan</a></li>
          <li><a href="#">Peepal</a></li>
          <li><a href="#">Jackfruit</a></li>
          <li><a href="#">Neem Tree</a></li>
        </ul>
      </div>
    </div>
    <div class="col-md-3">
      <div class="footerbox">
        <div class="footerboxtop">useful links  </div>
        <ul class="footer_menu_prd">
          
          <li><a href="#"> contact us</a></li>
          <li><a href="#">FAQs</a></li>
          <li><a href="#">Privacy Policy</a></li>
          <li><a href="#">Terms and Conditions</a></li>
		  
		  


		  
        </ul>
      </div>
    </div>
    <div class="col-md-3">
      <div class="footerbox">
        <div class="footerboxtop">social media world </div>
		<style> 
			
			

/*=========================
  Icons
 ================= */

/* footer social icons */
ul.social-network {
	list-style: none;
	display: inline;
	margin-left:0 !important;
	padding: 0;
}
ul.social-network li {
	display: inline;
	margin: 0 5px;
}


/* footer social icons */
.social-network a.icoRss:hover {
	background-color: #F56505;
}
.social-network a.icoFacebook:hover {
	background-color:#3B5998;
}
.social-network a.icoTwitter:hover {
	background-color:#33ccff;
}
.social-network a.icoGoogle:hover {
	background-color:#BD3518;
}
.social-network a.icoVimeo:hover {
	background-color:#0590B8;
}
.social-network a.icoLinkedin:hover {
	background-color:#007bb7;
}
.social-network a.icoRss:hover i, .social-network a.icoFacebook:hover i, .social-network a.icoTwitter:hover i,
.social-network a.icoGoogle:hover i, .social-network a.icoVimeo:hover i, .social-network a.icoLinkedin:hover i {
	color:#fff;
}
a.socialIcon:hover, .socialHoverClass {
	color:#44BCDD;
}

.social-circle li a {
	display:inline-block;
	position:relative;
	margin:0 auto 0 auto;
	-moz-border-radius:50%;
	-webkit-border-radius:50%;
	border-radius:50%;
	text-align:center;
	width: 50px;
	height: 50px; background:#129c2f;
	font-size:20px;
}
.social-circle li i {
	margin:0;
	line-height:50px;
	text-align: center;
}

.social-circle li a:hover i, .triggeredHover {
	-moz-transform: rotate(360deg);
	-webkit-transform: rotate(360deg);
	-ms--transform: rotate(360deg);
	transform: rotate(360deg);
	-webkit-transition: all 0.2s;
	-moz-transition: all 0.2s;
	-o-transition: all 0.2s;
	-ms-transition: all 0.2s;
	transition: all 0.2s;
}
.social-circle i {
	color: #fff;
	-webkit-transition: all 0.8s;
	-moz-transition: all 0.8s;
	-o-transition: all 0.8s;
	-ms-transition: all 0.8s;
	transition: all 0.8s;
}


			
			</style>
        <ul class="social-network social-circle">
          <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
        </ul>
        <ul class="kd-userinfo">
          <li> <i class="fa fa-map-marker"></i>
            <p>3637 Chandrasekharpur, District Center, Besides Tanishq Showroom,
    Bhubaneswar, 751021</p>
          </li>
          <li> <i class="fa fa-phone"></i>
            <p>+91 8599092666</p>
          </li>
          <li> <i class="fa fa-envelope-o"></i>
            <p>contact@ecovilla.in</p>
          </li>
          <li> <i class="fa fa-home"></i>
            <p>www.ecovilla.in</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="copy"> © 2018 | ALL RIGHTS RESERVED </div>
</footer>