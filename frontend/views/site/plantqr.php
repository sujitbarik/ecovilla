<?php

//echo $id = Yii::$app->request->getQueryParam('id');

//Yii::app()->request->getUrl();
//
//Yii::app()->request->getParams('id');
//
 $id=$_GET['id'];
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$url=str_replace('frontend/web','backend',Yii::$app->getUrlManager()->getBaseUrl());
//foreach($treearr as $val=>$key){
//   echo $key.'<br/>';
//}

//echo $value;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<title>Untitled Document</title>
<style>
	html,body{margin:0px; padding:0px;}
	h4{color:#669900; width:100%; font-weight:normal; font-size:14px; float:left; border-bottom:1px dashed #dbdbdb;  font-family:Arial, Helvetica, sans-serif;line-height:1.7; padding:1%; margin-bottom:5px; padding-bottom:5px; margin-top:0px; color:#888;}
	h4 span{color:#333;}
	h3{color:#669900; font-weight:normal; margin:0px;padding:0px; font-size:17px; font-family:Arial, Helvetica, sans-serif;}
</style>
</head>

<body>

	<div style="width:100%; height:auto; float:left; background:#efefef; text-align:center; padding-top:5px; padding-bottom:5px; border-bottom:1px solid #dbdbdb;">
		<?= Html::img('@web/images/plantqr/logo.png',['style'=>'height:40px' ])?>
	</div>
	<div style="width:100%; height:auto; float:left; padding-bottom:20px;background: #56ab2f; text-align:center; color:#fff; font-size:17px; line-height:1.5; text-shadow:1px 1px 1px #333; font-family:Arial, Helvetica, sans-serif;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #a8e063, #56ab2f);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #a8e063, #56ab2f); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
">
		<img src="<?=$url.'/'.$timage1;?>" style="border-radius:10%; border:2px solid #fff; margin-top:20px;height:110px;"/><br/>
		<?=$tname;?><br/>
		<span style="font-size:15px; color:#333; background:#fff; padding:3px; text-shadow:none; padding-left:10px; padding-right:10px;"><?=$plantationdate;?></span>

	</div>
	<div style="width:100%;height:auto; float:left; text-align:center; background:#fff;">
		<div style="width:auto; margin:auto; display:inline-block;">
		<h3>All Photos</h3>
        <?php
        foreach($alltreeimage as $imageval){
        ?>
		<img src="<?=$url.'/'.$imageval->Image;?>"  style="float:left; margin:1px;height:50px;"/>
        <?php
        }
        ?>
        
        <!---
		<img src="m1.jpg" height="50" style="float:left;  margin:1px;"/>
		<img src="m1.jpg" height="50" style="float:left;  margin:1px;"/>
		<img src="m1.jpg" height="50" style="float:left; margin:1px; "/>--->
		</div>
	</div>
	<div style="width:96%; padding:1%; float:left; height:20px; margin-left:1%; background:#fff; ">
			<h4><?= Html::img('@web/images/plantqr/user.png',['style'=>'float:left; margin-right:5px; height:20px;' ])?> Customer Name: <span> <?=$cname;?></span></h4>
			
			<h4><?= Html::img('@web/images/plantqr/calendar.png',['style'=>'float:left; margin-right:5px; height:20px;' ])?>
            Plantation Date:<span> <?=$plantationdate;?></span></h4>
			<h4><?= Html::img('@web/images/plantqr/article.png',['style'=>'float:left; margin-right:5px; height:20px;' ])?>
                Description: <span><?=$tdescription;?></span></h4>
			<h4><?= Html::img('@web/images/plantqr/tree.png',['style'=>'float:left; margin-right:5px; height:20px;' ])?>
            Other Trees: <span><?=$treename;?></span></h4>
			
		</h4>
	</div>

</body>
</html>
