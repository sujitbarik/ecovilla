<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\helpers\Html;
use yii\helpers\Url;
 $this->title='Index';
?>
<div class="slider">
			
							
			<div  id="top" class="callbacks_container">
				<ul class="rslides" id="slider3">
					<li>
						<div class="banner-info">
							<h1>Green trees </h1>
							<h2>Essential for Life </h2>
							<div class="border"></div>
							<p> Almost everyone contributes their daily share of carbon and greenhouse gasses to the environment. But how many trees do we plant to negate the effect? Perhaps none.
							<br />
							Now rely on us to plant the tree and take care of it for you while you track the growth and stay connected to it. 
							</p>
							
							
						</div>
					</li>
					<li>
						<div class="banner-info">
							<h1>Healthy Vegetable</h1>
							<h2>Good Food Healthy Life</h2>
							<div class="border"></div>
							<p>Our daily food is laxed with pesticides, herbicides and what not? How would you like to contol the environment where your vegetables are grown?
							<br />
							We let people choose the vegetables they want to grow, grow it organically and track the growth all from their handheld devices. Now grow your food the way you want to. 
							</p>							
						</div>
					</li>
					
				</ul>
			</div>
		</div>
		<div class="down-arrow text-center">
			<a class="scroll" href="#hello"> <?= Html::img('@web/images/scroll.png')?></a>
		</div>
		
	</div>		
</div>
<!-- //banner -->
<!-- clients -->
<div class="clients">
	
		<div class="client-grids">
			<div class="client-left">
				<h3>Popular Trees</h3>
			</div>
			<div class="client-right">
				<ul>
					<li><?= Html::img('@web/images/1.png')?></li>
					<li><?= Html::img('@web/images/2.png')?></li>
					<li><?= Html::img('@web/images/3.png')?></li>
					<li><?= Html::img('@web/images/4.png')?></li>
					<li><?= Html::img('@web/images/25.png')?></li>
					<li><?= Html::img('@web/images/23.png')?></li>
					<li><?= Html::img('@web/images/3.png')?></li>
					
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	
</div>
<!-- //clients -->
<!-- hello -->
<div id="hello" class="hello">
	<div class="container" style="height:auto; padding-top:20px; padding-bottom:10px;">
		<div class="col-md-7 hello-info a" style="margin-top:20px;">
			 <?= Html::img('@web/images/veg.png',['style'=>'width:90%;'])?>
		</div>
		<div class="col-md-5 hello-info">
			<h3>Growing greens <span>One Tree at a time</span></h3>
			<div class="strip"></div>
			<p>Almost all of us contribute immenselt to carbon and greenhouse emissons. We definitely know how to negate the impacts. <strong>Trees Definitely.</strong> But how many of us do actually plant a tree? </p>
			<p>
				Our experiences have brought us to a lot many people who want to plant trees but have been unable owing to lack of space and time. Now, let us manage it for you. Just order to plant a tree through our app and we do it for you. The app not only allows you to plant the tree but also gives you updates on the health and the growth of the tree.
				<br /><br />
				We on our part ensure the tree is never cut down.* 
			</p>
			<a href="<?= Url::to(['site/planttree'])?>" class="hvr-rectangle-out button">Read how it works</a>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //hello -->
<!-- experience -->
<div class="experience" style="height:auto; padding-top:20px; padding-bottom:20px;">
	<div class="container">
		<div class="col-md-6 exp-left">
			<h3>A simple step<span>Towards a healthy life</span></h3>
			<div class="strip"></div>
			<p>Almost all the crops and vegetables we eat is today laced with pesticides and other chemical agents. We definitely know the impacts they cause on our health. But lack of alternatives has ensured we continue consuming them. 
				<br />
				<br />
			How would you like to control the way your food is grown, control the chemicals which are sprayed on them, have a report of the soil on which they are grown and also a test on how much pesticides and other chemicals they contain before you use it for cooking?
				<br /><br />
			At the present day; Sounds pretty impossible, but we do have a solution.
			</p>
			<a href="<?= Url::to(['site/selffirm'])?>" class="hvr-rectangle-out button">Find out more</a>
			<br />
		</div>
		<div class="col-md-6 exp-right" style="margin-top:20px;">
			<div class="col-md-6 exp-grid text-center">
				<div class="exp-image a"></div>
				<p>Mango Plant</p>
			</div>
			<div class="col-md-6 exp-grid text-center">
				<div class="exp-image b"></div>
				<p>Jackfruit plant</p>
			</div>
			<div class="col-md-6 exp-grid text-center">
				<div class="exp-image c"></div>
				<p>Neem Plant </p>
			</div>
			<div class="col-md-6 exp-grid text-center">
				<div class="exp-image d"></div>
				<p>Teak  plant</p>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //experience -->
<!-- work -->
<div class="recent-work" >
	<div class="container-fluid">
		
		<div class="recent-btm">
				<div class="col-md-3 col-xs-12 recent-left carbon-left">
					<?= Html::img('@web/images/tree.png')?>
				</div>
				<div class="col-md-9 col-xs-12 carbon-right">
					<div class="recent-info text-center carbon-right-top" style="display:none;">
						<h3> <?= Html::img('@web/images/cr.jpg', ['style'=>'height:150px;'])?></h3>
					
					</div>
					<div class="carbon-right-bottom" style="text-align:center;">
					<iframe src="http://ecovilla.in/carbon" scrolling="no" ></iframe>
					</div>
					
				</div>
				<div class="clearfix"></div>
		</div>
	</div>
</div>





<div class="vc_row wpb_row vc_row-fluid vc_custom_1516930700513" style="padding-top:70px; display:none;">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div class="text-center  organiko-title-tpl1 ">
               <span class="organiko-subtitle">We Are Organic farmfood</span>
               <h2 class="organiko-title" style="color: #67b20d">
                  Our Products            
               </h2>
            </div>
            <div class="vc_empty_space" style="height: 60px"><span class="vc_empty_space_inner"></span></div>
            <div class="woocommerce tb-product-carousel tb-product-carousel4 tpl1 " id="15362163375b90cd11554f8">
               <div class="container">
               <div class="tb-products-grid">
                  <div class="owl-carousel slick-initialized slick-slider" role="toolbar">
                     <button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: inline-block;">Previous</button>
                     <div aria-live="polite" class="slick-list draggable">
                        <div class="slick-track" style="opacity: 1; width: 4200px; transform: translate3d(-1200px, 0px, 0px);" role="listbox">
                           
                           
                           
                           
                           <div class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 300px;" tabindex="0" role="option" aria-describedby="slick-slide20">
                              <div>
                                 <div class="products-widget post-976 product type-product status-publish has-post-thumbnail product_cat-dried-products  instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onnew">New</span><span class="onsale">-5%</span></div>
                                          <a class="tb-normal-effect" href="http://organiko.jwsuperthemes.com/product/honey/" tabindex="0">
                                          <img src="images/product3-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/product3-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">					</a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="976" tabindex="0"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=976" data-quantity="1" data-product_id="976" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="0">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-976">
                                                   
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/honey/" tabindex="0">Honey</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="976"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>84.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>80.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-974 product type-product status-publish has-post-thumbnail product_cat-dried-products product_tag-meat  instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onnew">New</span><span class="onsale">-29%</span></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/dried-strawberries/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="0">
                                          <img src="images/product2-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/product2-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/product2-1-300x300.jpg" alt="Dried Strawberries">
                                          </span>
                                          </a>
                                         
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="#" tabindex="0">Dried Strawberries</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="974"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>84.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>60.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 300px;" tabindex="0" role="option" aria-describedby="slick-slide21">
                              <div>
                                 <div class="products-widget post-207 product type-product status-publish has-post-thumbnail product_cat-vegetables last instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onnew">New</span><span class="onsale">-4%</span></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/bell-pepper/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="0">
                                          <img src="images/item8-1-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item8-1-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/item7-1-300x300.jpg" alt="Bell Pepper">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="207" tabindex="0"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=207" data-quantity="1" data-product_id="207" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="0">Add to cart</a>					
                                             </div>
                                            
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="#" tabindex="0">Bell Pepper</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="207"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>146.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>140.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-349 product type-product status-publish has-post-thumbnail product_cat-vegetables product_tag-product first instock shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/bell-pepper-copy/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="0">
                                          <img src="images/item2-1-300x300.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item2-1-300x300.png" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/product2-300x300.jpg" alt="Bell Pepper">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="349" tabindex="0"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=349" data-quantity="1" data-product_id="349" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="0">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="#" tabindex="0">Bell Pepper</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="349"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>150.00</span></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 300px;" tabindex="0" role="option" aria-describedby="slick-slide22">
                              <div>
                                 <div class="products-widget post-969 product type-product status-publish has-post-thumbnail product_cat-dried-products  instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onsale">-23%</span></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/organic-lentils/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="0">
                                          <img src="images/product1-3-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/product1-3-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/product2-1-300x300.jpg" alt="Organic Lentils">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="969" tabindex="0"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=969" data-quantity="1" data-product_id="969" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="0">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-969">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=969" rel="nofollow" data-product-id="969" data-product-type="simple" class="add_to_wishlist" tabindex="0">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="0">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="0">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/organic-lentils/" tabindex="0">Organic Lentils</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="969"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>22.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>17.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-204 product type-product status-publish has-post-thumbnail product_cat-vegetables  instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onsale">-42%</span></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/fresh-cherry/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="0">
                                          <img src="images/item7-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item7-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/item8-1-1-300x300.jpg" alt="Fresh Cherry">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="204" tabindex="0"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=204" data-quantity="1" data-product_id="204" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="0">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-204">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=204" rel="nofollow" data-product-id="204" data-product-type="simple" class="add_to_wishlist" tabindex="0">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="0">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="0">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/fresh-cherry/" tabindex="0">Fresh Cherry</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="204"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>110.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>64.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="slick-slide slick-active" data-slick-index="3" aria-hidden="false" style="width: 300px;" tabindex="0" role="option" aria-describedby="slick-slide23">
                              <div>
                                 <div class="products-widget post-202 product type-product status-publish has-post-thumbnail product_cat-vegetables last instock shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/para-cotufas/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="0">
                                          <img src="images/item6-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item6-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/product1-2-300x300.jpg" alt="Para Cotufas">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="202" tabindex="0"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=202" data-quantity="1" data-product_id="202" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="0">Add to cart</a>					
                                             </div>
                                            
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/para-cotufas/" tabindex="0">Para Cotufas</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="202"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>145.00</span></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-200 product type-product status-publish has-post-thumbnail product_cat-vegetables first instock shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/fresh-tomato/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="0">
                                          <img src="images/item5-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item5-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/item7-1-300x300.jpg" alt="Fresh Tomato">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="200" tabindex="0"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=200" data-quantity="1" data-product_id="200" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="0">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                               
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/fresh-tomato/" tabindex="0">Fresh Tomato</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="200"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>20.00</span></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="slick-slide" data-slick-index="4" aria-hidden="true" style="width: 300px;" tabindex="-1" role="option" aria-describedby="slick-slide24">
                              <div>
                                 <div class="products-widget post-197 product type-product status-publish has-post-thumbnail product_cat-vegetables  instock shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"></div>
                                          <a class="tb-normal-effect" href="http://organiko.jwsuperthemes.com/product/green-onion/" tabindex="-1">
                                          <img src="images/item4-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item4-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">					</a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="197" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=197" data-quantity="1" data-product_id="197" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-197">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=197" rel="nofollow" data-product-id="197" data-product-type="simple" class="add_to_wishlist" tabindex="-1">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/green-onion/" tabindex="-1">Green Onion</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="197"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>72.00</span></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-194 product type-product status-publish has-post-thumbnail product_cat-juice  instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onsale">-33%</span></div>
                                          <a class="tb-normal-effect" href="http://organiko.jwsuperthemes.com/product/fresh-tea/" tabindex="-1">
                                          <img src="images/item3-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item3-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">					</a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="194" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=194" data-quantity="1" data-product_id="194" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-194">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=194" rel="nofollow" data-product-id="194" data-product-type="simple" class="add_to_wishlist" tabindex="-1">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/fresh-tea/" tabindex="-1">Fresh Tea</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="194"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>80.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>54.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="slick-slide" data-slick-index="5" aria-hidden="true" style="width: 300px;" tabindex="-1" role="option" aria-describedby="slick-slide25">
                              <div>
                                 <div class="products-widget post-192 product type-product status-publish has-post-thumbnail product_cat-juice last instock shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"></div>
                                          <a class="tb-normal-effect" href="http://organiko.jwsuperthemes.com/product/fresh-orange/" tabindex="-1">
                                          <img src="images/item2-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item2-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">					</a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="192" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=192" data-quantity="1" data-product_id="192" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-192">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=192" rel="nofollow" data-product-id="192" data-product-type="simple" class="add_to_wishlist" tabindex="-1">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/fresh-orange/" tabindex="-1">Fresh Orange</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="192"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>80.00</span></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-190 product type-product status-publish has-post-thumbnail product_cat-vegetables first instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onsale">-21%</span></div>
                                          <a class="tb-normal-effect" href="http://organiko.jwsuperthemes.com/product/fresh-pomegranate-2/" tabindex="-1">
                                          <img src="images/6002-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/6002-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">					</a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="190" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=190" data-quantity="1" data-product_id="190" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-190">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=190" rel="nofollow" data-product-id="190" data-product-type="simple" class="add_to_wishlist" tabindex="-1">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/fresh-pomegranate-2/" tabindex="-1">Fresh Pomegranate</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="190"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>120.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>95.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="slick-slide slick-cloned" data-slick-index="6" aria-hidden="true" style="width: 300px;" tabindex="-1">
                              <div>
                                 <div class="products-widget post-976 product type-product status-publish has-post-thumbnail product_cat-dried-products  instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onnew">New</span><span class="onsale">-5%</span></div>
                                          <a class="tb-normal-effect" href="http://organiko.jwsuperthemes.com/product/honey/" tabindex="-1">
                                          <img src="images/product3-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/product3-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">					</a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="976" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=976" data-quantity="1" data-product_id="976" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-976">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=976" rel="nofollow" data-product-id="976" data-product-type="simple" class="add_to_wishlist" tabindex="-1">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/honey/" tabindex="-1">Honey</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="976"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>84.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>80.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-974 product type-product status-publish has-post-thumbnail product_cat-dried-products product_tag-meat  instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onnew">New</span><span class="onsale">-29%</span></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/dried-strawberries/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="-1">
                                          <img src="images/product2-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/product2-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/product2-1-300x300.jpg" alt="Dried Strawberries">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="974" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=974" data-quantity="1" data-product_id="974" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-974">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=974" rel="nofollow" data-product-id="974" data-product-type="simple" class="add_to_wishlist" tabindex="-1">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/dried-strawberries/" tabindex="-1">Dried Strawberries</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="974"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>84.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>60.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="slick-slide slick-cloned" data-slick-index="7" aria-hidden="true" style="width: 300px;" tabindex="-1">
                              <div>
                                 <div class="products-widget post-207 product type-product status-publish has-post-thumbnail product_cat-vegetables last instock sale shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"><span class="onnew">New</span><span class="onsale">-4%</span></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/bell-pepper/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="-1">
                                          <img src="images/item8-1-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item8-1-1-300x300.jpg" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/item7-1-300x300.jpg" alt="Bell Pepper">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="207" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=207" data-quantity="1" data-product_id="207" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-207">
                                                   <div class="yith-wcwl-add-button show" style="display:block">
                                                      <a href="http://organiko.jwsuperthemes.com/?add_to_wishlist=207" rel="nofollow" data-product-id="207" data-product-type="simple" class="add_to_wishlist" tabindex="-1">
                                                      Add to Wishlist</a>
                                                      <img src="images/wpspin_light.gif" class="ajax-loading" alt="loading" style="visibility:hidden" width="16" height="16">
                                                   </div>
                                                   <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
                                                      <span class="feedback">Product added!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
                                                      <span class="feedback">The product is already in the wishlist!</span>
                                                      <a href="http://organiko.jwsuperthemes.com/wishlist/" tabindex="-1">
                                                      Browse Wishlist	        </a>
                                                   </div>
                                                   <div style="clear:both"></div>
                                                   <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/bell-pepper/" tabindex="-1">Bell Pepper</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="207"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>146.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>140.00</span></ins></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div class="products-widget post-349 product type-product status-publish has-post-thumbnail product_cat-vegetables product_tag-product first instock shipping-taxable purchasable product-type-simple" style="width: 100%; display: inline-block;">
                                    <div class="tb-product-item">
                                       <div class="tb-image">
                                          <div class="tb-box-flash"></div>
                                          <a href="http://organiko.jwsuperthemes.com/product/bell-pepper-copy/" class="tb-thumb-effect" data-tb-thumb="true" tabindex="-1">
                                          <img src="images/item2-1-300x300.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="images/item2-1-300x300.png" sizes="(max-width: 300px) 100vw, 300px" width="300" height="300">						<span>
                                          <img src="images/product2-300x300.jpg" alt="Bell Pepper">
                                          </span>
                                          </a>
                                          <div class="tb-action">
                                             <div class="tb-product-btn tb-btn-quickview">
                                                <a href="#" class="button yith-wcqv-button" data-product_id="349" tabindex="-1"><i class="fa fa-compress" aria-hidden="true"></i></a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-tocart">
                                                <a rel="nofollow" href="http://organiko.jwsuperthemes.com/?add-to-cart=349" data-quantity="1" data-product_id="349" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart btn-add-to-cart" tabindex="-1">Add to cart</a>					
                                             </div>
                                             <div class="tb-product-btn tb-btn-wishlist">
                                                
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="tb-content">
                                          <div class="tb-title text-ellipsis">
                                             <a href="http://organiko.jwsuperthemes.com/product/bell-pepper-copy/" tabindex="-1">Bell Pepper</a>
                                          </div>
                                          <div class="tb-price-rating">
                                             <span class="tb-product-price"><span class="woocs_price_code" data-product-id="349"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>150.00</span></span></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           
                        </div>
                     </div>
                    
                  </div>
               </div>
               <div style="display: none" class="tb-view-more-link">
                  <a data-column="4" data-row="2" data-margin="30" data-args="{&quot;post_type&quot;:&quot;product&quot;,&quot;post_status&quot;:&quot;publish&quot;,&quot;ignore_sticky_posts&quot;:1,&quot;posts_per_page&quot;:&quot;12&quot;,&quot;paged&quot;:1,&quot;no_found_rows&quot;:1,&quot;order&quot;:&quot;none&quot;,&quot;meta_query&quot;:[],&quot;post_parent&quot;:0,&quot;tax_query&quot;:[{&quot;taxonomy&quot;:&quot;product_cat&quot;,&quot;terms&quot;:[&quot;dried-products&quot;,&quot;fruits&quot;,&quot;juice&quot;,&quot;vegetables&quot;],&quot;field&quot;:&quot;slug&quot;,&quot;operator&quot;:&quot;IN&quot;}],&quot;orderby&quot;:&quot;date&quot;}" data-atts="{&quot;number&quot;:&quot;12&quot;,&quot;rows&quot;:&quot;2&quot;,&quot;show_sale_flash&quot;:&quot;1&quot;,&quot;show_title&quot;:&quot;1&quot;,&quot;show_cat&quot;:&quot;1&quot;,&quot;show_price&quot;:&quot;1&quot;,&quot;show_rating&quot;:&quot;&quot;,&quot;show_add_to_cart&quot;:&quot;1&quot;,&quot;show_quick_view&quot;:&quot;1&quot;,&quot;show_whishlist&quot;:&quot;1&quot;,&quot;show_compare&quot;:&quot;&quot;,&quot;product_cat&quot;:&quot;dried-products,fruits,juice,vegetables&quot;,&quot;key&quot;:&quot;15362163375b90cd11554f8&quot;}" href="#"><span><i class="fa fa-plus"></i></span>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>



<!-- we focus -->
<div class="we-focus">
	<div class="container">
		<div class="focus-grids">
			<div class="col-md-6 focus-grid">
				<h3>KEY FEATURES <span>OF OUR PROCESS</span></h3>
				<div class="strip"></div>
				<p>Inclusive growth of nature, farmers and users through sustainable organic farming.</p>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image a"></div>
						<h4>Sustainable Management</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image b"></div>
						<h4>Biodiversity Conservation</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image c"></div>
						<h4>Organic farming with least use of chemicals</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image d"></div>
						<h4>Natural Resources Conservation & Development</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image e"></div>
						<h4>Imparting better returns to Farmers</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image f"></div>
						<h4>Eliminating pricing surge</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image g"></div>
						<h4>Knowledge transfer and implementation of best practices</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image h"></div>
						<h4>Integrated Tourism Growth</h4>
					</div>
				</div>
			</div>
			<div class="col-md-3 focus-grid">
				<div class="focus-border">
					<div class="focus-layout">
						<div class="focus-image i"></div>
						<h4>Projecting farming as the best Employment</h4>
					</div>
				</div>
			</div>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //we focus -->
<!-- feature -->
<div class="feature" >
	<div class="container">
		<div class="col-md-6 feature-grid">
			<?= Html::img('@web/images/15.png')?>
		</div>
		<div class="col-md-6 feature-grid">
			<h3>DOWNLOAD <span>OUR APP</span></h3>
			<div class="strip"></div>
			<p>Download our app and login to start contributing to nature while availing better food leading to a healthier life.  </p>
			<p>Currently you can only download our app directly from our site and not through the appstore. We shall soon be available on the Android and IOS appstores. For now please scan the QR code to download the app.
				<br />
				<br />
				<strong> Set free the farmer in you. </strong>
			</p>
			<a href="#" class="hvr-rectangle-out button">READ MORE</a>
			<a href="#" class="hvr-rectangle-in button red">DOWNLOAD</a>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //feature -->
<!-- recent blog -->
<div class="feature1">
	
		<div class="container ">
		
		<h3 class="feature1d"> FAQ </h3>
    <div class="panel-group" id="faqAccordion">
        <div class="panel panel-default ">
            <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
                 <h4 class="panel-title">
                    <a href="#" class="ing">Q: What is the cost to plant a tree? Is it free?</a>
              </h4>

            </div>
            <div id="question0" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                     <h5><span class="label label-primary">Answer</span></h5>

                    <p>NO, Planting a tree is not free. Diferent trees have a different cost depending on the price of the sapling and the maintenance they need. We have two different cost for the tree. One is the initial planting cost and the second is the monthly maintenance cost for the tree.
                        </p>
                </div>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                 <h4 class="panel-title">
                    <a href="#" class="ing">Q: Why are you charging a monthly cost for the tree?</a>
              </h4>

            </div>
            <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                     <h5><span class="label label-primary">Answer</span></h5>

                    <p>We know there are a lot of NGO's engaged in planting trees. We would like to clarify that  WE ARE NOT ANY NGO and we do not operate out of any government grants or funds. Infact we believe that it is not just the responsibility of the government to plant tree; but every citizen should contribute for it. So we need funds to plant the tree and also maintain the trees till a certain age. We also do not plant the trees on govt areas but take large chunks of land on lease from their owners for long periods of time. So we have the monthly recursive cost to take care of these expences.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
                 <h4 class="panel-title">
                    <a href="#" class="ing">Q: Is it your own land where you plant the tree? Do you plant on Govt lands?</a>
              </h4>

            </div>
            <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                     <h5><span class="label label-primary">Answer</span></h5>

                    <p>We do not plant on govt areas. We take large patchs of lands on lease for long periods of time and plant trees on them. Because we have a written agreement with the owner it ensures that the tree is not cut. We provide the GPS location of the trees to their respective users who planted them so they can stay in touch with them.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
                 <h4 class="panel-title">
                    <a href="#" class="ing">Q: How can I ensure you are not planting the trees for timber and they are not cut for the wood?</a>
              </h4>

            </div>
            <div id="question3" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                     <h5><span class="label label-primary">Answer</span></h5>

                    <p>Go through the list of trees we provide for plantation. The trees in the list do not have much commercial value with Timber. We already mentioned that the trees we plant are not on Govt lands. We take large chunks of land on lease for a minimum of 20 years time. This ensures the tree doesnt get cut for atleast 20 years. However in the list you will also find fruit bearing plants like mango and jackfruit. The trees have been carefully selected to ensure they dont draw too much water from the ground or cause sudden depletion of nutrients. We do intend to sell the fruits from the trees and use them for purchasing/leasing out more areas and using them for plantation.</p>
                </div>
            </div>
			
        </div>
		
		<div class="panel panel-default ">
            <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question4">
                 <h4 class="panel-title">
                    <a href="#" class="ing">Q: What if the plant I paid for dies for some reason? What if there is a flood or a cyclone?</a>
              </h4>

            </div>
            <div id="question4" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                     <h5><span class="label label-primary">Answer</span></h5>

                    <p>We have trained manpower to take care of the trees. However in case the tree dies for some reason, we immediately replace it with a new one (with same specifications) at no additional charge to the user. The user is informed about it.(We ensure the health of the tree and growth is always updated the user and the whold process is kept transparent.) However in case of a natural disaster like a flood or a super cyclone, we are afraid we might not be economically sound enough to raplace all the broken/damaged trees. In such a case we would count on our users to understand the situation and plant new trees.</p>
                </div>
            </div>
			
        </div>
        
    </div>
    <!--/panel-group-->
</div>	
	
</div>
<!-- //recent blog -->
<!-- footer -->

<div class="plant" style="display:none;"> 
<div class="">
            <div id="myCarousel" class="carousel slide">
                
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row">
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <!--/item-->
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <!--/item-->
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-3"><a href="#x"> <img src="images/tr.png" class="img-responsive"></a>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <!--/item-->
                </div>
                <!--/carousel-inner--> 
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>

                <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
            </div>
            <!--/myCarousel-->
        </div>
</div>
