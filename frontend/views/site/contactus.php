<style> 
.innertop{ width:100%; float:left;}
.fixed{-webkit-box-shadow: 0px 2px 2px 0px rgba(50, 50, 50, 0.25);
    -moz-box-shadow: 0px 2px 2px 0px rgba(50, 50, 50, 0.25);
    box-shadow: 0px 2px 2px 0px rgba(50, 50, 50, 0.25);}


.innerbanner{ width:100%; float:left; }

.innerbanner img {
    width: 100%;
}
.inner-content h3 {
    margin: 0;
    color: #3d3d3d;
    font-size: 46px;
    font-family: 'Dosis', sans-serif;
}
.inner-content{ width:100%; float:left; padding-top:50px; padding-bottom:50px;}
.col-md-4.trs img {
    width: 100%; text-align:left;
}
button#cfsubmit {
    background: #000;
    color: #fff;
}


</style>

<div class="innerbanner"> <img src="/images/contact.jpg"> </div>
<div  class="inner-content">

<div class="container">
            <div class="row">
              <div class="col-sm-6">
                <h4 class="font-alt">Get in touch</h4><br/>
                <form id="contactForm" role="form" method="post" action="php/contact.php">
                  <div class="form-group">
                    <label class="sr-only" for="name">Name</label>
                    <input class="form-control" type="text" id="name" name="name" placeholder="Your Name*" required="required" data-validation-required-message="Please enter your name."/>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="email">Email</label>
                    <input class="form-control" type="email" id="email" name="email" placeholder="Your Email*" required="required" data-validation-required-message="Please enter your email address."/>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" rows="7" id="message" name="message" placeholder="Your Message*" required="required" data-validation-required-message="Please enter your message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="text-center">
                    <button class="btn btn-block btn-round btn-d" id="cfsubmit" type="submit">Submit</button>
                  </div>
                </form>
                <div class="ajax-response font-alt" id="contactFormResponse"></div>
              </div>
              <div class="col-sm-6">
                <h4 class="font-alt">Ecovilla</h4><br/>
                
                
                <ul class="kd-userinfo s">
          <li> <i class="fa fa-map-marker"></i>
            <p> 3637 Chandrasekharpur,
District Center, Besides Tanishq Showroom,<br />
&nbsp; &nbsp; Bhubaneswar, 751021
</p>
          </li>
          <li> <i class="fa fa-phone"></i>
            <p>+91 8599092666</p>
          </li>
          <li> <i class="fa fa-envelope-o"></i>
            <p>ecovilla@gmail.com</p>
          </li>
          <li> <i class="fa fa-home"></i>
            <p>www.ecovilla.in</p>
          </li>
        </ul>
              </div>
            </div>
          </div>
		  
		 
		</div>  
		  
	<div style="width:100%;"> <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3215.4326297548364!2d85.86333697934623!3d20.305276604540577!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a190a15ccf8a047%3A0x4e05d789d3c39dc6!2sPalasuni%2C+Rasulgarh%2C+Bhubaneswar%2C+Odisha!5e0!3m2!1sen!2sin!4v1537792128344" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>	  
		  
		  