<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
		'http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800',
		'https://fonts.googleapis.com/css?family=Josefin+Sans:400,600|Questrial',
        //'css/animate.css',
        //'css/font-awesome.min.css',
        'css/bootstrap.css',
        'css/style.css',
        'css/style_002.css',
    ];
    public $js = [
		'js/jquery-1.11.1.min.js',
		'js/bootstrap.min.js',
		'js/move-top.js',
		'js/easing.js',
		'js/responsiveslides.min.js',
		'js/custom.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
