<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[CustomerMessage]].
 *
 * @see CustomerMessage
 */
class CustomerMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CustomerMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CustomerMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
