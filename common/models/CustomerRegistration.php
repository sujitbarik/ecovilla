<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CustomerRegistration".
 *
 * @property int $Id
 * @property string $Name
 * @property string $EmailId
 * @property int $ContactNo
 * @property string $Password
 * @property int $Isdelete
 * @property string $Ondate
 */
class CustomerRegistration extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CustomerRegistration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Name', 'EmailId', 'ContactNo', 'Password'], 'required'],
            [['ContactNo', 'Isdelete','status'], 'integer'],
            [['Password'], 'string'],
            [['Ondate'], 'safe'],
            [['Name', 'EmailId'], 'string', 'max' => 200],
            [['uniquecode'],'string','max'=>150]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Name',
            'EmailId' => 'Email ID',
            'ContactNo' => 'Contact No',
            'Password' => 'Password',
            'Isdelete' => 'Isdelete',
            'Ondate' => 'Ondate',
        ];
    }
    public function getCustomername($id){
        $customerdetails = CustomerRegistration::find()->where(['Id'=>$id])->one();
        if(count($customerdetails)){
            $name = $customerdetails['Name'];
        }
        else{
            $name = '';
        }
        return $name;
    }
	public function getQrcode($qrcodedet){
		
		$msg = urlencode($qrcodedet);
		$send_array = array();
		$qrcodesize = "150x150";
		$data = $msg;
		$qrcodeUrl = "https://api.qrserver.com/v1/create-qr-code/?size=".$qrcodesize."&data=".$data;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL,$qrcodeUrl);
		curl_setopt($curl, CURLOPT_TIMEOUT, 500);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $send_array);
		$result = curl_exec($curl);
		
		curl_close($curl);
		
		return $qrcodeUrl;
		
	}
}
