<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "TreeDetails".
 *
 * @property int $ID
 * @property string $TreeName
 * @property string $TreeDescription
 * @property double $PlantingCost
 * @property double $MonthlyRecuringCost
 * @property string $AddedDate
 * @property string $UpdateDate
 * @property int $Isdelete
 */
class TreeDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TreeDetails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TreeName', 'TreeDescription', 'PlantingCost', 'MonthlyRecuringCost','OxyPoint','Stock','MaturityTime'], 'required'],
            [['TreeDescription','MaturityTime','TreeImage'], 'string'],
            [['PlantingCost', 'MonthlyRecuringCost','OxyPoint'], 'number'],
            [['AddedDate', 'UpdateDate'], 'safe'],
            [['Isdelete','Stock'], 'integer'],
            [['TreeName'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TreeName' => 'Tree Name',
            'TreeDescription' => 'Tree Description',
            'PlantingCost' => 'Planting Cost (Per Unit)',
            'MonthlyRecuringCost' => 'Monthly Recuring Cost (Per Unit)',
            'OxyPoint'=>'OxyPoint',
            'Stock'=>'Stock',
            'MaturityTime'=>'MaturityTime (Year)',
            'AddedDate' => 'Date Of Added',
            'UpdateDate' => 'Update Date',
            'Isdelete' => 'Isdelete',
        ];
    }
    
    public function getTreeimage()
    {
        return $this->hasOne(TreesImage::className(), ['TDID' => 'ID'])->orderBy(['ID'=>SORT_DESC]);
    }
}
