<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "FarmImage".
 *
 * @property int $FarmImageID
 * @property int $FarmID
 * @property int $ImageID
 * @property int $IsDelete
 * @property string $OnDate
 * @property string $UpdatedDate
 *
 * @property Farm $farm
 * @property Image $image
 */
class FarmImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'FarmImage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FarmID','OnDate'], 'required'],
            [['FarmID','IsDelete'], 'integer'],
            [['OnDate', 'UpdatedDate'], 'safe'],
            [['FarmID'], 'exist', 'skipOnError' => true, 'targetClass' => Farm::className(), 'targetAttribute' => ['FarmID' => 'FarmID']],
            [['ImageID'], 'exist', 'skipOnError' => true, 'targetClass' => Image::className(), 'targetAttribute' => ['ImageID' => 'ImageID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'FarmImageID' => 'Farm Image ID',
            'FarmID' => 'Farm ID',
            'ImageID' => 'Image ID',
            'IsDelete' => 'Is Delete',
            'OnDate' => 'On Date',
            'UpdatedDate' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFarm()
    {
        return $this->hasOne(Farm::className(), ['FarmID' => 'FarmID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['ImageID' => 'ImageID']);
    }
}
