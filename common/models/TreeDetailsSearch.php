<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TreeDetails;

/**
 * TreeDetailsSearch represents the model behind the search form of `common\models\TreeDetails`.
 */
class TreeDetailsSearch extends TreeDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'Isdelete'], 'integer'],
            [['TreeName', 'TreeDescription', 'AddedDate', 'UpdateDate'], 'safe'],
            [['PlantingCost', 'MonthlyRecuringCost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TreeDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'PlantingCost' => $this->PlantingCost,
            'MonthlyRecuringCost' => $this->MonthlyRecuringCost,
            'AddedDate' => $this->AddedDate,
            'UpdateDate' => $this->UpdateDate,
            'Isdelete' => 0,
        ]);

        $query->andFilterWhere(['like', 'TreeName', $this->TreeName])
            ->andFilterWhere(['like', 'TreeDescription', $this->TreeDescription]);

        return $dataProvider;
    }
}
