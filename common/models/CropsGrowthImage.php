<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CropsGrowthImage".
 *
 * @property int $ID
 * @property int $RentId
 * @property string $Image
 * @property string $Ondate
 * @property int $IsDelete
 */
class CropsGrowthImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CropsGrowthImage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['RentId'], 'required'],
            [['ID', 'RentId', 'IsDelete'], 'integer'],
            [['Image'], 'string'],
            [['Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RentId' => 'Rent ID',
            'Image' => 'Image',
            'Ondate' => 'Ondate',
            'IsDelete' => 'Is Delete',
        ];
    }
	public function imageUpload($image,$type)
    {
  
	    $im=explode(".", $image->name);
        $ext = $im[1];
        if(strtolower($ext)=='jpg' || strtolower($ext)=='jpeg' || strtolower($ext)=='png' || strtolower($ext)=='gif')
        {
            $basepath=str_replace('frontend','backend',Yii::$app->basePath);
            Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
			$obj_model = new CropsGrowthImage();
            $obj_model->Image = 'imageupload/'. $ppp.".{$ext}";
            $obj_model->RentId=$type;
            $obj_model->StatusType='Image';
            $obj_model->AddedDate = date("Y-m-d");
            $image->saveAs($path);
            $obj_model->save();

        }
    }
    public function imageUpload1($image,$type)
    {
  
	    $im=explode(".", $image->name);
        $ext = $im[1];
        if(strtolower($ext)=='jpg' || strtolower($ext)=='jpeg' || strtolower($ext)=='png' || strtolower($ext)=='gif')
        {
            $basepath=str_replace('frontend','backend',Yii::$app->basePath);
            Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
			$obj_model = new CropsGrowthImage();
            $obj_model->Image = 'imageupload/'. $ppp.".{$ext}";
            $obj_model->TreePlantID=$type;
            $obj_model->StatusType='Image';
            $obj_model->AddedDate = date("Y-m-d");
            $image->saveAs($path);
            $obj_model->save();

        }
    }
    public function noofUpdates($rentid){
        $rentdetails = CropsGrowthImage::find()->where(['RentId'=>$rentid])->all();
        $noofcount = count($rentdetails);
        return $noofcount;
    }
	public function noofUpdatess($rentid){
        $rentdetails = CropsGrowthImage::find()->where(['TreePlantID'=>$rentid])->all();
        $noofcount = count($rentdetails);
        return $noofcount;
    }
}
