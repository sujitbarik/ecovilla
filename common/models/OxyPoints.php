<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "OxyPoints".
 *
 * @property int $ID
 * @property int $TreePlantID
 * @property double $OxyPoints
 * @property string $AddedDate
 * @property string $UpdateDate
 */
class OxyPoints extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OxyPoints';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TreePlantID', 'OxyPoints'], 'required'],
            [['TreePlantID'], 'integer'],
            [['OxyPoints'], 'number'],
            [['AddedDate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TreePlantID' => Yii::t('app', 'Tree Plant ID'),
            'OxyPoints' => Yii::t('app', 'Oxy Points'),
            'AddedDate' => Yii::t('app', 'Added Date'),
            'UpdateDate' => Yii::t('app', 'Update Date'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return OxyPointsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OxyPointsQuery(get_called_class());
    }
}
