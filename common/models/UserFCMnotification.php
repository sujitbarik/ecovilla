<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "UserFCMnotification".
 *
 * @property int $ID
 * @property int $CustomerID
 * @property string $DeviceID
 * @property string $Ondate
 * @property int $IsDelete
 */
class UserFCMnotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'UserFCMnotification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CustomerID', 'DeviceID'], 'required'],
            [['CustomerID', 'IsDelete'], 'integer'],
            [['DeviceID'], 'string'],
            [['Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'DeviceID' => Yii::t('app', 'Device ID'),
            'Ondate' => Yii::t('app', 'Ondate'),
            'IsDelete' => Yii::t('app', 'Is Delete'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return UserFCMnotificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserFCMnotificationQuery(get_called_class());
    }
    
    
    public function sendnotification($displaydata,$descp,$token,$type)
    {
        ini_set('display_errors',1);
        $ch = curl_init("https://fcm.googleapis.com/fcm/send");
        
        //The device token.
        $token = $token;
        
        //Title of the Notification.
		if($type == "Tree"){
            $title='New growth update receive from Tree';
			$type = "Tree";
		}else{
            $title='New growth update receive from Farm';
			$type = "Farm";
        }
        //Body of the Notification.
        $body = strip_tags($descp);
        
        $data=array('id'=>1234,'type'=>$type,'title' =>$title,'text' =>$displaydata ,'descp' =>$body,'image'=>'http://188.166.247.24/biggies/frontend/web/images/logo.png','sound'=>'neworder.wav','AnotherActivity'=>'True');
        //This array contains, the token and the notification. The 'to' attribute stores the token.
        $arrayToSend = array('to' => $token, 'data' => $data);
        
        //Generating JSON encoded string form the above array.
        $json = json_encode($arrayToSend);
        
        //Setup headers:
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization:key=AAAAYXxRHMk:APA91bGBlQRApvU3oeKtk6Gp5PAFslli_DiMGMwp7z3GD348UReEiXbSSoyq8UmPuqhXSd6n6HTR3dfiV36hEVtt2LluGb3EMGYdx4WBlhScdscApgKJQPpjEiCLqYp7PpD658cBeQMa';
        
        //Setup curl, add headers and post parameters.
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);       
        
        //Send the request
        $output=curl_exec($ch);
        
        //Close request
        curl_close($ch);
       return $output;
    }
}
