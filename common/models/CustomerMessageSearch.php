<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CustomerMessage;

/**
 * CustomerMessageSearch represents the model behind the search form of `common\models\CustomerMessage`.
 */
class CustomerMessageSearch extends CustomerMessage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'Type'], 'integer'],
            [['Details', 'AddedDate', 'Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerMessage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Type' => $this->Type,
            'AddedDate' => $this->AddedDate,
            'Ondate' => $this->Ondate,
        ]);

        $query->andFilterWhere(['like', 'Details', $this->Details]);

        return $dataProvider;
    }
}
