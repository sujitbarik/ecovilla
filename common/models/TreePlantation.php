<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "TreePlantation".
 *
 * @property int $ID
 * @property int $TPID TreePackage ID
 * @property int $CustomerID CustomerRegistration ID
 * @property int $PlantingStatus 1->Success
 * @property string $PlantingDate
 * @property string $Ondate
 * @property int $Isdelete
 */
class TreePlantation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TreePlantation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TPID', 'CustomerID'], 'required'],
            [['TPID', 'CustomerID', 'PlantingStatus', 'Isdelete'], 'integer'],
            [['PlantingDate', 'Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TPID' => 'Package ID',
            'CustomerID' => 'Customer ID',
            'PlantingStatus' => 'Planting Status',
            'PlantingDate' => 'Planting Date',
            'Ondate' => 'Ondate',
            'Isdelete' => 'Isdelete',
        ];
    }
    
    public function getTreepckg()
    {
        return $this->hasOne(TreePackage::className(), ['PKGID' => 'TPID']);
    }
}
