<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "MoneyRecieveDetail".
 *
 * @property int $RecieveId
 * @property string $BankDetail
 * @property string $TezDetail
 * @property string $OnDate
 * @property string $UpdatedDate
 */
class MoneyRecieveDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'MoneyRecieveDetail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BankDetail', 'TezDetail', 'OnDate'], 'required'],
            [['BankDetail'], 'string'],
            [['OnDate', 'UpdatedDate'], 'safe'],
            [['TezDetail'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RecieveId' => Yii::t('app', 'Recieve ID'),
            'BankDetail' => Yii::t('app', 'Bank Detail'),
            'TezDetail' => Yii::t('app', 'Tez Detail'),
            'OnDate' => Yii::t('app', 'On Date'),
            'UpdatedDate' => Yii::t('app', 'Updated Date'),
        ];
    }
}
