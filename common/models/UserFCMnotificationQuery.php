<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[UserFCMnotification]].
 *
 * @see UserFCMnotification
 */
class UserFCMnotificationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserFCMnotification[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserFCMnotification|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
