<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CustomerMessage".
 *
 * @property int $ID
 * @property int $Type 0->tree,1->crop
 * @property string $Details
 * @property string $AddedDate
 * @property string $Ondate
 */
class CustomerMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CustomerMessage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Type', 'Details'], 'required'],
            [['Type'], 'integer'],
            [['Details'], 'string'],
            [['AddedDate', 'Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Type' => Yii::t('app', 'Message Type'),
            'Details' => Yii::t('app', 'Details'),
            'AddedDate' => Yii::t('app', 'Added Date'),
            'Ondate' => Yii::t('app', 'Ondate'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CustomerMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerMessageQuery(get_called_class());
    }
}
