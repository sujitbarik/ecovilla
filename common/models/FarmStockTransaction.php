<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "FarmStockTransaction".
 *
 * @property int $Id
 * @property int $FarmID
 * @property int $StockQTY
 * @property int $Type 0->StockAdd,1>StockUpdate,2->FarmRent
 * @property string $OnDate
 * @property string $UpdateDate
 * @property int $IsDelete
 */
class FarmStockTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'FarmStockTransaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FarmID', 'StockQTY', 'Type'], 'required'],
            [['FarmID', 'StockQTY', 'Type', 'IsDelete'], 'integer'],
            [['OnDate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => Yii::t('app', 'ID'),
            'FarmID' => Yii::t('app', 'Farm ID'),
            'StockQTY' => Yii::t('app', 'Stock Qty'),
            'Type' => Yii::t('app', '0->StockAdd,1>StockUpdate,2->FarmRent'),
            'OnDate' => Yii::t('app', 'On Date'),
            'UpdateDate' => Yii::t('app', 'Update Date'),
            'IsDelete' => Yii::t('app', 'Is Delete'),
        ];
    }
}
