<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[OxyPoints]].
 *
 * @see OxyPoints
 */
class OxyPointsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return OxyPoints[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return OxyPoints|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
