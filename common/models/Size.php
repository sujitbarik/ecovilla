<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Size".
 *
 * @property int $SizeID
 * @property string $Size
 * @property int $IsDelete
 * @property string $OnDate
 * @property string $UpdatedDate
 *
 * @property Farm[] $farms
 */
class Size extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Size';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Size','OnDate'], 'required'],
            [['IsDelete'], 'integer'],
            [['OnDate', 'UpdatedDate'], 'safe'],
            [['Size'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SizeID' => 'Size ID',
            'Size' => 'Size',
            'IsDelete' => 'Is Delete',
            'OnDate' => 'On Date',
            'UpdatedDate' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFarms()
    {
        return $this->hasMany(Farm::className(), ['Size' => 'SizeID']);
    }
	
	public function getSize($sizeid){
		$sizedetails = Size::find()->where(['SizeID'=>$sizeid])->one();
		if(count($sizedetails)){
			$name = $sizedetails['Size'];
		}
		else{
			$name = '';
		}
		
		return $name;
	}
}
