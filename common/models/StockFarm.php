<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "StockFarm".
 *
 * @property int $StockId
 * @property int $FarmID
 * @property int $SizeID
 * @property int $Status
 * @property string $Ondate
 * @property int $IsDelete
 */
class StockFarm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'StockFarm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FarmID', 'SizeID'], 'required'],
            [['FarmID', 'SizeID', 'Status', 'IsDelete'], 'integer'],
            [['Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'StockId' => 'Stock ID',
            'FarmID' => 'Farm ID',
            'SizeID' => 'Size ID',
            'Status' => 'Status',
            'Ondate' => 'Ondate',
            'IsDelete' => 'Is Delete',
        ];
    }
}
