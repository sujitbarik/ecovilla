<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Transaction".
 *
 * @property int $Id
 * @property int $CustomerID
 * @property double $OpeningBalance
 * @property double $ClosingBalance
 * @property string $Details
 * @property string $Ondate
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CustomerID', 'OpeningBalance', 'ClosingBalance', 'Details'], 'required'],
            [['CustomerID'], 'integer'],
            [['OpeningBalance', 'ClosingBalance'], 'number'],
            [['Details'], 'string'],
            [['Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'CustomerID' => 'Customer ID',
            'OpeningBalance' => 'Opening Balance',
            'ClosingBalance' => 'Closing Balance',
            'Details' => 'Details',
            'Ondate' => 'Ondate',
        ];
    }
}
