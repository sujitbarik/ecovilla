<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TreePosition]].
 *
 * @see TreePosition
 */
class TreePositionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TreePosition[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TreePosition|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
