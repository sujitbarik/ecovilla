<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "TreesImage".
 *
 * @property int $ID
 * @property int $TDID
 * @property string $Image
 * @property string $Ondate
 * @property int $Isdelete
 */
class TreesImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TreesImage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TDID', 'Image'], 'required'],
            [['TDID', 'Isdelete'], 'integer'],
            [['Image'], 'string'],
            [['Ondate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TDID' => 'Tdid',
            'Image' => 'Image',
            'Ondate' => 'Ondate',
            'Isdelete' => 'Isdelete',
        ];
    }
	
	public function imageUpload($image,$type)
    {
  
	    $im=explode(".", $image->name);
        $ext = $im[1];
        if(strtolower($ext)=='jpg' || strtolower($ext)=='jpeg' || strtolower($ext)=='png' || strtolower($ext)=='gif')
        {
            $basepath=str_replace('frontend','backend',Yii::$app->basePath);
            Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
			$obj_model = new TreesImage();
            $obj_model->Image = 'imageupload/'. $ppp.".{$ext}";
            $obj_model->TDID = $type;
            $image->saveAs($path);
            $obj_model->save();

        }
    }
    
    public function imageUploads($image)
    {
	    $im=explode(".", $image->name);
        $ext = $im[1];
        if(strtolower($ext)=='jpg' || strtolower($ext)=='jpeg' || strtolower($ext)=='png' || strtolower($ext)=='gif')
        {
            $basepath=str_replace('frontend','backend',Yii::$app->basePath);
            Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $img='imageupload/'. $ppp.".{$ext}";
            $image->saveAs($path);
            return $img;
        }
    }
}
