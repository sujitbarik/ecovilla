<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CustomerRent".
 *
 * @property int $RentId
 * @property int $CustomerId
 * @property int $FarmId
 * @property int $CropId
 * @property int $CropStatus
 * @property string $BookingDate
 * @property string $OnDate
 * @property int $IsDelete
 * @property string $RentPoints
 *
 * @property Crops $crop
 * @property CustomerRegistration $customer
 * @property Farm $farm
 */
class CustomerRent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CustomerRent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['CustomerId', 'FarmId', 'CropId', 'CropStatus', 'BookingDate', 'RentPoints'], 'required'],
            [['CustomerId', 'FarmId', 'CropId', 'CropStatus', 'IsDelete'], 'integer'],
            [['BookingDate', 'OnDate'], 'safe'],
            [['RentPoints'], 'string', 'max' => 100],
            //[['CropId'], 'exist', 'skipOnError' => true, 'targetClass' => Crops::className(), 'targetAttribute' => ['CropId' => 'CropID']],
            //[['CustomerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerRegistration::className(), 'targetAttribute' => ['CustomerId' => 'Id']],
           // [['FarmId'], 'exist', 'skipOnError' => true, 'targetClass' => Farm::className(), 'targetAttribute' => ['FarmId' => 'FarmID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RentId' => 'Rent ID',
            'CustomerId' => 'Customer ID',
            'FarmId' => 'Farm ID',
            'CropId' => 'Crop ID',
            'CropStatus' => 'Crop Status',
            'BookingDate' => 'Booking Date',
            'OnDate' => 'On Date',
            'IsDelete' => 'Is Delete',
            'RentPoints' => 'Rent Points',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
/*
    public function getCrop()
    {
        return $this->hasOne(Crops::className(), ['CropID' => 'CropId']);
    }
*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerRegistration::className(), ['Id' => 'CustomerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFarm()
    {
        return $this->hasOne(Farm::className(), ['FarmID' => 'FarmId']);
    }
    
    public function getCropsize()
    {
        return $this->hasOne(CropSizePrice::className(), ['Id' => 'CropId']);
    }
    
}
