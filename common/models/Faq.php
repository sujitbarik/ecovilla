<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Faq".
 *
 * @property int $FaqId
 * @property string $Question
 * @property string $Answer
 * @property int $Status
 * @property int $IsDelete
 * @property string $OnDate
 * @property string $UpdatedDate
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Question', 'Answer'], 'required'],
            [['Question', 'Answer'], 'string'],
            [['Status', 'IsDelete'], 'integer'],
            [['OnDate', 'UpdatedDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'FaqId' => Yii::t('app', 'Faq ID'),
            'Question' => Yii::t('app', 'Question'),
            'Answer' => Yii::t('app', 'Answer'),
            'Status' => Yii::t('app', 'Status'),
            'IsDelete' => Yii::t('app', 'Is Delete'),
            'OnDate' => Yii::t('app', 'On Date'),
            'UpdatedDate' => Yii::t('app', 'Updated Date'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return FaqQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FaqQuery(get_called_class());
    }
}
