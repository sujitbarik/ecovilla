<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CropSizePrice".
 *
 * @property int $Id
 * @property int $CropID
 * @property int $SizeID
 * @property double $PlantingPrice
 * @property string $Ondate
 * @property string $UpdateDate
 * @property int $IsDelete
 */
class CropSizePrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CropSizePrice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CropID', 'SizeID', 'PlantingPrice'], 'required'],
            [['CropID', 'SizeID', 'IsDelete'], 'integer'],
            [['PlantingPrice'], 'number'],
            [['Ondate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'CropID' => 'Crop ID',
            'SizeID' => 'Size ID',
            'PlantingPrice' => 'Planting Price',
            'Ondate' => 'Ondate',
            'UpdateDate' => 'Update Date',
            'IsDelete' => 'Is Delete',
        ];
    }
    
    
    public function getCrops()
    {
        return $this->hasOne(Crops::className(), ['CropID' => 'CropID']);
    }
    
    public function getSize()
    {
        return $this->hasOne(Size::className(), ['SizeID' => 'SizeID']);
    }
}
