<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Image".
 *
 * @property int $ImageID
 * @property string $Image
 * @property string $ImageUsedFor
 * @property int $IsDelete
 * @property string $OnDate
 * @property string $UpdatedDate
 *
 * @property Farm[] $farms
 * @property FarmImage[] $farmImages
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Image', 'ImageUsedFor','OnDate'], 'required'],
            [['IsDelete'], 'integer'],
            [['OnDate', 'UpdatedDate'], 'safe'],
            [['Image', 'ImageUsedFor'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ImageID' => 'Image ID',
            'Image' => 'Image',
            'ImageUsedFor' => 'Image Used For',
            'IsDelete' => 'Is Delete',
            'OnDate' => 'On Date',
            'UpdatedDate' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFarms()
    {
        return $this->hasMany(Farm::className(), ['SoilReport' => 'ImageID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFarmImages()
    {
        return $this->hasMany(FarmImage::className(), ['ImageID' => 'ImageID']);
    }
    
    public function imageUpload($image,$type)
    {
        // $image->name; 
	    $im=explode(".", $image->name);
        $ext = $im[1];
        if(strtolower($ext)=='jpg' || strtolower($ext)=='jpeg' || strtolower($ext)=='png' || strtolower($ext)=='gif')
        {
            // the path to save file, you can set an uploadPath
            // in Yii::$app->params (as used in example below)
            $basepath=str_replace('frontend','backend',Yii::$app->basePath);
            Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $this->Image = 'imageupload/'. $ppp.".{$ext}";
            $this->OnDate=date('Y-m-d H:i:s');
            $this->ImageUsedFor=$type;
            
            $image->saveAs($path);
            $x=$this->save();
            return $this->ImageID;
        }
    }
    
    public function pdfUpload($image,$type)
    {
        // $image->name; 
	    $im=explode(".", $image->name);
        $ext = $im[1];
        if(strtolower($ext)=='pdf')
        {
            // the path to save file, you can set an uploadPath
            // in Yii::$app->params (as used in example below)
            $basepath=str_replace('frontend','backend',Yii::$app->basePath);
            Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $this->Image = 'imageupload/'. $ppp.".{$ext}";
            $this->OnDate=date('Y-m-d H:i:s');
            $this->ImageUsedFor=$type;
            
            $image->saveAs($path);
            $x=$this->save();
            return $this->ImageID;
        }
    }

    public function cropimageUpload($image,$type)
    {
        $ext = end((explode(".", $image->name)));
        $basepath=str_replace('frontend','backend',Yii::$app->basePath);
        Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
        $ppp=Yii::$app->security->generateRandomString();
        $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
        $this->Image = 'imageupload/'. $ppp.".{$ext}";
        $this->FromName=$type;
        $this->AddedBy=Yii::$app->user->identity->login_id;
        $this->ModifiedBy=Yii::$app->user->identity->login_id;
        $this->OnDate=date('Y-m-d');
        
        $image->saveAs($path);
        $x=$this->save();
        return $this->ImageId;
    }
	
	public function getImagepath($imageid){
		if($imageid!=''){
			$imagedetails = Image::find()->where(['ImageID'=>$imageid])->one();
			if(count($imagedetails)){
				$imagepath = $imagedetails['Image'];
			}
			else{
				$imagepath = '';
			}
		}
		else{
			$imagepath = '';
		}
		return $imagepath;
	}
    
    
    public function imageUploads($image)
    {
      
	    $im=explode(".", $image->name);
        $ext = $im[1];
        if(strtolower($ext)=='jpg' || strtolower($ext)=='jpeg' || strtolower($ext)=='png' || strtolower($ext)=='gif')
        {
            $basepath=str_replace('frontend','backend',Yii::$app->basePath);
            Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';
            $ppp=Yii::$app->security->generateRandomString();
            $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
            $img='imageupload/'. $ppp.".{$ext}";
            $image->saveAs($path);
            return $img;
        }
    }
}
