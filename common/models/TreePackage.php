<?php

namespace common\models;

use Yii;
use common\models\TreeDetails;
use common\models\TreePackage;
use common\models\TreesImage;
use common\models\TreePlantation;

/**
 * This is the model class for table "TreePackage".
 *
 * @property int $PKGID
 * @property int $TDID
 * @property int $Duration Duration In Month
 * @property int $Quantity Tree Quantity
 * @property double $PlantingCost
 * @property double $MonthlyRecuringCost
 * @property string $CreateDate
 * @property string $UpdateDate
 * @property int $Isdelete
 */
class TreePackage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TreePackage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PackageName','TDID', 'Duration','PackageCost'], 'required'],
            [['TDID', 'Duration', 'Quantity', 'Isdelete'], 'integer'],
            [['PlantingCost', 'MonthlyRecuringCost'], 'number'],
            [['CreateDate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
			'PackageName'=>'Package Name',
            'PKGID' => 'Pkgid',
            'TDID' => ' Trees Name',
			'PackageCost'=>'Package Cost',
            'Duration' => 'Package Duration(In Month)',
            'Quantity' => 'Trees Quantity',
            'PlantingCost' => 'Trees Planting Cost',
            'MonthlyRecuringCost' => 'Trees Monthly Recuring Cost',
            'CreateDate' => 'Package Create Date',
            'UpdateDate' => 'Package Update Date',
            'Isdelete' => 'Isdelete',
        ];
    }
	public function getTreename($pkgid){
		$treepkg = TreePackage::find()->where(['PKGID'=>$pkgid])->one();
		if(count($treepkg)){
		   $treeid = $treepkg['TDID'];
		   $treedetails = TreeDetails::find()->where(['ID'=>$treeid])->one();
		   if(count($treedetails)){
			   $treename = $treedetails['TreeName'];
		   }
		}
		else{
			$treename = '';
		}
		
		return $treename;
	}
	public function getTreepackage($pkgid){
		$treepkg = TreePackage::find()->where(['PKGID'=>$pkgid])->one();
		if(count($treepkg)){
		   $pkgname = $treepkg['PackageName'];
		}
		else{
			$pkgname = '';
		}
		
		return $pkgname;
	}
    public function getPackagecost($pkgid){
		$treepkg = TreePackage::find()->where(['PKGID'=>$pkgid])->one();
		if(count($treepkg)){
		   $pkgcost = $treepkg['PackageCost'];
		}
		else{
			$pkgcost = '';
		}
		
		return $pkgcost;
	}
    public function getTreedetail()
    {
        return $this->hasOne(TreeDetails::className(), ['ID' => 'TDID']);
    }
}
