<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "StockTransaction".
 *
 * @property int $StockId
 * @property int $TreeDetailsId
 * @property int $Stock
 * @property int $IsDelete
 * @property string $AddedDate
 * @property string $UpdateDate
 */
class StockTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'StockTransaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Stock'], 'required'],
            [['TreeDetailsId', 'Stock', 'IsDelete'], 'integer'],
            [['AddedDate', 'UpdateDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'StockId' => Yii::t('app', 'Stock ID'),
            'TreeDetailsId' => Yii::t('app', 'Tree Details ID'),
            'Stock' => Yii::t('app', 'Stock'),
            'IsDelete' => Yii::t('app', 'Is Delete'),
            'AddedDate' => Yii::t('app', 'Added Date'),
            'UpdateDate' => Yii::t('app', 'Update Date'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return StockTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StockTransactionQuery(get_called_class());
    }
}
