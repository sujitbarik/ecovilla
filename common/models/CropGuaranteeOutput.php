<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CropGuaranteeOutput".
 *
 * @property int $CGID
 * @property int $CropID
 * @property int $SizeID
 * @property string $Harvest
 * @property int $IsDelete
 * @property string $OnDate
 * @property string $UpdatedDate
 *
 * @property Crops $crop
 * @property Size $size
 */
class CropGuaranteeOutput extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CropGuaranteeOutput';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CropID', 'SizeID'], 'required'],
            [['CropID', 'SizeID', 'IsDelete'], 'integer'],
            [['OnDate', 'UpdatedDate'], 'safe'],
            [['Harvest'], 'string', 'max' => 20],
            [['CropID'], 'exist', 'skipOnError' => true, 'targetClass' => Crops::className(), 'targetAttribute' => ['CropID' => 'CropID']],
            [['SizeID'], 'exist', 'skipOnError' => true, 'targetClass' => Size::className(), 'targetAttribute' => ['SizeID' => 'SizeID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CGID' => 'Cgid',
            'CropID' => 'Crop ID',
            'SizeID' => 'Size ID',
            'Harvest' => 'Harvest (In kg)',
            'IsDelete' => 'Is Delete',
            'OnDate' => 'On Date',
            'UpdatedDate' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crops::className(), ['CropID' => 'CropID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSize()
    {
        return $this->hasOne(Size::className(), ['SizeID' => 'SizeID']);
    }
}
