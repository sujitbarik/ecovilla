<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Pages".
 *
 * @property int $ID
 * @property string $PageName
 * @property string $PageContents
 * @property string $AddedDate
 * @property string $Ondate
 * @property int $IsDelete
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PageName', 'PageContents', 'AddedDate'], 'required'],
            [['PageContents'], 'string'],
            [['AddedDate', 'Ondate'], 'safe'],
            [['IsDelete'], 'integer'],
            [['PageName'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PageName' => Yii::t('app', 'Page Name'),
            'PageContents' => Yii::t('app', 'Page Contents'),
            'AddedDate' => Yii::t('app', 'Added Date'),
            'Ondate' => Yii::t('app', 'Ondate'),
            'IsDelete' => Yii::t('app', 'Is Delete'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return PagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PagesQuery(get_called_class());
    }
}
