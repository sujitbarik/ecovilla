<?php

namespace common\models;

use Yii;
use common\models\CropSizePrice;

/**
 * This is the model class for table "Crops".
 *
 * @property int $CropID
 * @property string $CropName
 * @property string $ScientificName
 * @property string $MaturityTime
 * @property string $PlantationDays
 * @property double $PlantingCost
 * @property double $MaintenanceCost
 * @property int $IsDelete
 * @property string $OnDate
 * @property string $UpdatedDate
 *
 * @property CropEstimatedOutput[] $cropEstimatedOutputs
 * @property CropGuaranteeOutput[] $cropGuaranteeOutputs
 */
class Crops extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Crops';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CropName', 'ScientificName', 'MaturityTime'], 'required'],
            [['PlantingCost', 'MaintenanceCost'], 'number'],
            [['IsDelete'], 'integer'],
            [['OnDate', 'UpdatedDate','CropAppImage'], 'safe'],
            [['CropName', 'ScientificName'], 'string', 'max' => 200],
            [['MaturityTime', 'PlantationDays'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CropID' => 'Crop ID',
            'CropName' => 'Crop Name',
            'CropImage'=>'Crop Image',
            'ScientificName' => 'Scientific Name',
            'MaturityTime' => 'Maturity Time(In Month)',
            'PlantationDays' => 'Plantation Days',
            'PlantingCost' => 'Planting Cost',
            'MaintenanceCost' => 'Maintenance Cost',
            'IsDelete' => 'Is Delete',
            'OnDate' => 'On Date',
            'UpdatedDate' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCropEstimatedOutputs()
    {
        return $this->hasMany(CropEstimatedOutput::className(), ['CropID' => 'CropID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCropGuaranteeOutputs()
    {
        return $this->hasMany(CropGuaranteeOutput::className(), ['CropID' => 'CropID']);
    }
	
	public function getCropname($cropsizepriceid){
		$details = CropSizePrice::find()->where(['Id'=>$cropsizepriceid])->one();
		if(count($details)){
			$cropid = $details['CropID'];
			$cropdetails = Crops::find()->where(['CropID'=>$cropid])->one();
			$cropname = $cropdetails['CropName'];
		}
		else{
			$cropname = '';
		}
		return $cropname;
	}
    
    public function getCropdetail($cropsizepriceid){
		$details = CropSizePrice::find()->where(['Id'=>$cropsizepriceid])->one();
		if(count($details)){
			$cropid = $details['CropID'];
			$cropdetails = Crops::find()->where(['CropID'=>$cropid])->one();
			$rest['cropname'] = $cropdetails->ScientificName;
            $rest['maturity'] = $cropdetails->MaturityTime;
		}
		else{
			$rest = array();
		}
		return $rest;
	}
    
    public function getCropimage()
    {
        return $this->hasOne(Image::className(), ['ImageID' => 'CropImage'])->orderBy(['ImageID'=>SORT_DESC]);
    }
}
