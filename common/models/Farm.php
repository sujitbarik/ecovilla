<?php

namespace common\models;

use Yii;
use common\models\Size;

/**
 * This is the model class for table "Farm".
 *
 * @property int $FarmID
 * @property int $Size
 * @property int $Price
 * @property int $SoilReport
 * @property string $Location
 * @property string $Duration
 * @property int $IsDelete
 * @property string $OnDate
 * @property string $UpdatedDate
 *
 * @property Size $size
 * @property Image $soilReport
 * @property FarmImage[] $farmImages
 */
class Farm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Farm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FarmName','Size', 'Price','Location', 'Duration','OnDate'], 'required'],
            [['Size', 'Price','IsDelete','Stock','AvailableStock'], 'integer'],
            [['Location'], 'string'],
            [['OnDate', 'UpdatedDate'], 'safe'],
            [['Duration'], 'string', 'max' => 20],
            [['Size'], 'exist', 'skipOnError' => true, 'targetClass' => Size::className(), 'targetAttribute' => ['Size' => 'SizeID']],
           // [['SoilReport'], 'exist', 'skipOnError' => true, 'targetClass' => Image::className(), 'targetAttribute' => ['SoilReport' => 'ImageID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'FarmID' => 'Farm ID',
            'FarmName' => 'Farm Name',
            'Size' => 'Size (sq Ft)',
            'Price' => 'Price',
            'SoilReport' => 'Soil Report',
            'Location' => 'Location',
            'Duration' => 'Duration',
            'IsDelete' => 'Is Delete',
            'OnDate' => 'On Date',
            'UpdatedDate' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSize()
    {
        return $this->hasOne(Size::className(), ['SizeID' => 'Size']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSoilReport()
    {
        return $this->hasOne(Image::className(), ['ImageID' => 'SoilReport']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFarmImages()
    {
        return $this->hasMany(FarmImage::className(), ['FarmID' => 'FarmID']);
    }
	
	public function getFarmsize($farmid){
		$details = Farm::find()->where(['FarmID'=>$farmid])->one();
		if(count($details)){
			$sizeid = $details['Size'];
			$sizedetails = Size::find()->where(['SizeID'=>$sizeid])->one();
			$size = $sizedetails['Size'];
		}
		else{
			$size = '';
		}
		return $size;
    }
    
    public function getFarmname($farmid){
        $details = Farm::find()->where(['FarmID'=>$farmid])->one();
        if(count($details)){
            $name = $details['FarmName'];
        }
        else{
            $name= "";
        }
        return $name;
    }
	
}
