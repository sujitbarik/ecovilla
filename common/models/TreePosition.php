<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "TreePosition".
 *
 * @property int $TreePositionId
 * @property int $UserId
 * @property string $TopPosition
 * @property string $LeftPosition
 * @property int $Is
 * @property string $OnDate
 * @property string $UpdatedDate
 */
class TreePosition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TreePosition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UserId', 'TopPosition', 'LeftPosition'], 'required'],
            [['UserId', 'IsDelete'], 'integer'],
            [['OnDate', 'UpdatedDate'], 'safe'],
            [['TopPosition', 'LeftPosition'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TreePositionId' => Yii::t('app', 'Tree Position ID'),
            'UserId' => Yii::t('app', 'User ID'),
            'TopPosition' => Yii::t('app', 'Top Position'),
            'LeftPosition' => Yii::t('app', 'Left Position'),
            'IsDelete' => Yii::t('app', 'IsDelete'),
            'OnDate' => Yii::t('app', 'On Date'),
            'UpdatedDate' => Yii::t('app', 'Updated Date'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TreePositionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TreePositionQuery(get_called_class());
    }
}
