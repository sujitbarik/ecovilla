<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Krititech</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
		
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
		
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    //['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    //['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    
					[
                        'label' => 'Customer',
                        'icon' => 'user',
                        'url' => '#',
                        'items' => [
                            
                            ['label' => 'All Customer', 'icon' => 'file-code-o', 'url' => ['customer-registration/index'],],
                            ['label' => 'Add Customer Points', 'icon' => 'file-code-o', 'url' => ['customer-registration/addpoints'],],
                        ],
                    ],
                    [
                        'label' => 'Add',
                        'icon' => 'plus-square',
                        'url' => '#',
                        'items' => [
                            
                            ['label' => 'Size', 'icon' => 'file-code-o', 'url' => ['size/index'],],
                            ['label' => 'Crops', 'icon' => 'file-code-o', 'url' => ['crops/index'],],
							['label' => 'Farm', 'icon' => 'file-code-o', 'url' => ['farm/index'],],
							['label' => 'Add Crop Growth Image', 'icon' => 'file-code-o', 'url' => ['crops/croplist'],],
							
                        ],
                    ],
					[
                        'label' => 'Trees',
                        'icon' => 'plus-square',
                        'url' => '#',
                        'items' => [
							['label' => 'Tree', 'icon' => 'file-code-o', 'url' => ['tree-details/index'],],
							['label' => 'Package', 'icon' => 'file-code-o', 'url' => ['tree-package/index'],],
                        ],
                    ],
					[
                        'label' => 'Report',
                        'icon' => 'bar-chart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Transaction Report', 'icon' => 'money', 'url' => ['site/alltransaction'],],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
